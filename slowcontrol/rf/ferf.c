/********************************************************************\

  Name:         ferf.c
  Created by:   Pierre
  $Id: ferf.c 84 2007-11-06 21:58:48Z midas $

\********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "midas.h"
#include "mcstd.h"
#include "mscb.h"
#include "experim.h"

#define N_DEVICES          2 // # of mscb devices
#define MAX_RF_CHANNELS    5 // # of mode per devices
#define SWEEP_MODE       0x2 // mode 1...
#define SIN_BURST_MODE   0x4
#define LADDER_MODE      0x8
#define FM_MODE          0x10
#define AM_MODE          0x20 // mode 5

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "ferf";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 0000;

/* maximum event size produced by this frontend */
INT max_event_size = 10000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 100 * 10000;

typedef struct {
   char mscb_device[NAME_LENGTH];
   char pwd[NAME_LENGTH];
   int  base_address;
} LMSCB_SETTINGS;
LMSCB_SETTINGS lmscb;
int lmscb_fd;

#define LMSCB_SETTINGS_STR "\
MSCB Device = STRING : [32] usb0\n\
MSCB Pwd = STRING : [32] \n\
Base Address = INT : 1\n\
"

#define RF_SETTINGS_STR "\
Address Offset = INT : 0\n\
Mode = STRING : [32] none\n\
controlMng = INT : 0\n\
status = INT : 0\n\
condition = INT : 0\n\
amplitude = DOUBLE : 555\n\
duration = DOUBLE : 1.02\n\
num Cycles = INT : 12\n\
num Steps = INT : 14\n\
center Freq = DOUBLE : 2000000\n\
start Freq = DOUBLE : 2000000\n\
end Freq = DOUBLE : 2000000\n\
burst Freq = DOUBLE : 1000000\n\
Modulation Freq = DOUBLE : 2000000\n\
fa Depth = DOUBLE : 2000000\n\
"
typedef struct {
  INT       node_address;
  char      mode[32];
  INT       controlmng;
  INT       status;
  INT       condition;
  double    amplitude;
  double    duration;
  INT       num_cycles;
  INT       num_steps;
  double    center_freq;
  double    start_freq;
  double    end_freq;
  double    burst_freq;
  double    modulation_freq;
  double    fadepth;
} RF_SETTINGS;

typedef struct {
  HNDLE hrfS;
  RF_SETTINGS *rfset;
} RF_STRUCT;

HNDLE hSet;
RF_STRUCT *prfs[MAX_RF_CHANNELS]={NULL, NULL, NULL, NULL};

/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);
INT read_mscb_event(char *pevent, INT off);
INT localmscb_init(char *eqname);
void register_cnaf_callback(int debug);
void rf_callback(INT hDB, INT hKey, void *info);
INT rfchannel_init(char *eqname, int nchannel);

/*-- Equipment list ------------------------------------------------*/

EQUIPMENT equipment[] = {
   {"RF",                  /* equipment name */
    {3, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC | EQ_MANUAL_TRIG,   /* equipment type */
     0,                      /* event source */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_ALWAYS | RO_TRANSITIONS |   /* read when running and on transitions */
     RO_ODB,                 /* and update ODB */
     10000,                  /* read every 10 sec */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     1,                      /* log history */
     "", "", "",},
    read_mscb_event,       /* readout routine */
    },

   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
  int status;
   /* hardware initialization */
   /* print message and return FE_ERR_HW if frontend should not be started */
   status = localmscb_init("RF");
   if (status != FE_SUCCESS) {
     cm_msg(MERROR,"ferf","Access to mscb failed [%d]", status);
     return status;
   }
   //   printf("localmscb_init status:%d\n", status);
   
   status = rfchannel_init("RF", N_DEVICES);
   printf("mscb and rfchannel_init status:%d\n", status);

   return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
  int i;

  for (i=0 ; i<MAX_RF_CHANNELS ; i++) {
    if (prfs[i])
      free(prfs[i]);
    prfs[i] = NULL;
  }
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */
  ss_sleep(100);
   return SUCCESS;
}

/*------------------------------------------------------------------*/
/********************************************************************\

  Readout routines for different events

\********************************************************************/
/*-- Trigger event routines ----------------------------------------*/
INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   int i;
   DWORD lam;

   for (i = 0; i < count; i++) {
     lam = 1;
      if (lam & LAM_SOURCE_STATION(source))
         if (!test)
            return lam;
   }
   return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}

static BOOL update_flag = FALSE;
/*-- MSCB event --------------------------------------------------*/
INT read_mscb_event(char *pevent, INT off)
{
  int i, status, size, stat;
  double *pddata;
  HNDLE hDB;
  //  float fvalue;
  //  DWORD dvalue;
  //  WORD svalue;

  cm_get_experiment_database(&hDB, NULL);

  bk_init(pevent);
  /* Find RF channel */
  for (i= 0 ; i<MAX_RF_CHANNELS; i++) {
    if (prfs[i]) {
      size = sizeof(INT);
      status = db_get_value(hDB, prfs[i]->hrfS, "status", &stat, &size, TID_INT, FALSE);
      if (stat == 0) continue;
      //      printf("Compose event for RF_%i\n", i);
      if (update_flag) {
	switch (stat) {
	case SWEEP_MODE: 
	  bk_create(pevent, "SWEE", TID_DOUBLE, &pddata);
	  
	  *pddata++ = (double) prfs[i]->rfset->duration;
	  *pddata++ = (double) prfs[i]->rfset->start_freq;
	  *pddata++ = (double) prfs[i]->rfset->end_freq;
	  *pddata++ = (double) prfs[i]->rfset->amplitude;
	  
	  /*
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 3, &fvalue, &size);
	    *pddata++ = (double) fvalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 5, &dvalue, &size);	
	    *pddata++ = (double) dvalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 6, &dvalue, &size);
	    *pddata++ = (double) dvalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 7, &fvalue, &size);
	    *pddata++ = (double) fvalue;
	    */
	  
	  bk_close(pevent, pddata);
	  break;
	case SIN_BURST_MODE:
	  bk_create(pevent, "SINB", TID_DOUBLE, &pddata);
	  
	  *pddata++ = (double) prfs[i]->rfset->num_cycles;
	  *pddata++ = (double) prfs[i]->rfset->burst_freq;
	  *pddata++ = (double) prfs[i]->rfset->amplitude;
	  
	  /*
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 9, &svalue, &size);
	    *pddata++ = (double) svalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 8, &dvalue, &size);	
	    *pddata++ = (double) dvalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 7, &fvalue, &size);
	    *pddata++ = (double) fvalue;
	    */
	  
	  bk_close(pevent, pddata);
	  break;
	case LADDER_MODE:	
	  bk_create(pevent, "LADB", TID_DOUBLE, &pddata);
	  
	  *pddata++ = (double) prfs[i]->rfset->num_cycles;
	  *pddata++ = (double) prfs[i]->rfset->num_steps;	
	  *pddata++ = (double) prfs[i]->rfset->modulation_freq;
	  *pddata++ = (double) prfs[i]->rfset->amplitude;
	  
	  /*
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 9, &svalue, &size);
	    *pddata++ = (double) svalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 4, &fvalue, &size);	
	    *pddata++ = (double) svalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 8, &dvalue, &size);
	    *pddata++ = (double) dvalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 7, &fvalue, &size);
	    *pddata++ = (double) fvalue;
	    */
	  
	  bk_close(pevent, pddata);
	  break;
	case FM_MODE:	
	  bk_create(pevent, "FM__", TID_DOUBLE, &pddata);
	  
	  *pddata++ = (double) prfs[i]->rfset->center_freq;
	  *pddata++ = (double) prfs[i]->rfset->modulation_freq;
	  *pddata++ = (double) prfs[i]->rfset->fadepth;
	  *pddata++ = (double) prfs[i]->rfset->amplitude;
	  
	  /*
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 5, &dvalue, &size);
	    *pddata++ = (double) dvalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 6, &dvalue, &size);	
	    *pddata++ = (double) dvalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 7, &fvalue, &size);
	    *pddata++ = (double) fvalue;
	    */
	  
	  bk_close(pevent, pddata);
	  break;
	case AM_MODE:	
	  bk_create(pevent, "AM__", TID_DOUBLE, &pddata);
	  
	  *pddata++ = (double) prfs[i]->rfset->center_freq;
	  *pddata++ = (double) prfs[i]->rfset->modulation_freq;
	  *pddata++ = (double) prfs[i]->rfset->fadepth;
	  *pddata++ = (double) prfs[i]->rfset->amplitude;
	  
	  /*
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 5, &dvalue, &size);
	    *pddata++ = (double) dvalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 6, &dvalue, &size);	
	    *pddata++ = (double) dvalue;
	    status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 7, &fvalue, &size);
	    *pddata++ = (double) fvalue;
	    */
	  
	  bk_close(pevent, pddata);
	  break;
	}  // switch
      }  // update
    }  // valid channel
  }  // over channels
  return bk_size(pevent);
}

/*-- Local MSCB event --------------------------------------------------*/
INT localmscb_init(char *eqname)
{
  int  status, size;
  HNDLE hDB, hDD;
  MSCB_INFO node_info;
  char set_str[80];
  /* Book Setting space */
  
  cm_get_experiment_database(&hDB, NULL);

  /* Map /equipment/Trigger/settings for the sequencer */
  sprintf(set_str, "/Equipment/%s/Settings", eqname);
  status = db_create_key(hDB, 0, set_str, TID_KEY);
  status = db_find_key (hDB, 0, set_str, &hSet);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"FE","Key %s not found", set_str);

  /* create MSCB settings record */
  status = db_find_key (hDB, 0, set_str, &hSet);
  status = db_create_record(hDB, hSet, "DD", LMSCB_SETTINGS_STR);
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;

  db_find_key(hDB, hSet, "DD", &hDD);
  size = sizeof(lmscb);
  db_get_record(hDB, hDD, &lmscb, &size, 0);

  /* open device on MSCB */
  lmscb_fd = mscb_init(lmscb.mscb_device, NAME_LENGTH, lmscb.pwd, FALSE);
  if (lmscb_fd < 0) {
    cm_msg(MERROR, "mscb_init",
      "Cannot access MSCB submaster at \"%s\". Check power and connection.",
      lmscb.mscb_device);
    return FE_ERR_HW;
  }

  /* check if FGD devices are alive */
  mscb_ping(lmscb_fd, lmscb.base_address, 0);
  if (status != FE_SUCCESS) {
    cm_msg(MERROR, "mscb_init",
      "Cannot ping MSCB address 0. Check power and connection.");
    return FE_ERR_HW;
  }
  
  mscb_info(lmscb_fd, lmscb.base_address, &node_info);
  if (strncmp(node_info.node_name, "RF310_",6) != 0) {
    cm_msg(MERROR, "mscb_init",
	   "Found one expected node \"%s\" at address \"%d\".", node_info.node_name, lmscb.base_address);
    return FE_ERR_HW;
  }

  return FE_SUCCESS;
}

/*-- RF channel --------------------------------------------------*/
void rf_callback(INT hDB, INT hKey, void *info)
{
  int i, status, size, try;
  float fvalue;
  WORD svalue;
  char freq[32], cvalue;
  KEY  key;
  
  printf(" call back for rf %d\n", hKey);
  
  /* Find RF channel */
  for (i=0; i<MAX_RF_CHANNELS; i++) {
    if (prfs[i] == 0) continue;
    if (prfs[i]->hrfS == hKey) {
      db_close_record(hDB, hKey);
      //     printf("callback on RF_%i\n", i);
      if (prfs[i]->rfset->controlmng) {
	printf("Load mode %s\n", prfs[i]->rfset->mode);
	switch (prfs[i]->rfset->controlmng) {
	case SWEEP_MODE: 
	  fvalue = (float) prfs[i]->rfset->duration;
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 5, &fvalue, sizeof(float)); 
	  sprintf(freq, "%le", prfs[i]->rfset->start_freq);
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 9, freq, strlen(freq)+1); 
	  sprintf(freq, "%le", prfs[i]->rfset->end_freq);
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 10, freq, strlen(freq)+1);
	  fvalue = (float) prfs[i]->rfset->amplitude;
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 4, &fvalue, sizeof(float));
	  break;
	case SIN_BURST_MODE:
	  svalue = (WORD) prfs[i]->rfset->num_cycles;
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 6, &svalue, sizeof(WORD)); 
	  sprintf(freq, "%le", prfs[i]->rfset->burst_freq);
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 11, freq, strlen(freq)+1);
	  fvalue = (float) prfs[i]->rfset->amplitude;
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 4, &fvalue, sizeof(float)); 
	  break;
	case LADDER_MODE:	
	  svalue = (WORD) prfs[i]->rfset->num_cycles;
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 6, &svalue, sizeof(WORD)); 
	  svalue = (WORD) prfs[i]->rfset->num_steps;
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 7, &svalue, sizeof(WORD)); 
	  sprintf(freq, "%le", prfs[i]->rfset->modulation_freq);
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 12, freq, strlen(freq)+1);
	  fvalue = (float) prfs[i]->rfset->amplitude;
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 4,&fvalue, sizeof(float)); 
	  break;
	case FM_MODE:	
	  sprintf(freq, "%le", prfs[i]->rfset->center_freq); // Central Frequency
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 8, freq, strlen(freq)+1); 
	  sprintf(freq, "%le", prfs[i]->rfset->modulation_freq); // Deviation
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 12, freq, strlen(freq)+1); 
	  sprintf(freq, "%le", prfs[i]->rfset->fadepth);
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 13, freq, strlen(freq)+1); 
	  fvalue = (float) prfs[i]->rfset->amplitude;
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 4, &fvalue, sizeof(float)); 
	  break;
	case AM_MODE:	
	  sprintf(freq, "%le", prfs[i]->rfset->center_freq); // Central Frequency
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 8, freq, strlen(freq)+1); 
	  sprintf(freq, "%le", prfs[i]->rfset->modulation_freq); // Deviation
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 12, freq, strlen(freq)+1); 
	  sprintf(freq, "%le", prfs[i]->rfset->fadepth);
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 13, freq, strlen(freq)+1); 
	  fvalue = (float) prfs[i]->rfset->amplitude;
	  mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 4, &fvalue, sizeof(float)); 
	  break;
	}
	// Trigger the change into the device
	size = sizeof(char);
	cvalue = (char) prfs[i]->rfset->controlmng;
	mscb_write(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 0, &cvalue, size);
	db_get_key(hDB, hKey, &key);
	cm_msg(MINFO,"ferf","RF channel(%s) mode:%s(%d) updated", key.name, prfs[i]->rfset->mode, prfs[i]->rfset->status);
	update_flag = TRUE;
	// Report the current mode state
	try = 0;
	do {
	  ss_sleep(1000);
	  size = sizeof(char);
	  status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 0, &cvalue, &size);
	  //	  printf("control:%x status:%d\n", cvalue, status);
	} while ((cvalue != 0) && (try++ < 10));

	// Reset the control
	size = sizeof(char);
	status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 0, &cvalue, &size);
	//	printf("mscb control:%d\n", cvalue);
	prfs[i]->rfset->controlmng = (INT) cvalue;
	status = db_set_value(hDB, hKey, "controlmng", &((INT) prfs[i]->rfset->controlmng), sizeof(INT), 1, TID_INT);
	size = sizeof(char);
	status = mscb_read(lmscb_fd, lmscb.base_address + prfs[i]->rfset->node_address, 1, &cvalue, &size);
	//	printf("mscb status:%d\n", cvalue);
	prfs[i]->rfset->status = (INT) cvalue;
	status = db_set_value(hDB, hKey, "status", &((INT) prfs[i]->rfset->status), sizeof(INT), 1, TID_INT);
      } else {
	printf("Record touched (control:%d)\n", prfs[i]->rfset->controlmng);
      }
      size = sizeof(RF_SETTINGS);
      status = db_open_record(hDB, prfs[i]->hrfS, prfs[i]->rfset, size, MODE_READ, rf_callback, NULL);
      if (status != DB_SUCCESS) {
	cm_msg(MERROR,"rfchannel"," cannot open re-record");
      }
    }
  }
}


/*-- RF channel --------------------------------------------------*/
INT rfchannel_init(char *eqname, int nchannel)
{
  int  status, size, i;
  HNDLE hDB, hRFS;
  char rf_str[80], info[32];

  cm_get_experiment_database(&hDB, NULL);

  /* Map /equipment/Trigger/settings */
  sprintf(rf_str, "/Equipment/%s/Settings", eqname);
  status = db_create_key(hDB, 0, rf_str, TID_KEY);
  status = db_find_key (hDB, 0, rf_str, &hSet);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"FE","Key %s not found", rf_str);

  for (i=0 ; i< nchannel ; i++) {
    sprintf(rf_str, "RF_%1i", i);
    status = db_find_key(hDB, hSet, rf_str, &hRFS);
    if (status != DB_SUCCESS) {
      /* create RF channel settings record */
      status = db_create_record(hDB, hSet, rf_str, RF_SETTINGS_STR);
      if (status != DB_SUCCESS)  return FE_ERR_ODB;
      // Get handle to RF setting
      db_find_key(hDB, hSet, rf_str, &hRFS);
    }
    if (i==2 || i==3) {
      size = sizeof(RF_STRUCT);
      prfs[i] = (RF_STRUCT *) malloc(size);
      size = sizeof(RF_SETTINGS);
      prfs[i]->rfset = (RF_SETTINGS *) malloc(size);
      prfs[i]->hrfS = hRFS;
      status = db_open_record(hDB, prfs[i]->hrfS, prfs[i]->rfset, size, MODE_READ, rf_callback, NULL);
      if (status != DB_SUCCESS) {
	cm_msg(MERROR,"rfchannel"," cannot open record on %s", rf_str);
      }
      
      // Get initial record from ODB
      size = sizeof(RF_SETTINGS);
      db_get_record(hDB, prfs[i]->hrfS, prfs[i]->rfset, &size, 0);
      // Load current ODB settings
      sprintf(info, "%s", prfs[i]->rfset->mode);
      rf_callback(hDB, prfs[i]->hrfS, info);
    }
  }
return FE_SUCCESS;
}

