/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Fri Jul  6 21:30:35 2007

\********************************************************************/

#define EXP_EDIT_DEFINED

typedef struct {
  BOOL      pedestals_run;
  BOOL      write_data;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) char *_name[] = {\
"[.]",\
"Pedestals run = BOOL : n",\
"Write Data = LINK : [19] /Logger/Write data",\
"",\
NULL }

#ifndef EXCL_TRIGGER

#define TRIGGER_SETTINGS_DEFINED

typedef struct {
  INT       mode;
  DWORD     channel_mask_1;
  DWORD     channel_mask_2;
} TRIGGER_SETTINGS;

#define TRIGGER_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"mode = INT : 1",\
"Channel Mask 1 = DWORD : 16777215",\
"Channel Mask 2 = DWORD : 16777215",\
"",\
NULL }

#define TRIGGER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 16777215",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] xenon02.triumf.ca",\
"Frontend name = STRING : [32] feVT48",\
"Frontend file name = STRING : [256] fevt48.c",\
"",\
NULL }

#endif

#ifndef EXCL_HV

#define HV_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} HV_COMMON;

#define HV_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 255",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] xenon02.triumf.ca",\
"Frontend name = STRING : [32] FeSy2527",\
"Frontend file name = STRING : [256] sy2527.c",\
"",\
NULL }

#define HV_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      struct {
        char      system_name[32];
        char      ip[32];
        INT       linktype;
        INT       first_slot;
      } dd;
      struct {
        char      description[128];
        char      name[128];
        char      model[15];
        WORD      channels;
      } slot_0;
      struct {
        char      description[128];
        char      name[128];
        char      model[15];
        WORD      channels;
      } slot_1;
    } sy2527;
  } devices;
  char      names[24][32];
  float     update_threshold_measured[24];
  float     update_threshold_current[24];
  float     voltage_limit[24];
  float     current_limit[24];
  float     trip_time[24];
  float     ramp_up_speed[24];
  float     ramp_down_speed[24];
  float     zero_threshold[24];
} HV_SETTINGS;

#define HV_SETTINGS_STR(_name) char *_name[] = {\
"[Devices/sy2527/DD]",\
"System Name = STRING : [32] sy2527",\
"IP = STRING : [32] 142.90.111.74",\
"LinkType = INT : 0",\
"First Slot = INT : 0",\
"",\
"[Devices/sy2527/Slot 0]",\
"Description = STRING : [128]  12 Ch Neg. 3KV 20uA ",\
"Name = STRING : [128] sy2527",\
"Model = STRING : [15] A1821",\
"Channels = WORD : 12",\
"",\
"[Devices/sy2527/Slot 1]",\
"Description = STRING : [128]  12 Ch Neg. 3KV 3mA  ",\
"Name = STRING : [128] sy2527",\
"Model = STRING : [15] A1733",\
"Channels = WORD : 12",\
"",\
"[.]",\
"Names = STRING[24] :",\
"[32] CHANNEL00",\
"[32] CHANNEL01",\
"[32] CHANNEL02",\
"[32] CHANNEL03",\
"[32] CHANNEL04",\
"[32] CHANNEL05",\
"[32] CHANNEL06",\
"[32] CHANNEL07",\
"[32] CHANNEL08",\
"[32] CHANNEL09",\
"[32] CHANNEL10",\
"[32] CHANNEL11",\
"[32] Slot2%ch00",\
"[32] Slot2%ch01",\
"[32] Slot2%ch02",\
"[32] Slot2%ch03",\
"[32] Slot2%ch04",\
"[32] Slot2%ch05",\
"[32] Slot2%ch06",\
"[32] Slot2%ch07",\
"[32] Slot2%ch08",\
"[32] Slot2%ch09",\
"[32] Slot2%ch10",\
"[32] Slot2%ch11",\
"Update Threshold Measured = FLOAT[24] :",\
"[0] 0.5",\
"[1] 0.5",\
"[2] 0.5",\
"[3] 0.5",\
"[4] 0.5",\
"[5] 0.5",\
"[6] 0.5",\
"[7] 0.5",\
"[8] 0.5",\
"[9] 0.5",\
"[10] 0.5",\
"[11] 0.5",\
"[12] 0.5",\
"[13] 0.5",\
"[14] 0.5",\
"[15] 0.5",\
"[16] 0.5",\
"[17] 0.5",\
"[18] 0.5",\
"[19] 0.5",\
"[20] 0.5",\
"[21] 0.5",\
"[22] 0.5",\
"[23] 0.5",\
"Update Threshold Current = FLOAT[24] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"[16] 1",\
"[17] 1",\
"[18] 1",\
"[19] 1",\
"[20] 1",\
"[21] 1",\
"[22] 1",\
"[23] 1",\
"Voltage Limit = FLOAT[24] :",\
"[0] 3000",\
"[1] 3000",\
"[2] 3000",\
"[3] 3000",\
"[4] 3000",\
"[5] 3000",\
"[6] 3000",\
"[7] 3000",\
"[8] 3000",\
"[9] 3000",\
"[10] 3000",\
"[11] 3000",\
"[12] 3000",\
"[13] 3000",\
"[14] 3000",\
"[15] 3000",\
"[16] 3000",\
"[17] 3000",\
"[18] 3000",\
"[19] 3000",\
"[20] 3000",\
"[21] 3000",\
"[22] 3000",\
"[23] 3000",\
"Current Limit = FLOAT[24] :",\
"[0] 2",\
"[1] 2",\
"[2] 2",\
"[3] 2",\
"[4] 2",\
"[5] 2",\
"[6] 2",\
"[7] 2",\
"[8] 2",\
"[9] 2",\
"[10] 2",\
"[11] 2",\
"[12] 140",\
"[13] 140",\
"[14] 140",\
"[15] 140",\
"[16] 140",\
"[17] 140",\
"[18] 140",\
"[19] 140",\
"[20] 140",\
"[21] 140",\
"[22] 140",\
"[23] 140",\
"Trip Time = FLOAT[24] :",\
"[0] 10",\
"[1] 10",\
"[2] 10",\
"[3] 10",\
"[4] 10",\
"[5] 10",\
"[6] 10",\
"[7] 10",\
"[8] 10",\
"[9] 10",\
"[10] 10",\
"[11] 10",\
"[12] 10",\
"[13] 10",\
"[14] 10",\
"[15] 10",\
"[16] 10",\
"[17] 10",\
"[18] 10",\
"[19] 10",\
"[20] 10",\
"[21] 10",\
"[22] 10",\
"[23] 10",\
"Ramp Up Speed = FLOAT[24] :",\
"[0] 50",\
"[1] 100",\
"[2] 50",\
"[3] 50",\
"[4] 50",\
"[5] 50",\
"[6] 50",\
"[7] 50",\
"[8] 50",\
"[9] 50",\
"[10] 50",\
"[11] 50",\
"[12] 10",\
"[13] 50",\
"[14] 50",\
"[15] 50",\
"[16] 50",\
"[17] 50",\
"[18] 50",\
"[19] 50",\
"[20] 50",\
"[21] 50",\
"[22] 50",\
"[23] 50",\
"Ramp Down Speed = FLOAT[24] :",\
"[0] 50",\
"[1] 50",\
"[2] 50",\
"[3] 50",\
"[4] 50",\
"[5] 50",\
"[6] 50",\
"[7] 50",\
"[8] 50",\
"[9] 50",\
"[10] 50",\
"[11] 50",\
"[12] 50",\
"[13] 50",\
"[14] 50",\
"[15] 50",\
"[16] 50",\
"[17] 50",\
"[18] 50",\
"[19] 50",\
"[20] 50",\
"[21] 50",\
"[22] 50",\
"[23] 50",\
"Zero Threshold = FLOAT[24] :",\
"[0] 20",\
"[1] 20",\
"[2] 20",\
"[3] 20",\
"[4] 20",\
"[5] 20",\
"[6] 20",\
"[7] 20",\
"[8] 20",\
"[9] 20",\
"[10] 20",\
"[11] 20",\
"[12] 20",\
"[13] 20",\
"[14] 20",\
"[15] 20",\
"[16] 20",\
"[17] 20",\
"[18] 20",\
"[19] 20",\
"[20] 20",\
"[21] 20",\
"[22] 20",\
"[23] 20",\
"",\
NULL }

#endif

#ifndef EXCL_APDHV

#define APDHV_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} APDHV_COMMON;

#define APDHV_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 255",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] xenon01.triumf.ca",\
"Frontend name = STRING : [32] FeIvc32",\
"Frontend file name = STRING : [256] feivc32.c",\
"",\
NULL }

#define APDHV_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      struct {
        char      mscb_device[32];
        char      mscb_pwd[32];
        INT       base_address;
      } dd;
    } ivc32_device;
  } devices;
  char      names[64][32];
  float     update_threshold_measured[64];
  float     update_threshold_current[64];
  float     voltage_limit[64];
  float     current_limit[64];
  float     trip_time[64];
  float     ramp_step[64];
  float     step_time[64];
} APDHV_SETTINGS;

#define APDHV_SETTINGS_STR(_name) char *_name[] = {\
"[Devices/IVC32 Device/DD]",\
"MSCB Device = STRING : [32] usb0",\
"MSCB Pwd = STRING : [32] ",\
"Base Address = INT : 0",\
"",\
"[.]",\
"Names = STRING[64] :",\
"[32] Front%CH 0",\
"[32] Front%CH 1",\
"[32] Front%CH 2",\
"[32] Front%CH 3",\
"[32] Front%CH 4",\
"[32] Front%CH 5",\
"[32] Front%CH 6",\
"[32] Front%CH 7",\
"[32] Back%CH 8",\
"[32] Back%CH 9",\
"[32] Back%CH 10",\
"[32] Back%CH 11",\
"[32] Back%CH 12",\
"[32] Back%CH 13",\
"[32] Back%CH 14",\
"[32] Back%CH 15",\
"[32] Left%CH 16",\
"[32] Left%CH 17",\
"[32] Left%CH 18",\
"[32] Left%CH 19",\
"[32] Left%CH 20",\
"[32] Left%CH 21",\
"[32] Left%CH 22",\
"[32] Left%CH 23",\
"[32] Right%CH 24",\
"[32] Right%CH 25",\
"[32] Right%CH 26",\
"[32] Right%CH 27",\
"[32] Right%CH 28",\
"[32] Right%CH 29",\
"[32] Right%CH 30",\
"[32] Right%CH 31",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"[32] Slot2%CH",\
"Update Threshold Measured = FLOAT[64] :",\
"[0] 0.5",\
"[1] 0.5",\
"[2] 0.5",\
"[3] 0.5",\
"[4] 0.5",\
"[5] 0.5",\
"[6] 0.5",\
"[7] 0.5",\
"[8] 0.5",\
"[9] 0.5",\
"[10] 0.5",\
"[11] 0.5",\
"[12] 0.5",\
"[13] 0.5",\
"[14] 0.5",\
"[15] 0.5",\
"[16] 0.5",\
"[17] 0.5",\
"[18] 0.5",\
"[19] 0.5",\
"[20] 0.5",\
"[21] 0.5",\
"[22] 0.5",\
"[23] 0.5",\
"[24] 0.5",\
"[25] 0.5",\
"[26] 0.5",\
"[27] 0.5",\
"[28] 0.5",\
"[29] 0.5",\
"[30] 0.5",\
"[31] 0.5",\
"[32] 0.5",\
"[33] 0.5",\
"[34] 0.5",\
"[35] 0.5",\
"[36] 0.5",\
"[37] 0.5",\
"[38] 0.5",\
"[39] 0.5",\
"[40] 0.5",\
"[41] 0.5",\
"[42] 0.5",\
"[43] 0.5",\
"[44] 0.5",\
"[45] 0.5",\
"[46] 0.5",\
"[47] 0.5",\
"[48] 0.5",\
"[49] 0.5",\
"[50] 0.5",\
"[51] 0.5",\
"[52] 0.5",\
"[53] 0.5",\
"[54] 0.5",\
"[55] 0.5",\
"[56] 0.5",\
"[57] 0.5",\
"[58] 0.5",\
"[59] 0.5",\
"[60] 0.5",\
"[61] 0.5",\
"[62] 0.5",\
"[63] 0.5",\
"Update Threshold Current = FLOAT[64] :",\
"[0] 0.5",\
"[1] 0.5",\
"[2] 0.5",\
"[3] 0.5",\
"[4] 0.5",\
"[5] 0.5",\
"[6] 0.5",\
"[7] 0.5",\
"[8] 0.5",\
"[9] 0.5",\
"[10] 0.5",\
"[11] 0.5",\
"[12] 0.5",\
"[13] 0.5",\
"[14] 0.5",\
"[15] 0.5",\
"[16] 0.5",\
"[17] 0.5",\
"[18] 0.5",\
"[19] 0.5",\
"[20] 0.5",\
"[21] 0.5",\
"[22] 0.5",\
"[23] 0.5",\
"[24] 0.5",\
"[25] 0.5",\
"[26] 0.5",\
"[27] 0.5",\
"[28] 0.5",\
"[29] 0.5",\
"[30] 0.5",\
"[31] 0.5",\
"[32] 0.5",\
"[33] 0.5",\
"[34] 0.5",\
"[35] 0.5",\
"[36] 0.5",\
"[37] 0.5",\
"[38] 0.5",\
"[39] 0.5",\
"[40] 0.5",\
"[41] 0.5",\
"[42] 0.5",\
"[43] 0.5",\
"[44] 0.5",\
"[45] 0.5",\
"[46] 0.5",\
"[47] 0.5",\
"[48] 0.5",\
"[49] 0.5",\
"[50] 0.5",\
"[51] 0.5",\
"[52] 0.5",\
"[53] 0.5",\
"[54] 0.5",\
"[55] 0.5",\
"[56] 0.5",\
"[57] 0.5",\
"[58] 0.5",\
"[59] 0.5",\
"[60] 0.5",\
"[61] 0.5",\
"[62] 0.5",\
"[63] 0.5",\
"Voltage Limit = FLOAT[64] :",\
"[0] 100",\
"[1] 100",\
"[2] 100",\
"[3] 100",\
"[4] 100",\
"[5] 100",\
"[6] 100",\
"[7] 100",\
"[8] 100",\
"[9] 100",\
"[10] 100",\
"[11] 100",\
"[12] 100",\
"[13] 100",\
"[14] 100",\
"[15] 100",\
"[16] 100",\
"[17] 100",\
"[18] 100",\
"[19] 100",\
"[20] 100",\
"[21] 100",\
"[22] 100",\
"[23] 100",\
"[24] 100",\
"[25] 100",\
"[26] 100",\
"[27] 100",\
"[28] 100",\
"[29] 100",\
"[30] 100",\
"[31] 100",\
"[32] 100",\
"[33] 100",\
"[34] 100",\
"[35] 100",\
"[36] 100",\
"[37] 100",\
"[38] 100",\
"[39] 100",\
"[40] 100",\
"[41] 100",\
"[42] 100",\
"[43] 100",\
"[44] 100",\
"[45] 100",\
"[46] 100",\
"[47] 100",\
"[48] 100",\
"[49] 100",\
"[50] 100",\
"[51] 100",\
"[52] 100",\
"[53] 100",\
"[54] 100",\
"[55] 100",\
"[56] 100",\
"[57] 100",\
"[58] 100",\
"[59] 100",\
"[60] 100",\
"[61] 100",\
"[62] 100",\
"[63] 100",\
"Current Limit = FLOAT[64] :",\
"[0] 100",\
"[1] 100",\
"[2] 100",\
"[3] 100",\
"[4] 100",\
"[5] 100",\
"[6] 100",\
"[7] 100",\
"[8] 100",\
"[9] 100",\
"[10] 100",\
"[11] 100",\
"[12] 100",\
"[13] 100",\
"[14] 100",\
"[15] 100",\
"[16] 100",\
"[17] 100",\
"[18] 100",\
"[19] 100",\
"[20] 100",\
"[21] 100",\
"[22] 100",\
"[23] 100",\
"[24] 100",\
"[25] 100",\
"[26] 100",\
"[27] 100",\
"[28] 100",\
"[29] 100",\
"[30] 100",\
"[31] 100",\
"[32] 100",\
"[33] 100",\
"[34] 100",\
"[35] 100",\
"[36] 100",\
"[37] 100",\
"[38] 100",\
"[39] 100",\
"[40] 100",\
"[41] 100",\
"[42] 100",\
"[43] 100",\
"[44] 100",\
"[45] 100",\
"[46] 100",\
"[47] 100",\
"[48] 100",\
"[49] 100",\
"[50] 100",\
"[51] 100",\
"[52] 100",\
"[53] 100",\
"[54] 100",\
"[55] 100",\
"[56] 100",\
"[57] 100",\
"[58] 100",\
"[59] 100",\
"[60] 100",\
"[61] 100",\
"[62] 100",\
"[63] 100",\
"Trip Time = FLOAT[64] :",\
"[0] 1000",\
"[1] 1000",\
"[2] 1000",\
"[3] 1000",\
"[4] 1000",\
"[5] 1000",\
"[6] 1000",\
"[7] 1000",\
"[8] 1000",\
"[9] 1000",\
"[10] 1000",\
"[11] 1000",\
"[12] 1000",\
"[13] 1000",\
"[14] 1000",\
"[15] 1000",\
"[16] 1000",\
"[17] 1000",\
"[18] 1000",\
"[19] 1000",\
"[20] 1000",\
"[21] 1000",\
"[22] 1000",\
"[23] 1000",\
"[24] 1000",\
"[25] 1000",\
"[26] 1000",\
"[27] 1000",\
"[28] 1000",\
"[29] 1000",\
"[30] 1000",\
"[31] 1000",\
"[32] 1000",\
"[33] 1000",\
"[34] 1000",\
"[35] 1000",\
"[36] 1000",\
"[37] 1000",\
"[38] 1000",\
"[39] 1000",\
"[40] 1000",\
"[41] 1000",\
"[42] 1000",\
"[43] 1000",\
"[44] 1000",\
"[45] 1000",\
"[46] 1000",\
"[47] 1000",\
"[48] 1000",\
"[49] 1000",\
"[50] 1000",\
"[51] 1000",\
"[52] 1000",\
"[53] 1000",\
"[54] 1000",\
"[55] 1000",\
"[56] 1000",\
"[57] 1000",\
"[58] 1000",\
"[59] 1000",\
"[60] 1000",\
"[61] 1000",\
"[62] 1000",\
"[63] 1000",\
"Ramp Step = FLOAT[64] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"[16] 1",\
"[17] 1",\
"[18] 1",\
"[19] 1",\
"[20] 1",\
"[21] 1",\
"[22] 1",\
"[23] 1",\
"[24] 1",\
"[25] 1",\
"[26] 1",\
"[27] 1",\
"[28] 1",\
"[29] 1",\
"[30] 1",\
"[31] 1",\
"[32] 1",\
"[33] 1",\
"[34] 1",\
"[35] 1",\
"[36] 1",\
"[37] 1",\
"[38] 1",\
"[39] 1",\
"[40] 1",\
"[41] 1",\
"[42] 1",\
"[43] 1",\
"[44] 1",\
"[45] 1",\
"[46] 1",\
"[47] 1",\
"[48] 1",\
"[49] 1",\
"[50] 1",\
"[51] 1",\
"[52] 1",\
"[53] 1",\
"[54] 1",\
"[55] 1",\
"[56] 1",\
"[57] 1",\
"[58] 1",\
"[59] 1",\
"[60] 1",\
"[61] 1",\
"[62] 1",\
"[63] 1",\
"Step Time = FLOAT[64] :",\
"[0] 100",\
"[1] 100",\
"[2] 100",\
"[3] 100",\
"[4] 100",\
"[5] 100",\
"[6] 100",\
"[7] 100",\
"[8] 100",\
"[9] 100",\
"[10] 100",\
"[11] 100",\
"[12] 100",\
"[13] 100",\
"[14] 100",\
"[15] 100",\
"[16] 100",\
"[17] 100",\
"[18] 100",\
"[19] 100",\
"[20] 100",\
"[21] 100",\
"[22] 100",\
"[23] 100",\
"[24] 100",\
"[25] 100",\
"[26] 100",\
"[27] 100",\
"[28] 100",\
"[29] 100",\
"[30] 100",\
"[31] 100",\
"[32] 100",\
"[33] 100",\
"[34] 100",\
"[35] 100",\
"[36] 100",\
"[37] 100",\
"[38] 100",\
"[39] 100",\
"[40] 100",\
"[41] 100",\
"[42] 100",\
"[43] 100",\
"[44] 100",\
"[45] 100",\
"[46] 100",\
"[47] 100",\
"[48] 100",\
"[49] 100",\
"[50] 100",\
"[51] 100",\
"[52] 100",\
"[53] 100",\
"[54] 100",\
"[55] 100",\
"[56] 100",\
"[57] 100",\
"[58] 100",\
"[59] 100",\
"[60] 100",\
"[61] 100",\
"[62] 100",\
"[63] 100",\
"",\
NULL }

#endif

#ifndef EXCL_ASUM

#define ASUM_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} ASUM_COMMON;

#define ASUM_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 4",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 255",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] xenon01.triumf.ca",\
"Frontend name = STRING : [32] FeAsum",\
"Frontend file name = STRING : [256] feasum.c",\
"",\
NULL }

#define ASUM_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      struct {
        char      mscb_device[32];
        char      mscb_pwd[32];
        INT       base_address;
      } dd;
    } asum_device;
  } devices;
  char      names[32];
  float     update_threshold_measured;
} ASUM_SETTINGS;

#define ASUM_SETTINGS_STR(_name) char *_name[] = {\
"[Devices/Asum Device/DD]",\
"MSCB Device = STRING : [32] mscb019",\
"MSCB Pwd = STRING : [32] ",\
"Base Address = INT : 65280",\
"",\
"[.]",\
"Names = STRING : [32] Default%CH 0",\
"Update Threshold Measured = FLOAT : 1",\
"",\
NULL }

#endif

