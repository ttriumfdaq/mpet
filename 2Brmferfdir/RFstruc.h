typedef struct {
  struct {
    int    controlmng;
    struct {
      double    ssweepdur;
      double    sstartfreq;
      double    sendfreq;
      double    srfamplitude;
    } sweepmode;
    struct {
      double    brfamplitude;
      double    bburstfreq;
      double    bnumcycles;
    } burstmode;
    struct {
      double    lbnumsteps;
      double    lbnumcycles;
      double    lbrfamplitude;
      double    lbburstfreq;
    } laddemode;
    struct {
      double    fmsweep;
      double    fmstartfreq;
      double    fmendfreq;
      double    fmrfamplitude;
    } fmmode;
  } RFX;


