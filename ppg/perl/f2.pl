#!/usr/bin/perl -w
#
# Run this file e.g.
# /home/titan/online/titan/try.pl  nsample 1490.010000
#                             Input Parameters:
#                             
#                                             
# fplot.pl 
#         
#         number to plot
#         max time range (x axis)
use strict;
our ( $nsample, $max_time ) = @ARGV;# input parameters
our $nparam=2; # need 2 input params
my $file="/home/mpet/online/da816-ch0";
my @last;
my @volt;
my ($i,$j);
my ($count,$chan,$filename);
$|=1; # flush output buffers
print   "gnuplot.pl  starting with parameters:  \n";
print    "  max_time = $max_time  number of samples=$nsample  \n";
print    "  filename = $file  \n";
$count = @ARGV;
print "num arguments = $count\n";
if($count != $nparam){ die "not enough arguments\n";}
$chan = 0;
while ($chan < 8)
{
  $filename= "$file$chan.log";
  print "filename: $filename\n";


  open FILE,$filename or die $!;
  while (<FILE>)
  {
    push @last, $_; # add to the end
    shift @last if @last > $nsample; # remove line from beginning
  }
#  print "last $nsample lines:\n",@last;
  $count=0;
  foreach $_ (@last)
  {
    if(/Off:([\d]+).*Volt:([\d\-\.]+)/)
    {
	unless($count == $1) {die "incorrect sample number; got $1 expect $count";}
#	print "sample=$1 Volt=$2\n";
	$volt[$chan][$count]=$2;
	print "volt[$chan]=$volt[$chan][$count]\n";
	$count++;
    }
   }
   $chan++;
}
my $gcol = '' ;
open (IN,"ppgload/titan.dat") or die "Can't open ppgload/titan.dat\n" ;
$_ = <IN> ; chomp ; tr/ / /s ;
my @col = split(/ /,$_) ;
for ($i=0;$i<@col;$i++) {
  if ($col[$i] =~ /AWGAM/) { $gcol = $i ; last ; }
}
print "AWGAM col $gcol\n" ;
my $p ; my $p0='' ; $i = 0 ; my @T ;
while (<IN>) {
  chomp ; tr/ / /s ;
  @col = split(/ /) ;
  $p = $col[$gcol] ;
  if ($p ne $p0 and $p) { 
    print "$col[0] $col[$gcol]\n" ;
    $T[$i] = $col[0] ; $i++ ;
  }
  $p0 = $p ;
}

#  SPARE(ch15) TDCBLOCK(ch14) TDCGATE(ch13) AWGAM(ch11) RFTRIG2(ch5) EGUN(ch2) LSTEP
#0.0000       1 1 0 0 0 0 0  # STDPULSE1,TDCBLK_S0



open (OUT,">f2.dat") or die "Can't open f2.dat\n" ;
for ($j=0;$j<$nsample-1;$j++) {
  print OUT "$j $T[$j] " ;
  for ($i=0;$i<8;$i++) {
    print OUT "$volt[$i][$j] " ;
  }
  print OUT "\n" ;
}


