/********************************************************************\

  Name:         RF_gpib_310_string.c
  Created by:   Brian Lee/Pierre


  Contents:     Application specific (user) part of
                Midas Slow Control Bus protocol
                for Agilent RF Generators (Agilent 33220A)

  $Id: RF_gpib_310_string.c 53 2008-08-06 17:54:43Z midas $

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mscbemb.h"

//Control definitions
#define CONTROL_LADDERSTEP    (1<<0)
#define CONTROL_SWEEP         (1<<1)
#define CONTROL_SINEBURST     (1<<2)
#define CONTROL_LADDERBURST   (1<<3)
#define CONTROL_FM            (1<<4) 
#define CONTROL_AM            (1<<5) 
#define CONTROL_NOT_USED2     (1<<6) 

//Currently, 33220A and 33250A are supported
#define CONDITION_INTERNAL      (0x1)
#define CONDITION_MODEL_33250A  (0x2)

//User defined Definitions
#define CHANGE_ON 1
#define CHANGE_OFF 2

// Node Name
char code node_name[] = "RF310_xx";
char idata svn_rev_code[] = "$Rev: 53 $";

/* declare number of sub-addresses to framework */
unsigned char idata _n_sub_addr = 1;

<<<<<<< .mine
char xdata str[256];
=======
char xdata str[300];
>>>>>>> .r54
extern SYS_INFO sys_info;

/*---- Define channels and configuration parameters returned to
       the CMD_GET_INFO command                                 ----*/

/* data buffer (mirrored in EEPROM) */
struct
{
  unsigned char control;
  unsigned char status;
  unsigned char condition;
  unsigned char gpib_adr;
  float         rfAmp;
  float         sweepDur;
  int           numCyc;
  int           numSteps;
  char          fCenter[32]; 
  char          fStart[32]; 
  char          fEnd[32]; 
  char          fBurst[32]; 
  char          fMod[32]; 
  char          faDepth[32]; 
} xdata user_data;

MSCB_INFO_VAR code vars[] = {
   1, UNIT_BYTE,        0, 0,          0, "Control",  &user_data.control,    // 0
   1, UNIT_BYTE,        0, 0,          0, "Status",   &user_data.status,     // 1
   1, UNIT_BYTE,        0, 0,          0, "Setting",  &user_data.condition,  // 2
   1, UNIT_BYTE,        0, 0,          0, "GPIB Adr", &user_data.gpib_adr,   // 3
   4, UNIT_VOLT,PRFX_MILLI,0,MSCBF_FLOAT, "rfAmp"   , &user_data.rfAmp,      // 4
   4, UNIT_SECOND,      0, 0,MSCBF_FLOAT, "sweepDur", &user_data.sweepDur,   // 5
   2, UNIT_COUNT,       0, 0,          0, "numCyc"  , &user_data.numCyc,     // 6
   2, UNIT_COUNT,       0, 0,          0, "numSteps", &user_data.numSteps,   // 7
  32, UNIT_STRING,      0, 0,          0, "fCenter" , &user_data.fCenter[0], // 8 
  32, UNIT_STRING,      0, 0,          0, "fStart"  , &user_data.fStart[0],  // 9 
  32, UNIT_STRING,      0, 0,          0, "fEnd"    , &user_data.fEnd[0],    // 10 
  32, UNIT_STRING,      0, 0,          0, "fBurst"  , &user_data.fBurst[0],  // 11 
  32, UNIT_STRING,      0, 0,          0, "fMod"    , &user_data.fMod[0],    // 12 
  32, UNIT_STRING,      0, 0,          0, "faDepth" , &user_data.faDepth[0], // 13 
   0
};

MSCB_INFO_VAR *variables = vars;

/********************************************************************\

  Application specific init and inout/output routines

\********************************************************************/

/* 8 data bits to LPT */
#define GPIB_DATA        P2

/* GPIB control/status bits DB40 */
sbit GPIB_EOI  = P1 ^ 1;         // Pin 5
sbit GPIB_DAV  = P1 ^ 2;         // Pin 6
sbit GPIB_NRFD = P1 ^ 3;         // Pin 7
sbit GPIB_NDAC = P1 ^ 4;         // Pin 8
sbit GPIB_IFC  = P1 ^ 5;         // Pin 9
sbit GPIB_SRQ  = P1 ^ 6;         // Pin 10
sbit GPIB_ATN  = P1 ^ 7;         // Pin 11
sbit GPIB_REM  = P1 ^ 0;         // Pin 17

sbit BUF_CLE   = P0 ^ 7;
sbit BUF_DATAE = P3 ^ 4;

#pragma NOAREGS

void user_write(unsigned char index) reentrant;
unsigned char send(unsigned char adr, char *str);
unsigned char send_byte(unsigned char b);
void fSweep(void);
void sineBurst(void);
void ladderBurst(void);
void fm(void);
void rf_Init(void);
void hardWareUpdate(void);

char flag = CHANGE_OFF;
char kFlag = 0;
bit firstTime = 1;

/*---- User init function ------------------------------------------*/

void user_init(unsigned char init)
{
   int i;
   /* Format the SVN and store this code SVN revision into the system */
   for (i=0;i<4;i++) {
     if (svn_rev_code[6+i] < 48) {
       svn_rev_code[6+i] = '0';
     }
   }
   sys_info.svn_revision = (svn_rev_code[6]-'0')*1000+
     (svn_rev_code[7]-'0')*100+
     (svn_rev_code[8]-'0')*10+
     (svn_rev_code[9]-'0');

     /* set initial state of lines */
   GPIB_DATA = 0xFF;
   GPIB_EOI = 1;
   GPIB_DAV = 1;
   GPIB_NRFD = 1;
   GPIB_NDAC = 1;
   GPIB_IFC = 1;
   GPIB_SRQ = 1;
   GPIB_ATN = 1;
   GPIB_REM = 1;

   BUF_CLE   = 0;   // For the 121 rev 2 of the GPIB
   BUF_DATAE = 0;

   /* initialize GPIB */
   GPIB_IFC = 0;
   delay_ms(1);
   GPIB_IFC = 1;

   GPIB_ATN = 0;
   send_byte(0x14);             // DCL
   GPIB_ATN = 1;

   if (init) { 
      user_data.gpib_adr = 10; 
      user_data.status = 0; 
    user_data.condition = 0x2;   // 33250A 
    user_data.rfAmp = 500;       //mV 
    user_data.sweepDur = 8.0;      // in seconds 
    user_data.numCyc = 4; 
    user_data.numSteps = 4; 
    sprintf(user_data.fCenter, "5000000.0");  //in Hz 
    sprintf(user_data.fStart, "100000.0");   //in Hz 
    sprintf(user_data.fEnd,   "200000.0");   //in Hz 
    sprintf(user_data.fBurst, "1000000.0");  //in Hz 
    sprintf(user_data.fMod , "5.0");  //in Hz 
    sprintf(user_data.faDepth, "50.0");  //in Hz/% 
    sys_info.node_addr = 0x01; 
   } 
 

  //Initialize RF Generators
  strcpy(str, " ");
  rf_Init();
}

/*---- User write function -----------------------------------------*/

void user_write(unsigned char index) reentrant
{
   if(index == 0) flag = CHANGE_ON;

  return;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
  if(index);

   return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
   /* echo input data */
   data_out[0] = data_in[0];
   data_out[1] = data_in[1];
   return 2;
}

/*---- Functions for GPIB port -------------------------------------*/

unsigned char send_byte(unsigned char b)
{
   unsigned int i;

   yield();

   /* wait for NRFD go high */
   for (i = 0; i < 1000; i++)
      if (GPIB_NRFD == 1)
         break;

   if (GPIB_NRFD == 0)
      return 0;

   GPIB_DATA = ~b;              // negate
   delay_us(1);                 // let signals settle
   GPIB_DAV = 0;

   /* wait for NDAC go high */
   for (i = 0; i < 1000; i++)
      if (GPIB_NDAC == 1)
         break;

   if (GPIB_NDAC == 0) {
      GPIB_DAV = 1;
      GPIB_DATA = 0xFF;
      return 0;                 // timeout
   }

   GPIB_DAV = 1;
   GPIB_DATA = 0xFF;            // prepare for input

   /* wait for NRFD go high */
   for (i = 0; i < 1000; i++)
      if (GPIB_NRFD == 1)
         break;

   if (GPIB_NRFD == 0)
      return 0;

   return 1;
}

/*------------------------------------------------------------------*/
void send_header(unsigned char adr)
{

   if (user_data.condition & CONDITION_MODEL_33250A)
     delay_ms(100); //for 33250A

  /*---- address cycle ----*/

   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   send_byte(0x20 | adr);       // listen device
   send_byte(0x40 | 21);        // talk 21
   GPIB_ATN = 1;                // remove attention

}

/*------------------------------------------------------------------*/
unsigned char send_content(char *str)
{
  unsigned char i;

  /*---- data cycles ----*/
   for (i = 0; i < strlen(str); i++)
      if (send_byte(str[i]) == 0)
         return 0;

   return i;
}

/*------------------------------------------------------------------*/
unsigned char send(unsigned char adr, char *str)
{
   unsigned char i;

    if (user_data.condition & CONDITION_MODEL_33250A)
       delay_ms(100); //for 33250A

  /*---- address cycle ----*/

   GPIB_ATN = 0;                // assert attention
   send_byte(0x3F);             // unlisten
   send_byte(0x5F);             // untalk
   send_byte(0x20 | adr);       // listen device
   send_byte(0x40 | 21);        // talk 21
   GPIB_ATN = 1;                // remove attention

  /*---- data cycles ----*/

   for (i = 0; str[i] > 0; i++)
      if (send_byte(str[i]) == 0)
         return 0;

   GPIB_EOI = 0;
   send_byte(0x0A);             // NL
   GPIB_EOI = 1;

   return i;
}

<<<<<<< .mine
/*------------------------------------------------------------------*/ 
/* User Defined Function Definitions */ 
/**
rf_init
*/
void rf_Init(void) 
{ 
  send(user_data.gpib_adr, "OUTPut OFF"); 
  send(user_data.gpib_adr, "*CLS"); 
  send(user_data.gpib_adr, "OUTPut:LOAD MAXimum"); 
  if (user_data.condition & CONDITION_INTERNAL) 
    send(user_data.gpib_adr, "TRIG:SOUR IMM"); 
  else 
    send(user_data.gpib_adr, "TRIG:SOUR EXT"); 
  send(user_data.gpib_adr, "TRIGger:SLOPe POSitive"); 
 
  //turn all the states off to avoid beeping sounds later 
  send(user_data.gpib_adr, "SWEep:STATe OFF"); 
  send(user_data.gpib_adr, "BURSt:STATe OFF"); 
  send(user_data.gpib_adr, "FM:STATe OFF"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
fSweep setting, fStart, fEnd, sweepDur, rfAmp
*/
void fSweep(void) // (float sweepDur, char *fStart, char *fEnd, float rfAmp) 
{ 
  rf_Init(); 
 
  sprintf(str, "FREQuency:STARt %s", user_data.fStart); 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "FREQuency:STOP %s", user_data.fEnd); 
  send(user_data.gpib_adr, str); 
 
  send(user_data.gpib_adr, "SWEep:SPACing LINear"); 
 
  sprintf(str, "SWEep:TIME %e", user_data.sweepDur); 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "VOLTAGE %e", (user_data.rfAmp / 1000)); 
  send(user_data.gpib_adr, str); 
 
  //finally, turn on the Sweep Mode 
  send(user_data.gpib_adr, "SWEep:STATe ON"); 
  send(user_data.gpib_adr, "OUTPut ON"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
sinBurst setting, fBurst, rfAmp, numcyc
*/
void sineBurst(void) // (char *fBurst, int numCyc, float rfAmp) 
{ 
  rf_Init(); 
 
  send(user_data.gpib_adr, "BURSt:MODE TRIGgered"); 
 
  sprintf(str, "APPLy:SINusoid %s, %e", user_data.fBurst, (user_data.rfAmp / 1000)); 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "BURSt:NCYCles %d", user_data.numCyc); 
  send(user_data.gpib_adr, str); 
 
  //send(user_data.gpib_adr, "BURSt:INTernal:PERiod 10e-3"); 
 
  //finally, turn on the Sine Burst Mode 
  send(user_data.gpib_adr, "BURSt:STATe ON"); 
  send(user_data.gpib_adr, "OUTPut ON"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
ladderBurst setting, -1..1 in numSteps, numCycl=1 (should be), rfAmp
*/
void ladderBurst(void) // (int numSteps, int numCyc, float rfAmp, char *fBurst) 
{ 
  int i = 0; 
  char xdata bufstr[512];
  char xdata buffer[16]; 
 
  rf_Init(); 
 
  memset(bufstr, 0, sizeof(bufstr));
  strcpy(bufstr, "DATA VOLATILE "); 
  for(i = 0; i < user_data.numSteps; i++) 
  { 
    sprintf(buffer, ", %8.4f", (double) ((((double)(2 * i)) / ((double)(user_data.numSteps -1))) - 1)); 
    strcat(bufstr, buffer); 
  } 
 
  if(!firstTime) 
  { 
    send(user_data.gpib_adr, "FUNCtion:USER EXP_RISE"); 
    send(user_data.gpib_adr, "DATA:DELete TWISTLADDER"); 
  } 
  //delay_ms(500); 
  #ifdef AGILENT_33250A 
  //delay_ms(500); //additional delay fo 33250A 
  #endif 
 
  send(user_data.gpib_adr, bufstr); 
  delay_ms(500); 
  yield(); 
  send(user_data.gpib_adr, "DATA:COPY TWISTLADDER"); 
  delay_ms(500); 
  if(firstTime) firstTime = 0; 
  send(user_data.gpib_adr, "FUNCtion:USER TWISTLADDER"); 
  send(user_data.gpib_adr, "FUNCtion USER"); 
 
  sprintf(str, "BURSt:NCYCles %d", user_data.numCyc); 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "APPLy:USER %s, %e", user_data.fMod, (user_data.rfAmp / 1000)); 
  send(user_data.gpib_adr, str); 
 
  //finally, turn on the Ladder Burst Mode 
  send(user_data.gpib_adr, "BURSt:STATe ON"); 
  send(user_data.gpib_adr, "OUTPut ON"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
FM setting, fCenter, fMod, faDepth, rfAmp
*/
void fm(void) // (char *fBurst, float rfAmp) 
{ 
  rf_Init(); 
 
  if (user_data.condition & CONDITION_INTERNAL) 
     send(user_data.gpib_adr, "FM:SOUR INT"); 
   else 
     send(user_data.gpib_adr, "FM:SOUR EXT"); 
 
  sprintf(str, "FREQ %s", user_data.fCenter);  // Center Freq
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "FM:INT:FREQ %s", user_data.fMod);   // Modulation Freq
  send(user_data.gpib_adr, str); 
  
  sprintf(str, "FM:DEViation %s", user_data.faDepth); // max Freq deviation 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "VOLTAGE %e", (user_data.rfAmp / 1000)); // Amplitude
  send(user_data.gpib_adr, str); 
 
  //finally, turn on the Sweep Mode 
  send(user_data.gpib_adr, "FM:STATe ON"); 
  send(user_data.gpib_adr, "OUTPut ON"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
AM setting, fCenter, fMod, faDepth, rfAmp
*/
void am(void) // (char *fBurst, float rfAmp) 
{ 
  rf_Init(); 
 
  if (user_data.condition & CONDITION_INTERNAL) {
    send(user_data.gpib_adr, "AM:SOUR INT"); 
  }
  else {
    send(user_data.gpib_adr, "AM:SOUR EXT"); 
  }
  
  sprintf(str, "FREQ %s", user_data.fCenter);  // Carrier Freq 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "AM:INT:FREQ %s", user_data.fMod);   // Modulation Freq
  send(user_data.gpib_adr, str); 
  
  sprintf(str, "AM:DEPTh %s", user_data.faDepth);  // Amplitude variation in %
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "VOLTAGE %e", (user_data.rfAmp / 1000));  // Center amplitude
  send(user_data.gpib_adr, str); 
 
  //finally, turn on the Sweep Mode 
  send(user_data.gpib_adr, "AM:STATe ON"); 
  send(user_data.gpib_adr, "OUTPut ON"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
Mode selection: sweep, sineburst, ladderburst, FM, AM
*/
void hardWareUpdate(void) 
{ 
  if(flag == CHANGE_ON) { 
    led_blink(0, 1, 50);  //Red
    if(user_data.control & CONTROL_SWEEP) { 
      fSweep(); 
    } 
 	else if(user_data.control & CONTROL_SINEBURST) { 
      sineBurst(); 
    } 
    else if(user_data.control & CONTROL_LADDERBURST) { 
      ladderBurst(); 
    } 
    else if(user_data.control & CONTROL_FM) { 
      fm(); 
    } 
    else if(user_data.control & CONTROL_AM) { 
      am(); 
    } 
    flag = CHANGE_OFF; 
    user_data.status = user_data.control; 
    user_data.control = 0; 
  } 
} 
 
/*---- User loop function ------------------------------------------*/ 
/**
main user loop
*/
void user_loop(void) 
{ 
  led_blink(1, 1, 200);  // Green
  hardWareUpdate();
} 
=======
/*------------------------------------------------------------------*/
void send_trailer(void)
{

   GPIB_EOI = 0;
   send_byte(0x0A);             // NL
   GPIB_EOI = 1;
}

/*------------------------------------------------------------------*/ 
/* User Defined Function Definitions */ 
/**
rf_init
*/
void rf_Init(void) 
{ 
  send(user_data.gpib_adr, "OUTPut OFF"); 
  send(user_data.gpib_adr, "*CLS"); 
  send(user_data.gpib_adr, "OUTPut:LOAD MAXimum"); 
  if (user_data.condition & CONDITION_INTERNAL) 
    send(user_data.gpib_adr, "TRIG:SOUR IMM"); 
  else 
    send(user_data.gpib_adr, "TRIG:SOUR EXT"); 
  send(user_data.gpib_adr, "TRIGger:SLOPe POSitive"); 
 
  //turn all the states off to avoid beeping sounds later 
  send(user_data.gpib_adr, "SWEep:STATe OFF"); 
  send(user_data.gpib_adr, "BURSt:STATe OFF"); 
  send(user_data.gpib_adr, "FM:STATe OFF"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
fSweep setting, fStart, fEnd, sweepDur, rfAmp
*/
void fSweep(void) // (float sweepDur, char *fStart, char *fEnd, float rfAmp) 
{ 
  rf_Init(); 
 
  sprintf(str, "FREQuency:STARt %s", user_data.fStart); 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "FREQuency:STOP %s", user_data.fEnd); 
  send(user_data.gpib_adr, str); 
 
  send(user_data.gpib_adr, "SWEep:SPACing LINear"); 
 
  sprintf(str, "SWEep:TIME %e", user_data.sweepDur); 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "VOLTAGE %e", (user_data.rfAmp / 1000)); 
  send(user_data.gpib_adr, str); 
 
  //finally, turn on the Sweep Mode 
  send(user_data.gpib_adr, "SWEep:STATe ON"); 
  send(user_data.gpib_adr, "OUTPut ON"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
sinBurst setting, fBurst, rfAmp, numcyc
*/
void sineBurst(void) // (char *fBurst, int numCyc, float rfAmp) 
{ 
  rf_Init(); 
 
  send(user_data.gpib_adr, "BURSt:MODE TRIGgered"); 
 
  sprintf(str, "APPLy:SINusoid %s, %e", user_data.fBurst, (user_data.rfAmp / 1000)); 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "BURSt:NCYCles %d", user_data.numCyc); 
  send(user_data.gpib_adr, str); 
 
  //send(user_data.gpib_adr, "BURSt:INTernal:PERiod 10e-3"); 
 
  //finally, turn on the Sine Burst Mode 
  send(user_data.gpib_adr, "BURSt:STATe ON"); 
  send(user_data.gpib_adr, "OUTPut ON"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
ladderBurst setting, -1..1 in numSteps, numCycl=1 (should be), rfAmp
*/
void ladderBurst(void) // (int numSteps, int numCyc, float rfAmp, char *fBurst) 
{ 
  int i = 0; 
 
  rf_Init(); 
 
  send_header(user_data.gpib_adr);

  memset(str, 0 , sizeof(str));
  strcpy(str, "DATA VOLATILE ");
  send_content(str);

  for(i = 0; i < user_data.numSteps; i++) { 
    sprintf(str, " ,%4.2f ", (double) ((((double)(2 * i)) / ((double)(user_data.numSteps -1))) - 1)); 
	send_content(str); 
    led_blink(0, 1, 25);  // Green
    delay_ms(25);
  } 
 
  send_trailer(); 
  
  yield(); 
  send(user_data.gpib_adr, "DATA:COPY TWISTLADDER"); 
  delay_ms(500); 

  send(user_data.gpib_adr, "FUNCtion:USER TWISTLADDER"); 
  send(user_data.gpib_adr, "FUNCtion USER"); 
 
  sprintf(str, "BURSt:NCYCles %d", user_data.numCyc); 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "APPLy:USER %s, %e", user_data.fMod, (user_data.rfAmp / 1000)); 
  send(user_data.gpib_adr, str); 
 
  //finally, turn on the Ladder Burst Mode 
  send(user_data.gpib_adr, "BURSt:STATe ON"); 
  send(user_data.gpib_adr, "OUTPut ON"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
FM setting, fCenter, fMod, faDepth, rfAmp
*/
void fm(void) // (char *fBurst, float rfAmp) 
{ 
  rf_Init(); 
 
  if (user_data.condition & CONDITION_INTERNAL) 
     send(user_data.gpib_adr, "FM:SOUR INT"); 
   else 
     send(user_data.gpib_adr, "FM:SOUR EXT"); 
 
  sprintf(str, "FREQ %s", user_data.fCenter);  // Center Freq
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "FM:INT:FREQ %s", user_data.fMod);   // Modulation Freq
  send(user_data.gpib_adr, str); 
  
  sprintf(str, "FM:DEViation %s", user_data.faDepth); // max Freq deviation 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "VOLTAGE %e", (user_data.rfAmp / 1000)); // Amplitude
  send(user_data.gpib_adr, str); 
 
  //finally, turn on the Sweep Mode 
  send(user_data.gpib_adr, "FM:STATe ON"); 
  send(user_data.gpib_adr, "OUTPut ON"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
AM setting, fCenter, fMod, faDepth, rfAmp
*/
void am(void) // (char *fBurst, float rfAmp) 
{ 
  rf_Init(); 
 
  if (user_data.condition & CONDITION_INTERNAL) {
    send(user_data.gpib_adr, "AM:SOUR INT"); 
  }
  else {
    send(user_data.gpib_adr, "AM:SOUR EXT"); 
  }
  
  sprintf(str, "FREQ %s", user_data.fCenter);  // Carrier Freq 
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "AM:INT:FREQ %s", user_data.fMod);   // Modulation Freq
  send(user_data.gpib_adr, str); 
  
  sprintf(str, "AM:DEPTh %s", user_data.faDepth);  // Amplitude variation in %
  send(user_data.gpib_adr, str); 
 
  sprintf(str, "VOLTAGE %e", (user_data.rfAmp / 1000));  // Center amplitude
  send(user_data.gpib_adr, str); 
 
  //finally, turn on the Sweep Mode 
  send(user_data.gpib_adr, "AM:STATe ON"); 
  send(user_data.gpib_adr, "OUTPut ON"); 
} 
 
/*------------------------------------------------------------------*/ 
/**
Mode selection: sweep, sineburst, ladderburst, FM, AM
*/
void hardWareUpdate(void) 
{ 
  if(flag == CHANGE_ON) { 
    led_blink(0, 1, 50);  // Green
    if(user_data.control & CONTROL_SWEEP) { 
      fSweep(); 
    } 
 	else if(user_data.control & CONTROL_SINEBURST) { 
      sineBurst(); 
    } 
    else if(user_data.control & CONTROL_LADDERBURST) { 
      ladderBurst(); 
    } 
    else if(user_data.control & CONTROL_FM) { 
      fm(); 
    } 
    else if(user_data.control & CONTROL_AM) { 
      am(); 
    } 
    flag = CHANGE_OFF; 
    user_data.status = user_data.control; 
    user_data.control = 0; 
  } 
} 
 
/*---- User loop function ------------------------------------------*/ 
/**
main user loop
*/
void user_loop(void) 
{ 
  led_blink(1, 1, 200);  // Red
  hardWareUpdate();
} 
>>>>>>> .r54
