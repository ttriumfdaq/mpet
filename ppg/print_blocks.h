/* print_blocks.h */
#ifndef  PRINT_BLOCKS_INCLUDE_H
#define  PRINT_BLOCKS_INCLUDE_H

void  print_pulse_blocks(FILE *fout );
void print_sorted_transitions(TRANSITIONS *ptrans, BOOL bp, FILE *fout);
void print_ev_step_params(INT index, BOOL print_values, FILE *fout);
void print_ev_set_params(INT index, BOOL print_values, FILE *fout);
void print_ref_blocks( FILE *fout);
void print_transitions(TRANSITIONS *ptrans, FILE *fout);
void print_rfsweep_params(char * name, INT my_index);
void print_loop_params(FILE *fout);
void print_tdcblock_params(void);
void print_awg_params(void);
BOOL is_loop(INT code);
void print_sorted_loop_transitions(TRANSITIONS *ptrans, FILE *fout );
#endif
