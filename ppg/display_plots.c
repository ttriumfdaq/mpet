/* plot_pictures.c
  // Creates and plots PPG display from mpet.dat -> titan.png
  // Creates all 8 AWG plots from awgN.dat -> (awgN_<Chan>.png)

*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
//#include <math.h>
#include "midas.h"
#include "msystem.h"
#include "experim.h"
#include "tri_config.h"
#include "parameters.h"
#include "tri_config_prototypes.h"
#include "info_blocks.h"


void plot_picture(char *filename, INT nplot, double max_ms,  INT loop_flag, char *ppg_mode)
     /*	 gnuplot.pl 
	 #         filename
	 #         number to plot
	 #         max time range (x axis)	
	 #         flag
	 #         output filename
     */
{
  char perl_path[80];
  char perl_name[]="gnuplot.pl";
  char outfile[80];
  char cmd[128];
  INT status;
  
  sprintf(perl_path,"%s%s%s",settings.ppg.input.ppg_path,"ppg/perl/",perl_name);
  sprintf(outfile,"%sdata/run%d_%s.png",settings.ppg.input.ppg_path,run_number,ppg_mode);
  printf("plot_transitions: outfile=%s\n",outfile);

  sprintf(cmd,"%s %s %d %f %d %s %d",perl_path, filename, nplot, max_ms, loop_flag, outfile, run_number);
  printf("plot_picture: sending system command:\"%s\" \n",cmd);
  status =  system(cmd);
  
  printf("plot_picture: after system command, status=%d\n",status);
  if(status)
    cm_msg(MINFO,"plot_picture","could not plot picture of ppg cycle");
  
  /*  else
    {
      sprintf(cmd,"mv %stitan.png %s/run%d_titan.png",settings.ppg.input.ppg_path,data_dir,run_number );
      printf("cmd:%s\n",cmd);
      status =  system(cmd);
      if(status)
	cm_msg(MINFO,"plot_picture","could not rename titan.png ");
      else
	{
	  sprintf(cmd,"ln -s   %s/run%d_titan.png %stitan.png",data_dir,run_number,settings.ppg.input.ppg_path);
	  printf("cmd:%s\n",cmd);
	  status =  system(cmd);
	  if(status)
	    cm_msg(MINFO,"plot_picture","could not create link to titan.png ");
	}
	}*/
  return;
}



void plot_awg_picture()
{
  char plot_perlname[]="gnuplot_awg.pl"; // plots the data for each awg channel

  sprintf(perl_path,"%s%s%s",settings.ppg.input.ppg_path,"ppg/perl/",plot_perlname);
  sprintf(cmd,"%s %s %s/awg%d.dat",perl_path,
	  settings.ppg.input.ppg_path, 
	  settings.ppg.input.ppgload_path,awg_unit); // all 8 channels
  printf("plot_awg: sending system command:\"%s\" \n",cmd);
  status =  system(cmd); 
  printf("plot_awg: after system command, status=%d\n",status);
  if(status)
    {
    cm_msg(MINFO,"plot_awg","could not plot AWG data");
    return;
    }
}
