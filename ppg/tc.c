/* 
   Name: tri_config.c
   Created by: SD

   Contents: Main configuration program -  for TRIUMF Titan Experiment (mpet, ebit).
             Converts information blocks from odb 
	         writing a time/bitpattern file to be downloaded into the PPG
		 loading the AWG module(s) 
		 writing files to be plotted by gnuplot
                 etc.
 $Id: tri_config.c  $

                                         
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <errno.h>  // needed to interpret errors after "system" call
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include "midas.h"
#include "msystem.h"
#include "experim.h"
#include "tri_config.h"
#include "parameters.h"
#include "tri_config_prototypes.h"
#include "info_blocks.h"
#include <errno.h>

  /* ODB keys */
char eqp_name[32];
BOOL single_shot;
INT size;
INT error_flag=0;

FILE *tfile;
/* ODB */

TITAN_ACQ_SETTINGS settings;
HNDLE hDB, hSET, hPPG,hIn,hOut,hAWG0,hAWG1,hCycle,hLEGI;
INT block_list[MAX_BLOCKS]; // list of block types (shifted by TYPE_SHIFT) & block numbers
TRANSITIONS p_transitions[MAX_TRANSITIONS];
TRANSITIONS *ptrans;
#ifdef AUTO_TDCBLOCK
TDCBLOCK_PARAMS *ptdc;
#endif

// needed for conversion of long names to short
  const char *blockarray[]={"TRANSITION","PULSE","STDPULSE","PATTERN","DELAY","TIME_REF","BEGIN_LOOP","END_LOOP","EVSTEP","EVSET"};
  const INT  blockmin[]={3,3,3,3,3,3,5,3,5,5};
  char long_names[100][256]; // long_names
  char short_names[100][256]; // short_names
  char long_loop_names[100][256];
  char short_loop_names[100][256];
  INT nnames,nlloops;



BOOL make_plots;

BOOL gbl_skip=FALSE;
INT run_number;
char data_dir[256];

typedef struct {
   int flags;
   char pattern[32];
   int index;
} PRINT_INFO;

PRINT_INFO print_info;

#define PI_LONG      (1<<0)
#define PI_RECURSIVE (1<<1)
#define PI_VALUE     (1<<2)
#define PI_HEX       (1<<3)
#define PI_PAUSE     (1<<4)

static char data[50000];
INT  ls_line, ls_abort;

 double gbl_elapsed_time;
 static struct timeval tt0, tt1;  




INT tr_precheck(INT run_number, char *error)
{
  INT status;
  BOOL flag;

 /*  see how long it takes */
 gettimeofday(&tt0, NULL); // start gbl timer with tr_precheck
 cm_msg(MINFO,"tr_precheck","== tr_precheck starting timer");  

  /* get settings all records */
  status = settings_rec_get();/* returns -1 for fail, or SUCCESS */
  if (status < 0)
    {
      printf("tr_precheck: Error return after settings_rec_get. See odb messages for details\n");
      return status;
    }
  
  settings.ppg.output.ppg_nominal_frequency__mhz_ =  PPG_CLOCK_MHZ; // internal clock freq and/or frequency coded in header.ppg

  printf("tr_precheck: PPG is running at =%f MHz; Nominal freq  = %f MHz \n",
	 settings.ppg.input.ppg_clock__mhz_,  settings.ppg.output.ppg_nominal_frequency__mhz_);
  
  if ( (settings.ppg.output.ppg_nominal_frequency__mhz_ < 1 ) ||   
       (settings.ppg.input.ppg_clock__mhz_ <  1 )             ||
       (settings.ppg.output.ppg_nominal_frequency__mhz_ > 100)||  
       (settings.ppg.input.ppg_clock__mhz_ > 100) )
    {
      cm_msg(MERROR,"tr_precheck",
	     "illegal ppg frequency value(s): PPG clock=%dMHz  nominal value=%dMHz. Must be between 1 and 100 MHz\n",
	     settings.ppg.output.ppg_nominal_frequency__mhz_ ,  settings.ppg.input.ppg_clock__mhz_);
      return DB_INVALID_PARAM;
    }
  
   settings.ppg.output.ppg_freq_conversion_factor = 
     settings.ppg.input.ppg_clock__mhz_ / settings.ppg.output.ppg_nominal_frequency__mhz_ ;
  printf("frequency conversion factor between actual and nominal PPG clock frequency is %f\n",
	 settings.ppg.output.ppg_freq_conversion_factor);
  

  settings.ppg.output.time_slice__ms_ = 1e-3/settings.ppg.input.ppg_clock__mhz_; // converted to ms
  settings.ppg.output.minimal_delay__ms_ = 5 * settings.ppg.output.time_slice__ms_; 
  
  printf("tr_precheck: time slice =%f ms; min delay = %f ms; standard pulse width = %f ms\n",
	 settings.ppg.output.time_slice__ms_, settings.ppg.output.minimal_delay__ms_,
	 settings.ppg.input.standard_pulse_width__ms_);

 

  /*   Write the output values here in case of error prior to prestart 
       compile time of bytecode.dat will not yet be updated, of course */ 
     
  /* Update these output settings in ODB */
  size = sizeof(settings.ppg.output);
  status = db_set_record(hDB, hOut, &settings.ppg.output, size, 0); 
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"prestart","Failed to set output record (size %d) (%d)",size,status);



  /* all std pulses are of width "standard pulse width"  
     i.e.  settings.ppg.input.standard_pulse_width__ms_  */

  if(debug)printf("tr_precheck: standard pulse width is %fms\n", settings.ppg.input.standard_pulse_width__ms_   );
  
  nnames=nlloops=0; // initialize for short blocks
  init_blocks(); // initialize
  set_awg_params(); // setup constants and defaults for awg

 
  define_t0_ref();  /* defines a dummy block with T0 as the first reference */




  /* we can't add this automatically any more... need pulse(s) prior to scan loop   
  status = auto_add_loop(1,  settings.ppg.input.number_of_cycles ,ptrans); // add begin_scan loop
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"tr_precheck","error adding begin of scan loop");
      return status;
    }
  */
  status = define_trans0(); /* start with a dummy pattern at T0 */
  if( status != SUCCESS)
    {
      printf("tr_precheck: error from define_trans0\n");
      return status ;
    }

  print_transitions(ptrans,stdout);
  //  if(hLEGI)get_input_data(hLEGI);

  status = get_input_data(hCycle);
  if(status != SUCCESS)
    printf("tr_precheck: error from get_input_data\n");

  if(tfile)print_sorted_loop_transitions(ptrans,tfile);
   gettimeofday(&tt1, NULL);  // end time
 // compute and print the elapsed time in millisec
	      
 gbl_elapsed_time = (tt1.tv_sec - tt0.tv_sec) * 1000.0;      // sec to ms
 gbl_elapsed_time += (tt1.tv_usec - tt0.tv_usec) / 1000.0;   // us to ms
 printf("tr_precheck: returning, function took elapsed time   %f ms\n",gbl_elapsed_time);
 cm_msg(MINFO,"tr_precheck","== tr_precheck took  %f ms",gbl_elapsed_time);

  return status;
}
/*------------------------------------------------------------------*/
INT settings_rec_get(void)
{
  /* retrieve the ODB structure
     check if the rule file exists in the given dir
     return the rulefile
  */
  INT size, status,j;
  char str[128];

 /* get the record for hOut */
  size = sizeof(settings.ppg.output);
  status = db_get_record(hDB, hOut, &settings.ppg.output, &size, 0);
  if (status != DB_SUCCESS)
    {
      if(debug)printf("settings_rec_get:Failed to retrieve record for Output  (%d)",status);
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for Output  (%d)",status);
      return (-1);
    }
  
  /* get the record for hIn */
  size = sizeof(settings.ppg.input);
  status = db_get_record(hDB, hIn, &settings.ppg.input, &size, 0);
  if (status != DB_SUCCESS)
    {
      if(debug)printf("settings_rec_get:Failed to retrieve record for Input  (%d)",status);
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for Input  (%d)",status);
      return (-1);
    }

  /* get the record for AWG unit 0 */
  size = sizeof(settings.awg0);
  status = db_get_record(hDB, hAWG0, &settings.awg0, &size, 0);
  if (status != DB_SUCCESS)
    {
      if(debug)printf("settings_rec_get:Failed to retrieve record for AWG unit 0  (%d)",status);
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for AWG unit 0 (%d)",status);
      return (-1);
    }



  /* get the record for AWG unit 1 */
  size = sizeof(settings.awg1);
  status = db_get_record(hDB, hAWG1, &settings.awg1, &size, 0);
  if (status != DB_SUCCESS)
    {
      if(debug)printf("settings_rec_get:Failed to retrieve record for AWG unit 1  (%d)",status);
      cm_msg(MINFO,"settings_rec_get","Failed to retrieve record for AWG unit 1 (%d)",status);
      return (-1);
    }
  printf("settings_rec_get: returning success\n");
  return SUCCESS;

}

INT tr_prestart(INT run_number, char *error)
{
  INT status,size;
  char  ppg_mode[30];
 char  outfile[256] , foutfile[256]  , cmd[512];
 char  infile[256]  , finfile[256];
 char  infofile[256];
 FILE *errfile;
  char str[256];
 time_t timbuf;
  char timbuf_ascii[30];
  DWORD elapsed;
  char path[80];
 struct stat stat_buf;

 double elapsed_time;
 static struct timeval t3, t4, t5;

 /*  see how long it takes */
 gettimeofday(&t3, NULL); // start timer

 cm_msg(MINFO,"tr_prestart","++ tr_prestart starting timer");  

  check_params(ppg_mode);
 
  gettimeofday(&t4, NULL);  // end time
 // compute and print the elapsed time in millisec
	      
 elapsed_time = (t4.tv_sec - t3.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t4.tv_usec - t3.tv_usec) / 1000.0;   // us to ms
 printf("tr_prestart: check_params took   %f ms\n",elapsed_time);
 cm_msg(MINFO,"prestart"," check_params took  %f ms",elapsed_time);

 gettimeofday(&t5, NULL);  // start time
  status = compute(ppg_mode);
  if(status != SUCCESS)
    {
      printf("Error from compute, see odb messages for details\n");
      return status;
    }

  gettimeofday(&t4, NULL);  // end time
 // compute and print the elapsed time in millisec
	      
 elapsed_time = (t4.tv_sec - t5.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t4.tv_usec - t5.tv_usec) / 1000.0;   // us to ms
 printf("tr_prestart: compute took  %f ms\n",elapsed_time);
 cm_msg(MINFO,"tr_prestart"," compute took %f ms",elapsed_time);

 if(tfile)
   {
     printf("prestart:now printing delays\n");
     print_delays(tfile);
    
     fclose(tfile);
     printf("prestart:now closing tfile\n");
     tfile=NULL;
   }



#ifdef BYTECODE
 gettimeofday(&t5, NULL);  // start time

  /* remove old bytecode.dat from the PPG path */
  sprintf(foutfile,"%sbytecode.dat", settings.ppg.input.ppgload_path);
  //printf("foutfile=%s\n",foutfile);
  status = ss_file_remove(foutfile);
  if (status != 0)
      cm_msg(MINFO,"prestart","old file %s not found (to be deleted)", foutfile);

  /* Get the name of the output file (e.g. titan.ppg) */
  sprintf(outfile,"%s%s%s",settings.ppg.input.ppgload_path,ppg_mode,".ppg");


 /* update odb parameters */
  settings.ppg.output.compiled_file_time__binary_ = 0; /* binary time */
  strcpy (settings.ppg.output.compiled_file_time,"file deleted"); 
  

 sprintf(infofile,"%s/ppgmsg.txt",settings.ppg.input.ppgload_path );
 
 sprintf(cmd,"/usr/bin/perl %s/comp_int.pl %s %s  > %s", settings.ppg.input.ppg_perl_path, 
   outfile, foutfile, infofile );

 // ls > dirlist 2>&1  redirect stdout and stderr to the same file

  if(strlen(cmd) > sizeof(cmd))
    { /* overwriting cmd caused stat to fail due to overwrite of foutfile */
      cm_msg(MERROR,"prestart","Internal programming error. Length of cmd (%d) is too short for command string(%d)",strlen(cmd) , sizeof(cmd));
      printf("tr_prestart: Error return. See odb messages for details\n");
      return -1;
    }

  errno=0;  // clear errno
  status = system(cmd);
  if(debug)
    {
      printf("cmd=\"%s\"\n",cmd);
      printf("status after system command = %d\n",status);
    }

  if(status)
    {
     cm_msg(MERROR,"prestart","sys error (from ppg compiler) [%d/%d] %s", (status/256), errno, strerror(errno));
     printf("prestart: Error return from ppg compiler status/256=%d errno=%d strerror(errno)=%s\n",
	    status/256, errno, strerror(errno));
     printf("prestart: strerror(status/256)=%s\n",strerror(status/256));

     errfile = fopen(infofile,"r");
      
     if (errfile != NULL)
       {
	 sprintf(str,"PPG COMPILER ERROR MESSAGES:");
	 cm_msg(MERROR,"prestart","%s",str);
	 while(fgets(str,256,errfile) != NULL)
	   cm_msg(MERROR,"prestart","%s",str);
	 fclose(errfile);
       }      
     else
       cm_msg(MERROR,"prestart","Couldn't open ppg compiler error file %s",infofile);


     cm_msg(MERROR,"prestart","File bytecode.dat has NOT been produced by ppg compiler");
     printf("tr_prestart: Error return. See odb messages for details\n");
     return status;
    }
  else
    //    cm_msg(MINFO,"prestart","Successfully compiled ppg file & written ",foutfile);
    printf("prestart:Successfully compiled ppg file & written \n",foutfile);

  /* wait for compiled file */
  if(debug)printf("waiting for compiled file %sbytecode.dat...\n", settings.ppg.input.ppgload_path);

  gettimeofday(&t4, NULL);  // end time and start time
 // compute and print the elapsed time in millisec

 elapsed_time = (t4.tv_sec - t5.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t4.tv_usec - t5.tv_usec) / 1000.0;   // us to ms
 printf("prestart: old PPG compiler took   %f ms\n",elapsed_time);
 cm_msg(MINFO,"prestart"," old PPG compiler took %f ms",elapsed_time);


  status = file_wait(settings.ppg.input.ppgload_path, (char*)"bytecode.dat");


  //if(debug)printf("status after file wait = %d\n",status);
  if (status < 1)
    {               /* should not get this now */
      cm_msg(MINFO,"prestart","File %sbytecode.dat not found - Compile error", settings.ppg.input.ppgload_path);
      cm_msg(MINFO,"prestart","Compiler messages are in file %s\n",infofile); 
      printf("Compile command was: %s",cmd);
      printf("tr_prestart: Error return. See odb messages for details\n");
      return status;
    }
    else
      //  cm_msg(MINFO,"prestart","compiled file %s produced successfully",foutfile);
      printf("prestart: compiled file %s produced successfully\n",foutfile);

  gettimeofday(&t5, NULL);  // end time and start time
 // compute and print the elapsed time in millisec

 elapsed_time = (t5.tv_sec - t4.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t5.tv_usec - t4.tv_usec) / 1000.0;   // us to ms
 printf("prestart: time for file_wait  %f ms\n",elapsed_time);
 cm_msg(MINFO,"prestart"," time for file_wait  %f ms",elapsed_time);



//printf("foutfile=%s\n",foutfile);
  if( stat (foutfile,&stat_buf) ==0 )
  {
    settings.ppg.output.compiled_file_time__binary_ = stat_buf.st_ctime; /* binary time */
    strcpy(timbuf_ascii, (char *) (ctime(&stat_buf.st_ctime)) );  
     if(debug)printf("Last change to file %s:  %s\n",foutfile,timbuf_ascii); 
    {
      int j;
      j=strlen(timbuf_ascii);
      strncpy(settings.ppg.output.compiled_file_time,timbuf_ascii, j);

      /* An extra carriage return appears unless we do this:  */
      settings.ppg.output.compiled_file_time[j-1] ='\0';
    }    
      
  }
  else
    {
    strcpy (settings.ppg.output.compiled_file_time,"no information available");
    cm_msg(MERROR,"prestart","No information available about compile time of file %s",foutfile);
    cm_msg(MINFO,"prestart","Front-end check on compile time of file will fail");
    }
  
   if(debug)printf("Writing %s to odb ( compiled file time)\n",
		   settings.ppg.output.compiled_file_time ); 


  gettimeofday(&t4, NULL);  // end time
 // compute and print the elapsed time in millisec

 elapsed_time = (t4.tv_sec - t5.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t4.tv_usec - t5.tv_usec) / 1000.0;   // us to ms
 printf("prestart: to end of old PPG compiler   %f ms\n",elapsed_time);
 cm_msg(MINFO,"prestart"," to end of old PPG compiler took %f ms",elapsed_time);


#endif

#ifdef GONE  // do this in compute.c
 // If PLOT is not defined, delays.dat won't have been written

   /*  printf("%s/convert_delays.pl\n", settings.ppg.input.ppg_perl_path);
   printf(" %s/delays.dat\n",settings.ppg.input.ppgload_path);
   printf(" %s/ppgload.dat\n",settings.ppg.input.ppgload_path);
   printf(" %f \n",  settings.ppg.input.ppg_clock__mhz_);
   */
gettimeofday(&t5, NULL);  // start time

sprintf(infofile,"%s/ppgmsg.txt",settings.ppg.input.ppgload_path );
 char newperlpath[64];
 sprintf(newperlpath,"%s/newperl",settings.ppg.input.ppg_path); 
 sprintf(cmd,"/usr/bin/perl %s/convert_delays.pl %s %s/delays.dat %s/ppgload.dat %f > %s", newperlpath, newperlpath,
	 settings.ppg.input.ppgload_path, settings.ppg.input.ppgload_path,  settings.ppg.input.ppg_clock__mhz_, infofile );
 printf("cmd was %s\n",cmd);
 //convert_delays.pl /home/cpet/online/ppg/perl /home/cpet/online/ppg/ppgload/delays.dat /home/cpet/online/ppg/ppgload/ppgload.dat 100

 errno=0;  // clear errno
  status = system(cmd);
  if(debug)
    {
      printf("cmd=\"%s\"\n",cmd);
      printf("status after system command = %d\n",status);
    }

  if(status)
    {
     cm_msg(MERROR,"prestart","sys error (from convert_delays) [%d/%d] %s", (status/256), errno, strerror(errno));
     printf("prestart: Error return from convert_delays status/256=%d errno=%d strerror(errno)=%s\n",
	    status/256, errno, strerror(errno));
     printf("prestart: strerror(status/256)=%s\n",strerror(status/256));

     errfile = fopen(infofile,"r");
      
     if (errfile != NULL)
       {
	 // sprintf(str,"Convert_delay messages:");
      printf("Convert_delay messages:\n");
	 //	 cm_msg(MERROR,"prestart","%s",str);
	 while(fgets(str,256,errfile) != NULL)
	   //	   cm_msg(MERROR,"prestart","%s",str);
	   printf("%s\n",str);
	 fclose(errfile);
       }      
     else
       cm_msg(MERROR,"prestart","Couldn't open convert_delays error file %s",infofile);


     //  cm_msg(MERROR,"prestart","File ppgload.dat has NOT been produced by ppg compiler");
     printf("prestart: File ppgload.dat has NOT been produced by ppg compiler\n");
     printf("tr_prestart: Error return. See odb messages for details\n");
     return status;
    }
  else
    //    cm_msg(MINFO,"prestart","Successfully compiled ppg file & written ",foutfile);
    printf("prestart:Successfully compiled ppg file & written \n",foutfile);

 gettimeofday(&t4, NULL);  // end time

 elapsed_time = (t4.tv_sec - t5.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t4.tv_usec - t5.tv_usec) / 1000.0;   // us to ms
 printf("tr_prestart: system cmd convert_delays.pl took   %f ms\n",elapsed_time);
 cm_msg(MINFO,"tr_prestart"," system cmd convert_delays.pl took  %f ms",elapsed_time);
#endif // GONE


  /* Update output settings in ODB */
  size = sizeof(settings.ppg.output);
  status = db_set_record(hDB, hOut, &settings.ppg.output, size, 0); 
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"prestart","Failed to set output record (size %d) (%d)",size,status);

 gettimeofday(&t5, NULL);  // end time

 elapsed_time = (t5.tv_sec - t4.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t5.tv_usec - t4.tv_usec) / 1000.0;   // us to ms
 printf("tr_prestart: db_set_record took   %f ms\n",elapsed_time);
 cm_msg(MINFO,"tr_prestart","  db_set_record took  %f ms",elapsed_time);
  
 gettimeofday(&t4, NULL);  // end time

	      
 elapsed_time = (t4.tv_sec - t3.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t4.tv_usec - t3.tv_usec) / 1000.0;   // us to ms
 printf("tr_checkppg:time for prestart routine was  %f ms\n",elapsed_time);
 cm_msg(MINFO,"prestart","time for prestart routine to run was  %f ms",elapsed_time);


 gbl_elapsed_time = (t4.tv_sec - tt0.tv_sec) * 1000.0;      // sec to ms
 gbl_elapsed_time += (t4.tv_usec - tt0.tv_usec) / 1000.0;   // us to ms
 printf("tr_checkppg: total time  %f ms\n",gbl_elapsed_time);
 cm_msg(MINFO,"prestart","total time so far  %f ms",gbl_elapsed_time);


  return SUCCESS;
}



void print_some_info(void)
{
  int j;
  if( gbl_num_time_references_defined > 0)
    {
      if(single_shot)
	print_ref_blocks(stdout);
      if(tfile)print_ref_blocks(tfile);
    }

  if(gbl_num_loops_defined > 0)
    {
      if(single_shot)
	print_loop_params(stdout);
      if(tfile)print_loop_params(tfile);


    }
  

  print_transitions(ptrans,stdout);
  if(tfile)print_transitions(ptrans,tfile);

  printf("\n\n Long and Short Blocknames:\n");
     printf("Index       Long Names                          Short Names\n");
      for(j=0;j<nnames; j++)
	printf("%d  %40.40s   %40.40s\n",j,long_names[j],short_names[j]);
 
   printf("\n\n Long and Short Loopnames:\n");
     printf("Index       Long LoopNames                      Short LoopNames\n");
      for(j=0;j<nlloops; j++)
	printf("%d  %40.40s   %40.40s\n",j,long_loop_names[j],short_loop_names[j]);
 

  printf("\n");
  
}

/*--------------------------------------------------------*/
INT get_input_data(HNDLE hKey)
{
  INT status;
  HNDLE hSubkey;
  KEY key;
  INT i,j;
  INT swapped;
  char tfile_name[128];
  char cmd[128];


  print_info.flags = 0;
  print_info.pattern[0] = 0;
  ls_line = 0;


 double elapsed_time;
 static struct timeval t5, t6, t7;

 /*  see how long it takes */
 gettimeofday(&t5, NULL); // start timer
 cm_msg(MINFO,"get_input_data","** starting get_input_data (called from tr_precheck), starting timer");

  /* get info for tfile */
  size = sizeof(run_number);
  status = db_get_value(hDB, 0, "/Runinfo/run number", &run_number, &size, TID_INT, 0);
 
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"get_input_data","error getting run number (%d)",status);
      return status;
    }

  size = sizeof(data_dir);
  status = db_get_value(hDB, 0, "/logger/data dir",
                                   data_dir, &size, TID_STRING, 0);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"get_input_data","error getting \"data dir\" (%d)",status);
      return status;
    }

  tfile=NULL;
  if(debug)
    {
      sprintf(tfile_name,"%s/%s/run%d_%s",data_dir,"info",run_number,"config.txt");
      printf("get_input_data: about to open debug file: %s\n",tfile_name);
      tfile = fopen(tfile_name, "w");
  
      if(! tfile)
	{
	  cm_msg(MERROR,"get_input_data","error opening output transitions file %s", tfile_name);
	  return -1;
	}
      gettimeofday(&t7, NULL);  // start time
      sprintf(cmd, "rm %s/infofile",settings.ppg.input.ppg_path);
      status = system (cmd);
      printf("get_input_data: after cmd: %s status=%d\n",cmd,status);

      sprintf(cmd, "ln -s %s %s/infofile",tfile_name, settings.ppg.input.ppg_path);
      status = system (cmd);
      printf("get_input_data: after cmd: %s status=%d\n",cmd,status);

      printf("get_input_data: linked %s to %s/infofile\n",tfile_name, settings.ppg.input.ppg_path);

      gettimeofday(&t6, NULL);  // end time
      // compute and print the elapsed time in millisec
      
      elapsed_time = (t6.tv_sec - t7.tv_sec) * 1000.0;      // sec to ms
      elapsed_time += (t6.tv_usec - t7.tv_usec) / 1000.0;   // us to ms
      printf("get_input_data: system commands took    %f ms\n",elapsed_time);
      cm_msg(MINFO,"get_input_data","before hkey , system commands took  %f ms",elapsed_time);
    } // debug
  gettimeofday(&t7, NULL);  // start time
  if (hKey) 
    {
      if(debug)printf("get_input_data: calling db_scan_tree with hKey=%d\n",hKey);
    
      status = db_scan_tree(hDB, hKey, 0, print_key, &print_info);
      //printf("after db_scan_tree, status=%d error_flag=%d\n",status,error_flag);
      if(status != DB_SUCCESS || error_flag)
	{
	  printf("get_input_data: PRINTING after error detected\n");
	  print_some_info();	  
	  return DB_INVALID_PARAM;
	}
      if(debug)
	{
	  printf("get_input_data:total number of blocks : %d\n", block_counter[TOTAL]);
	 
	  for (i=0; i< block_counter[TOTAL]; i++)  // from 0 to total number of blocks
	    printf("get_input_data:block_list[%d] : 0x%x\n", i,block_list[i]) ;
	}
    }


 gettimeofday(&t6, NULL);  // end time
 // compute and print the elapsed time in millisec
	      
 elapsed_time = (t6.tv_sec - t7.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t6.tv_usec - t7.tv_usec) / 1000.0;   // us to ms
 printf("get_input_data: db_scan_tree took  %f ms\n",elapsed_time);
 cm_msg(MINFO,"get_input_data"," debug=%d, db_scan_tree took %f ms",debug,elapsed_time);


  printf("get_input_data:PRINTING\n");
  print_some_info();

  
  if( (block_counter[BEGIN_LOOP] ) != (block_counter[END_LOOP] ) )
    {
      cm_msg(MERROR,"get_input_data","ERROR: there are %d begin loops and %d  end loops; they must match\n",
	     block_counter[BEGIN_LOOP], block_counter[END_LOOP]     );
      return DB_INVALID_PARAM;
    }

  print_ref_blocks(stdout);

#ifdef AUTO_TDCBLOCK
  /* Now sort out TDCBLOCK pulses - call before SORT (or would need to re- sort after)
     Adds them to TRANSITIONS
  */ 
  printf("get_input_data: calling sort_tdcblock\n");
  sort_tdcblock(ptdc);
 
  printf("get_input_data: calling add_tdcblocks\n");
  if (add_tdcblocks(ptrans,ptdc) != SUCCESS)
    return DB_INVALID_PARAM;

  printf("\n After adding TDCBLOCK transitions:\n");

#endif // AUTO_TDCBLOCK

  /// print_transitions(ptrans,stdout);

  gettimeofday(&t7, NULL);  // start time
 // compute and print the elapsed time in millisec
	      
  printf("\nSorting transitions...\n");
  sort_transitions(ptrans);

 gettimeofday(&t6, NULL);  // end time
 // compute and print the elapsed time in millisec
	      
 elapsed_time = (t6.tv_sec - t7.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t6.tv_usec - t7.tv_usec) / 1000.0;   // us to ms
 printf("get_input_data: sort_transitions took  %f ms\n",elapsed_time);
 cm_msg(MINFO,"get_input_data","sort_transitions took  %f ms",elapsed_time);
  printf("\nSorting transitions...\n");
 
  if(tfile)print_sorted_transitions(ptrans,FALSE,tfile); // print to file transitions.txt


  gettimeofday(&t7, NULL);  // start time
 // compute and print the elapsed time in millisec 

  status = precheck_sorted_transitions(ptrans); // gets beg/end run in correct order
  if(status != SUCCESS)
    return status;
  gettimeofday(&t6, NULL);  // end time

 // compute and print the elapsed time in millisec 
 elapsed_time = (t6.tv_sec - t7.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t6.tv_usec - t7.tv_usec) / 1000.0;   // us to ms
 printf("get_input_data: precheck_sorted_transitions took  %f ms\n",elapsed_time);
 cm_msg(MINFO,"get_input_data","precheck_sorted_transitions took  %f ms",elapsed_time);
 

  /* 
*****    Do not add extra Clock pulse - not needed

  if ((block_counter[EV_SET] > 0) ||  (block_counter[EV_STEP] > 0))
    {
      /* if there are EV blocks, add final clock pulse(s) before end-of-scan loop */
  /*
      printf ("get_input_data: EV block(s) found; adding extra clock pulse(s) to reset AWG(s) \n");
      //      if( add_last_awg() != SUCCESS)
      //	{
      //  cm_msg(MERROR,"get_input_data","error from add_last_awg");
      //  return DB_INVALID_PARAM;
      //}
    
// re-sort
       printf("\nResorting transitions ...\n");
       sort_transitions(ptrans);
       precheck_sorted_transitions(ptrans); // gets beg/end run in correct order
      }

  fprintf(tfile,"Re-sorted after adding last EV clock pulse(s) \n");
  print_sorted_transitions(ptrans,FALSE,tfile); // print to file transitions.txt
  */  


  /*
 ***  Cannot add scan loop automatically any more; has to be present in the Block List in ODB


    status = auto_add_loop(0,1,ptrans); // add end-of-scan loop
    if(status != SUCCESS)
    {
      cm_msg(MERROR,"tr_precheck","error adding end of scan loop");
      return status;
    }
  */

  gettimeofday(&t7, NULL);  // start time
 // compute and print the elapsed time in millisec 

  /* if there are EV blocks, deal with them now */
  if ((block_counter[EV_SET] > 0) ||  (block_counter[EV_STEP] > 0))
    {
      if (awg_params[0].in_use ==0 && awg_params[1].in_use==0)
	{
	  cm_msg(MERROR,"get_input_data","EV blocks are present but neither AWG is in use");
	  return  DB_INVALID_PARAM;
	}


      for (i=0; i<NUM_AWG_MODULES; i++)
	{
	  if(awg_params[i].in_use)
	      status = config_AWG(i);
	  if(status != SUCCESS)
	    return status;
	}

      /* load AWG; removes EV_SET and EV_STEP from transition list */  
      if( check_ev(ptrans) != SUCCESS)
	{
	  printf("get_input_data: error from checking EV_SET and EV_STEP blocks\n");
	  return DB_INVALID_PARAM;
	}

      // load AWG Unit(s)
      /*  for (i=0; i<NUM_AWG_MODULES; i++)
	{
	  if(awg_params[i].in_use) // if Unit is in use
	  {*/	  
	      if( load_AWG_mem(ptrans) != SUCCESS)
		return DB_INVALID_PARAM;
	      /* }
		 }*/

	      for (i=0; i<NUM_AWG_MODULES; i++)
		{
		  if(awg_params[i].in_use)
		    close_AWG(i);
		}

	      if(tfile)
		{
		  fprintf(tfile,"EV blocks have been removed \n");
		  print_sorted_transitions(ptrans,TRUE, tfile); // with bitpats
		}
    }
  else
    {
    //    printf("get_input_data: no EV blocks found; no need to add extra clock pulse to reset AWG \n");
  
      printf("get_input_data: no EV blocks found; AWG(s) will not be loaded\n");
      cm_msg(MINFO,"get_input_data","no EV blocks found; AWG(s) will not be loaded");
    }


 gettimeofday(&t6, NULL);  // end time
 // compute and print the elapsed time in millisec
	      
 elapsed_time = (t6.tv_sec - t7.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t6.tv_usec - t7.tv_usec) / 1000.0;   // us to ms
 printf("get_input_data: dealing with EV blocks took   %f ms\n",elapsed_time);
 cm_msg(MINFO,"get_input_data","dealing with EV blocks took  %f ms",elapsed_time);

   gettimeofday(&t7, NULL);  // start time

  printf("\n Checking sorted transitions...\n");
  status = check_sorted_transitions(ptrans);
  if(status != DB_SUCCESS)
    {
      if(single_shot)
	print_sorted_transitions(ptrans,TRUE, stdout); // with bitpats
      if(tfile)
	{
	  fprintf(tfile, "Error after checking sorted transitions\n");
	  print_sorted_transitions(ptrans,TRUE, tfile); // with bitpats
	}
      return status;
    }
  

gettimeofday(&t6, NULL);  // end time
 // compute and print the elapsed time in millisec
	   
 elapsed_time = (t6.tv_sec - t7.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t6.tv_usec - t7.tv_usec) / 1000.0;   // us to ms
 printf("get_input_data: check_sorted_transitions took    %f ms\n",elapsed_time);
 cm_msg(MINFO,"get_input_data"," check_sorted_transitions took %f ms",elapsed_time);

   
 elapsed_time = (t6.tv_sec - t5.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t6.tv_usec - t5.tv_usec) / 1000.0;   // us to ms
 printf("get_input_data: ending, elapsed time   %f ms\n",elapsed_time);
 cm_msg(MINFO,"get_input_data","** returning from get_input_data , elapsed time  from start of get_input_data %f ms",elapsed_time);


  printf("\n");
  return status;
}



INT print_key(HNDLE hDB, HNDLE hKey, KEY * key, INT level, void *info)
{
  INT i, size, status;
  static char data_str[50000], line[256];
  DWORD delta;
  PRINT_INFO *pi;
  HNDLE hmykey;
  INT total_size;
  
  //printf("print_key: starting with hKey=%d\n",hKey);
  if(error_flag) 
    {
      // if(debug)printf("print_key: returning because error_flag is set\n");
      return DB_INVALID_PARAM;
    }

  pi = (PRINT_INFO *) info;
  
 
  size = sizeof(data);

  if (pi->flags & PI_VALUE) 
    {
      /* only print value */
      if (key->type != TID_KEY) 
	{
	  status = db_get_data(hDB, hKey, data, &size, key->type);
	  if (status == DB_NO_ACCESS)
	    strcpy(data_str, "<no read access>");
	  else
	    for (i = 0; i < key->num_values; i++) 
	      {
		if (pi->flags & PI_HEX)
		  db_sprintfh(data_str, data, key->item_size, i, key->type);
		else
		  db_sprintf(data_str, data, key->item_size, i, key->type);
		
		if ((pi->index != -1 && i == pi->index) || pi->index == -1)
		  printf("%s\n", data_str);
		if(error_flag)return 0;
		//	if (check_abort(pi->flags, ls_line++))
		// return 0;
	      }
	}
    } 
  else
    {
      /* print key name with value */
      memset(line, ' ', 80);
      line[80] = 0;
      sprintf(line + level * 4, "%s", key->name);
      if (pi->index != -1)
	sprintf(line + strlen(line), "[%d]", pi->index);
      line[strlen(line)] = ' ';
      if (key->type == TID_KEY) 
	{
	  if (pi->flags & PI_LONG)
	    sprintf(line + 32, "DIR");
	  else
	    line[32] = 0;
	  if(debug)printf("%s; KEY level %d\n", line,level);
	  //if (level == 1)
	    {
	      if(get_block(hKey, key) != DB_SUCCESS)
		{
		  error_flag=1; //set error flag
		  return DB_INVALID_PARAM;
		}
	    }
	  
	}
      else 
	{ 
	  status = db_get_data(hDB, hKey, data, &size, key->type);
	  if (status == DB_NO_ACCESS)
	    strcpy(data_str, "<no read access>");
	  else {
	    if (pi->flags & PI_HEX)
	      db_sprintfh(data_str, data, key->item_size, 0, key->type);
	    else
	      db_sprintf(data_str, data, key->item_size, 0, key->type);
	  }
	  
	  if (pi->flags & PI_LONG) {
	    sprintf(line + 32, "%s", rpc_tid_name(key->type));
	    line[strlen(line)] = ' ';
	    sprintf(line + 40, "%d", key->num_values);
	    line[strlen(line)] = ' ';
	    sprintf(line + 46, "%d", key->item_size);
	    line[strlen(line)] = ' ';
	    
	    db_get_key_time(hDB, hKey, &delta);
	    if (delta < 60)
	      sprintf(line + 52, "%lds", delta);
	    else if (delta < 3600)
	      sprintf(line + 52, "%1.0lfm", delta / 60.0);
	    else if (delta < 86400)
	      sprintf(line + 52, "%1.0lfh", delta / 3600.0);
	    else if (delta < 86400 * 99)
	      sprintf(line + 52, "%1.0lfh", delta / 86400.0);
	    else
	      sprintf(line + 52, ">99d");
	    line[strlen(line)] = ' ';
	    
            sprintf(line + 57, "%d", key->notify_count);
            line[strlen(line)] = ' ';
	    
            if (key->access_mode & MODE_READ)
	      line[61] = 'R';
            if (key->access_mode & MODE_WRITE)
	      line[62] = 'W';
	    if (key->access_mode & MODE_DELETE)
	      line[63] = 'D';
            if (key->access_mode & MODE_EXCLUSIVE)
	      line[64] = 'E';
	    
            if (key->type == TID_STRING && strchr(data_str, '\n'))
	      strcpy(line + 66, "<multi-line>");
            else if (key->num_values == 1)
	      strcpy(line + 66, data_str);
            else
	      line[66] = 0;
	  } 
	  else if (key->num_values == 1)
	    if (key->type == TID_STRING && strchr(data_str, '\n'))
	      strcpy(line + 32, "<multi-line>");
	    else
	      strcpy(line + 32, data_str);
	  else
	    line[32] = 0;
	
	  printf("%s\n", line);
	  
	  if (key->type == TID_STRING && strchr(data_str, '\n'))
	    puts(data_str);
	 
	  if(error_flag)return 0;
	  //if (check_abort(pi->flags, ls_line++))
	  //  return 0;
	  
	  if (key->num_values > 1) 
	    {
	      for (i = 0; i < key->num_values; i++) 
		{
		  if (pi->flags & PI_HEX)
		    db_sprintfh(data_str, data, key->item_size, i, key->type);
		  else
		    db_sprintf(data_str, data, key->item_size, i, key->type);
		  
		  memset(line, ' ', 80);
		  line[80] = 0;
		  
		  if (pi->flags & PI_LONG) 
		    {
		      sprintf(line + 40, "[%d]", i);
		      line[strlen(line)] = ' ';
		      
		      strcpy(line + 56, data_str);
		    } 
		  else
		    strcpy(line + 32, data_str);
		  
		  if ((pi->index != -1 && i == pi->index) || pi->index == -1)
		    printf("%s\n", line);
		  
		  if(error_flag)return 0;
		  //if (check_abort(pi->flags, ls_line++))
		  // return 0;
		}
	    }
	}

    }  
  return SUCCESS;
}
/*-----------------------------------------------------------------*/
INT get_block(HNDLE hKey, KEY *key  )
{
  INT i,j,len, size, status, total_size, my_index, block_index;
  INT skip;
  char Name[32];
  char str[128];
  INT nindex;
  status = DB_SUCCESS;
  skip=FALSE;
  
  if(debug)printf("get_block: debug=%d starting with hKey=%d and key->name = %s \n",debug,
    hKey,key->name );
  
  block_index=block_counter[TOTAL];
  if(debug)printf("get_block: block_index=%d\n",block_index);

  
  /* check record size */
  status = db_get_record_size(hDB, hKey, 0, &total_size);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"get_block","error getting record size for %s (%d)",
	     key->name,status);
      return status;
    }
  
  len = strlen(key->name);
  size=(INT)sizeof(Name);
  if(debug) printf("get_block: Key name %s len=%d and sizeof(Name)=%d\n",key->name,len,size);

  /*  if(len >= (int)sizeof(Name))
    {
      cm_msg(MERROR,"get_block",
	     "Key name %s is too long for temp array (%d). Max len=%d ",key->name,len,sizeof(Name));
      return DB_INVALID_PARAM;
    }
  */

  if(len > size)
    len=sizeof(Name) -1 ; // check size
  for   (j=0; j<len; j++)
    {
      Name[j]  = toupper (key->name[j]);
      if(Name[j]==' ')
	{
	  Name[j]='_';
	  //printf("replaced space by underscore\n");
	}
    }
    Name[len]='\0';

    len=strlen(Name);
    if(debug)
      printf("get_block: Name:%s  length=%d\n",Name,len);
  
    nindex=-1; // not found

  if(strncmp(Name,"SKIP",4)==0) 
    {
      printf("get_block: skipping blocks until find a UNSKIP block\n");
      gbl_skip=TRUE;
    }
  else if(strncmp(Name,"UNSKIP",5)==0) 
    {
      printf("get_block: parsing blocks is restarting\n");
      gbl_skip=FALSE;
    }

  if(gbl_skip)
    {
      printf("get_block: gbl_skip is set; skipping block %s; returning\n",Name);
      return SUCCESS;
    }


  block_counter[TOTAL]++; /* another block added */
  if( block_counter[TOTAL] > MAX_BLOCKS)
    {
      cm_msg(MERROR, "get_block","limit on maximum number of blocks reached (%d); increase array size",MAX_BLOCKS);
      return DB_INVALID_PARAM;
    }

  if(strncmp(Name,"STDPULSE",3)==0) 
    {
      my_index =block_counter[PULSE]; // my_index counts pulse blocks & goes from 0 (array index)
      printf("get_block: Working on STDPULSE block %s (index %d)\n", Name, my_index);
      status = get_a_block(hKey, block_index,total_size, key,
			   PULSE, MAX_PULSE_BLOCKS, (char*) STD_PULSE_BLOCK_STR );
      if(status != DB_SUCCESS)
	return status;    
      nindex=2; // index into blockarray and blockmin
    }
  
  else if(strncmp(Name,"PULSE",3)==0) 
    { 
      my_index =block_counter[PULSE]; // my_index counts pulse blocks & goes from 0 (array index)
      printf("get_block: Working on PULSE block %s (index %d)\n",Name, my_index);
      status = get_a_block(hKey, block_index,total_size, key,
			   PULSE, MAX_PULSE_BLOCKS, (char*)PULSE_BLOCK_STR   );
      if(status != DB_SUCCESS)
	return status;  
     nindex=1; // index into blockarray and blockmin
    }
  
  //  transition = pulse block with zero pulse width
  else if(strncmp(Name,"TRANS",3)==0) 
    {
      my_index =block_counter[PULSE]; // my_index counts pulse blocks & goes from 0 (array index)
      printf("get_block: Working on TRANS block %s (index %d)\n",Name, my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   PULSE, MAX_PULSE_BLOCKS, (char*)TRANS_BLOCK_STR   );
      if(status != DB_SUCCESS)
	return status;  
     nindex=0; // index into blockarray and blockmin
    }
  
  //  pattern block (special type of pulse block)
  else if(strncmp(Name,"PATTERN",3)==0) 
    {
      
      my_index =block_counter[PULSE]; // my_index counts pulse blocks & goes from 0 (array index)
      printf("get_block: Working on PATTERN block %s (index %d)\n",Name, my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   PULSE, MAX_PULSE_BLOCKS, (char*)PATTERN_BLOCK_STR   );
      if(status != DB_SUCCESS)
	return status; 
      nindex=3; // index into blockarray and blockmin
    }
  
  //   delay block (special type of pulse block)
      
  else if(strncmp(Name,"DELAY",3)==0) 
    {
      if(debug)printf("detected DELAY block\n");
      my_index =block_counter[PULSE]; // my_index counts pulse blocks & goes from 0 (array index)
      printf("get_block: Working on DELAY block %s (index %d)\n",Name, my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   PULSE, MAX_PULSE_BLOCKS, (char*)DELAY_BLOCK_STR   );
      if(status != DB_SUCCESS)
	return status;  
      nindex=4; // index into blockarray and blockmin
    }
  
  
      
      
  else if(strncmp(Name,"TIME_REF",3)==0)
    {
      my_index =block_counter[TIME_REF]; // my_index counts TIME_REF blocks & goes from 0 (array index)
      printf("get_block: Working on TIME_REF block %s (index %d)\n",Name, my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   TIME_REF, MAX_TIME_REF_BLOCKS, (char*)TIME_REF_BLOCK_STR   );
      if(status != DB_SUCCESS)
	return status; 
      nindex=5; // index into blockarray and blockmin
    }
  /*      // NOT IMPLEMENTED
  else if(strncmp(Name,"XY_START",6)==0)
    {  // NOT IMPLEMENTED ?
      my_index =block_counter[XY_START]; // my_index counts XY_START blocks & goes from 0 (array index)
      printf("get_block: Working on XY_START block %d\n",my_index);
      
      status = get_a_block_without_params(hKey, block_index,total_size, key,
      				  XY_START, MAX_XY_BLOCKS);
      if(status != DB_SUCCESS)
	return status;  
     
    }
      
  else if(strncmp(Name,"XY_STOP",6)==0)
    { // NOT IMPLEMENTED ?
      my_index =block_counter[XY_STOP]; // my_index counts XY_STOP blocks & goes from 0 (array index)
      printf("get_block: Working on XY_STOP block %d\n",my_index);
      
      status = get_a_block_without_params(hKey, block_index,total_size, key,
					  XY_STOP, MAX_XY_BLOCKS);
      if(status != DB_SUCCESS)
	return status;  
    }
  */ 
  else if(strncmp(Name,"EVSET",5)==0)
    { 
      my_index =block_counter[EV_SET]; // my_index counts ev_set blocks & goes from 0 (array index)
      printf("get_block: Working on EVSET block %s (index %d)\n",Name, my_index);
      status = get_a_block(hKey, block_index,total_size, key,
			   EV_SET, MAX_EV_BLOCKS, (char*)EV_SET_BLOCK_STR );
      if(status != DB_SUCCESS)
	return status; 
       nindex=9; // index into blockarray and blockmin
    }
  
  
  else if(strncmp(Name,"EVSTEP",5)==0)
    { 
      my_index =block_counter[EV_STEP]; // my_index counts ev_step blocks & goes from 0 (array index)
      status = get_a_block(hKey, block_index,total_size, key,
			   EV_STEP, MAX_EV_BLOCKS, (char*)EV_STEP_BLOCK_STR);
      if(status != DB_SUCCESS)
	return status;  
      nindex=8; // index into blockarray and blockmin
    } 
  
  
  
  else if(strncmp(Name,"BEGIN_LOOP",6)==0) 
    { 
      //printf("sizeof begin_loop_block is %d\n",sizeof(BEGIN_LOOP_BLOCK) );
      my_index =block_counter[BEGIN_LOOP]; // my_index counts begin_loop blocks & goes from 0 (array index)
      if(debug) printf("get_block: Working on BEGIN_LOOP block %s (index %d)\n",Name,my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   BEGIN_LOOP, MAX_LOOP_BLOCKS, (char*)BEGIN_LOOP_BLOCK_STR   );
      if(status != DB_SUCCESS)
	{
	  if(debug)printf("Error from get_a_block (%d) returning\n",status);
	  
	  return status;  
	}
      nindex=6; // index into blockarray and blockmin
    }
      
      
  else if(strncmp(Name,"END_LOOP",4)==0) 
    { 
      my_index =block_counter[END_LOOP]; // my_index counts end_loop blocks & goes from 0 (array index)
      if(debug) printf("get_block: Working on END_LOOP block %s (index %d)\n",Name,my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   END_LOOP, MAX_LOOP_BLOCKS, (char*)END_LOOP_BLOCK_STR  );
      if(status != DB_SUCCESS)
	return status;  
      nindex=7; // index into blockarray and blockmin
    }
  /* NOT IMPLEMENTED 
  else if (strncmp(Name,"RF_SWEEP",6)==0)
    { 
      my_index =block_counter[RF_SWEEP]; // my_index counts sweep blocks & goes from 0 (array index)
      if(debug) printf("get_block: Working on RF_SWEEP block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   RF_SWEEP,MAX_RFSWEEP_BLOCKS, 
			   (char*)RF_SWEEP_BLOCK_STR 
			   );
      
      if(status != DB_SUCCESS)
	return status;  
      
    }
      
  
  else if(strncmp(Name,"RF_FM_SWEEP",6)==0) 
    { 
      my_index =block_counter[RF_SWEEP]; // my_index counts sweep blocks & goes from 0 (array index)
      if(debug)  printf("get_block: Working on RF_FM_SWEEP block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   RF_SWEEP,MAX_RFSWEEP_BLOCKS,  
			   (char*)RF_FM_SWEEP_BLOCK_STR 
			   );
      
      if(status != DB_SUCCESS)
	return status;    
    }
  
  else if(strncmp(Name,"RF_BURST",6)==0) 
    { 
      my_index =block_counter[RF_SWEEP]; // my_index counts sweep blocks & goes from 0 (array index)
      if(debug) printf("get_block: Working on RF_BURST block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   RF_SWEEP,MAX_RFSWEEP_BLOCKS, 
			   (char*)RF_BURST_BLOCK_STR 
			   );
      
      if(status != DB_SUCCESS)
	return status;    
    }
  
  else if(strncmp(Name,"RF_SWIFT_AWG",6)==0) 
    { 
      my_index =block_counter[RF_SWEEP]; // my_index counts sweep blocks & goes from 0 (array index)
      if(debug) printf("get_block: Working on RF_SWIFT_AWG block %d\n",my_index);
      
      status = get_a_block(hKey, block_index,total_size, key,
			   RF_SWEEP,MAX_RFSWEEP_BLOCKS,
			   (char*)RF_SWIFT_AWG_BLOCK_STR  
			   );
      
      if(status != DB_SUCCESS)
	return status;    
    }
  else if(strncmp(Name,"LOAD_EG_IONIZE",6)==0) 
    {
      // do nothing .... should have already found this
      if(hLEGI)
	{
	  printf("found LOAD_EG_IONIZE... skipping\n");
	  skip = TRUE;
	}
      else
	{
	  printf("ERROR... found LOAD_EG_IONIZE but hLEGI==0\n");
	  return DB_INVALID_PARAM;
	}
    }
  */
  else
    { /* not one of the known block names */  
      str[0]='\0';
      printf("skipping block %s\n",Name);
      skip=TRUE;  /* block may be a superblock */
      
      /*      cm_msg(MERROR,"get_block","unknown block %s. Name must begin with one of :\n",Name  );
	      printf("get_block: unknown block %s. Name must begin with one of :\n",key->name  );
	      for(i=0; i<  NUM_BLOCK_TYPES; i++)
	      {
	      printf("i=%d block_names[%d]=%s \n",i,i,block_names[i]);
	      strcat(str, block_names[i]);
	      strcat(str,",");
	      }
	      str[strlen(str)-1]='\0';
	      cm_msg(MINFO,"get_block","%s",str);
	      printf("%s\n",str);
	      return DB_INVALID_NAME; */
    }
 
  if(skip)return SUCCESS;

  size=sizeof(blockmin)/sizeof(blockmin[0]); // number of elements
  if(debug)printf("get_block: nindex=%d blockmin length=%d \n",nindex,size);
  if((nindex >= 0) && (nindex < size))
    {
       if(debug)printf("get_block: calling get_short_name with Name=%s nindex=%d blockarray[nindex]=%s blockmin[nindex]=%d  \n",
		 Name,nindex,blockarray[nindex],blockmin[nindex]);
    status = get_short_name(Name,blockarray[nindex],blockmin[nindex]);
    if(status != SUCCESS) return status;
    }
  if(debug)printf("get_block: calling get_params with Name=%s\n",Name);
  status = get_params(hKey, key, my_index, block_index, total_size, Name);


 return status;
}



INT get_short_name(char *my_string, const char* block_type, INT min )
{
  // parameters  my_string = blockname
  //             block_type =  "TRANSITION" or "STDPULSE" (long name)
  //             min        =  minimum number of letters needed e.g. 3 "TRA" or "STD"
  // function shortens long blocknames by removing all unnecessary letters from block_type, removing spaces,underscores and truncates
  // cf check_string2 test.c

  int j,len;
  char*p;
  char*q,*r,*s;
  int m=0;
  char temp_string[BLOCKNAME_LEN];
  INT status;
  INT flag=0;
  INT blockname_truncate;

  if(debug)
    printf("get_short_name starting with block Name=%s block_type=%s and min=%d\n",my_string,block_type,min);
  sprintf(long_names[nnames], "%s",my_string);

  blockname_truncate=BLOCKNAME_TRUNCATE;  // string length
  
 if(strncmp(my_string,"BEGIN_",6)==0)
   {
     p=&my_string[6]; // long loopname
     sprintf(long_loop_names[nlloops],"%s",p);
     s=short_loop_names[nlloops];
     flag=1; // set a flag
   }
 if(strncmp(my_string,"END_",4)==0)
     blockname_truncate=BLOCKNAME_TRUNCATE-2; // END_ loopname should match BEGIN_ loopname

  len=strlen(my_string);
  if(len <= blockname_truncate && (strncmp(my_string,"TRANSITION",6)!= 0)   ) // substitute "trans[ition]"
    {
      sprintf(short_names[nnames], "%s",my_string);
      if(strncmp(my_string,"BEGIN_",6)==0)
	{
	  sprintf(short_loop_names[nlloops],"%s",p);
	  nlloops++;
	}
      nnames++;
      if(debug)printf("get_short_names: blockname is short already\n");
      printf("get_short_names: returning blockname= %s\n",my_string);
      return SUCCESS;
    }
  
  // names to be shortened

  sprintf(temp_string,"%s", my_string);
  sprintf(my_string,"                       "); // clear
  my_string[blockname_truncate]='\0';
  // END loop must end up with the same loopname as begin.
  //  if(strncmp(temp_string,"END_",4)==0)
  //   {
  //   my_string[blockname_truncate-2]='\0'; // begin_ is 2 characters longer than end_
  //  printf("get_short_names: truncating end loop at size %d\n",(blockname_truncate-2));
  //  }
  p=r=my_string; q=temp_string;
  for(j=0; j<strlen(block_type); j++)
    {
        
      if(*q == block_type[j])
	{
	  if(m < min)
	    {
	      *p=*q;p++;q++;m++;
	    }
	    
	  else
	    q++;
	}
      else
	break;
    }
    
  /*  printf("after loop p=%s and my_string=%s\n",p,my_string);
      printf("after loop q=%s and temp_string=%s\n",q,temp_string);
      printf("after loop m=%d and j=%d\n",m,j); */
    

  *p='_';p++;
  //	printf("added underscore. Now index (p-r)=%d, starting while loop\n",(p-r));
  while (*q)  // until end of string
    {
      //printf(" *p=%c  index (p-r)=%d\n",*p,(p-r));
      if(! *p )
	{ // end of string
	  // printf("Found *p = \n at index %d; breaking with my_string=%s\n", (p-&my_string[0]), my_string);
	  break;
	}
	  
      if(*q=='_')
	q++;
      else
	{
	  *p=*q; 
	  if(flag) // begin loop
	    {
	      *s=*q; // copy loopname
	      s++;
	    }
	  p++; q++;
	}
    }
      
    
  // printf("my_string=%s  and p=%s\n",my_string,p);
   sprintf(short_names[nnames], my_string);
  nnames++;

  if(flag)
    { //begin loop
      *s='\0'; // terminate
      printf("get_short_names: short loop name is %s\n", short_loop_names[nlloops]);
      nlloops++;
    }
  printf("get_short_names: returning blockname= %s\n",my_string); 
  return SUCCESS;
}




/*-----------------------------------------------------------------*/

INT print_key_info1(HNDLE hDB, HNDLE hKey, KEY * pkey, INT level, void *info)
{
   int i;
   char *p;
   printf("print_key_info1: starting\n");
   p = (char *) info;

   sprintf(p + strlen(p), "%08X  %08X  %04X    ",
           (int)(hKey - sizeof(DATABASE_HEADER)),
           (int)(pkey->data - sizeof(DATABASE_HEADER)), (int)pkey->total_size);

   for (i = 0; i < level; i++)
      sprintf(p + strlen(p), "  ");

   sprintf(p + strlen(p), "%s\n", pkey->name);

   return SUCCESS;
}
/*-----------------------------------------------------------------*/
void init_blocks(void)
{

  INT i;
  for (i=0; i<MAX_BLOCKS; i++)
    block_list[i]=0;
  for (i=0; i<NUM_BLOCK_TYPES+1; i++)
    block_counter[i]=0;

  // initialize some globals
  gbl_skip = FALSE;
  gbl_open_loop_counter=gbl_num_loops_defined=gbl_num_transitions=0;

  for (i=0; i<NUM_AWG_MODULES; i++)
    awg_params[i].sample_counter=0; 
  gbl_combined=FALSE;

  /* initialize defaults for input parameters  */
  init_default_param_values();
  return;
}

void set_awg_params(void)
{
  /* Set up the AWG parameters :
       AWG Units are fixed as follows:
         awg_trigger_names  PPG signals :  UNIT 0 "AWGHV"   fast   softdac
                                           UNIT 1 "AWGAM"   slow   da816

	 awg_trigger_names are defined in tri_config.h
  */
  INT i;
  // set up some defaults

  //printf("set_awg_params: %s,%s\n",psoftdac,pda816);

  sprintf(awg_params[0].type,"%s",psoftdac);
  sprintf(awg_params[1].type,"%s",pda816);
  sprintf(awg_params[0].outfile,"%s-ch0",psoftdac);
  sprintf(awg_params[1].outfile,"%s-ch0",pda816);


  for (i=0; i<NUM_AWG_MODULES; i++)
    {
      awg_params[i].plot=1; // default is to plot AWG output plots
      awg_params[i].in_use=0; // default is that AWG units are not in use
      ////awg_params[i].available=0; // default is that AWG units are not initialized

      //printf("set_awg_params: %s,%s\n",awg_params[i].type,awg_params[i].outfile);
    }
  return;  
}



void check_params(char *ppg_mode)
{
  INT size, status,j;
  char str[128];
  


 if (settings.ppg.input.ppgload_path[0] != 0)
   if (settings.ppg.input.ppgload_path[strlen(settings.ppg.input.ppgload_path)-1] != DIR_SEPARATOR)          
     strcat(settings.ppg.input.ppgload_path, DIR_SEPARATOR_STR);
 
   
 if (settings.ppg.input.ppg_path[0] != 0)
   if (settings.ppg.input.ppg_path[strlen(settings.ppg.input.ppg_path)-1] != DIR_SEPARATOR)
     strcat(settings.ppg.input.ppg_path, DIR_SEPARATOR_STR);
 
 if(debug)
   {
     printf("ppg path (for input): %s \n",settings.ppg.input.ppg_path);
     printf("ppgload path (for output) : %s \n",settings.ppg.input.ppgload_path);
   }
 sprintf(ppg_mode, "%s", settings.ppg.input.experiment_name);

}

void init_default_param_values(void)
{
  default_stop_freq_khz=0;
  default_start_freq_interval_khz=0;
  default_stop_freq_interval_khz=0;
  default_single_freq_khz=0; // single freq
  default_notch_freq_khz=0; // notch  freq
  default_amplitude_mv=0;
  default_duration_ms=0;
  default_num_cycles=0; // number of cycles
  default_num_bursts=0; // number of bursts
  default_num_frequencies=0; //number of freq
  default_frequency_inc=0; // freq increment)
  default_sweep_type=0; // index to above, 1=rf_sweep 2=rf_fm_sweep 3=rf_burst 4=rf_swift_awg
  default_start_freq_khz=0;
  sprintf(default_rf_generator,"none");
  sprintf(default_ppg_signal_name,"none");
  default_awg_unit=0; // Unit number can be 0 or 1 (only one unit at present, 0)
  default_time_reference=0;
  default_pulse_width_ms = settings.ppg.input.standard_pulse_width__ms_;
  default_loop_count = 1; // loop count if none supplied
  return;
}
  
/*------------------------------------------------------------------*/
INT file_wait(char *path, char *file)
{
  /* wait for compiled file */
  INT    j, nfile;
  char   *list = NULL;

  j = 0;
  do {
    ss_sleep(100);
    nfile = ss_file_find(path, file, &list); 
    j++;
  } while(j<10 && nfile != 1);

  if (nfile != 1)
  {
   cm_msg(MERROR,"file_wait","File not found (%s%s)", path, file);
   return -6;  
  }
  else
    if(debug)printf("file_wait: found file %s%s\n",path,file);
  return 1;
}
/*------------------------------------------------------------*/

void define_t0_ref(void)
{
  INT my_index,block_index;

  block_index=block_counter[TOTAL];
  block_counter[TOTAL]++;
  my_index = block_counter[TIME_REF]; // my_index is zero, defining T0
  block_counter[TIME_REF]++;
  sprintf(time_ref_params[my_index].this_reference_name,"T0");  // define Ref 0 as T0
  time_ref_params[my_index].time_ms=time_ref_params[my_index].t0_offset_ms=0;
  time_ref_params[my_index].my_ref=my_index; // referenced to itself
  time_ref_params[my_index].block_index=block_index; // should be zero
  strcpy(time_ref_params[my_index].block_name,"TIME_REF0"); 
  gbl_num_time_references_defined=1;
  if(debug)
    print_ref_blocks(stdout);
}

INT define_trans0(void)
{
  /* called after add_auto_loop for begin_scan loop or time_ref_index 1 will not be defined */

  INT block_index,my_index;

  if(debug)
    printf("define_trans0: default_time_reference = %d\n",default_time_reference);
  if(default_time_reference != 0)
    {
      cm_msg(MERROR,"define_trans0",
	     "internal programming error. Expect default_time_reference to be 0  not %d",default_time_reference);
      return DB_INVALID_PARAM;   
    }
  print_default_time_reference();
  printf("gbl_num_transitions=%d\n",gbl_num_transitions);
  p_transitions[gbl_num_transitions].t0_offset_ms = 0;
  p_transitions[gbl_num_transitions].time_reference_index=1; // _TBEGSCAN
  p_transitions[gbl_num_transitions].bit =  0x80000000; //bit pattern of 0 ORed with  0x80000000
  p_transitions[gbl_num_transitions].code = TRANS_CODE;
  block_index=block_counter[TOTAL];
  block_counter[TOTAL]++;
  my_index = block_counter[PULSE];
  block_counter[PULSE]++;
  p_transitions[gbl_num_transitions].block_index = block_index;
  sprintf(p_transitions[gbl_num_transitions].block_name,"PATTERN_T0");
  gbl_num_transitions++;

  return SUCCESS;
}

/*------------------------------------------------------------------*/
int main(int argc,char **argv)
{
  INT    status, stat1;
  DWORD  j, i;
  HNDLE  hKey;
  char   host_name[HOST_NAME_LENGTH], expt_name[HOST_NAME_LENGTH];
  char   svpath[64], info[128];
  INT    fHandle;
  INT    msg, ch;
  BOOL   daemon;

  TITAN_ACQ_SETTINGS_STR(titan_acq_settings_str); /* (from experim.h) */
  TITAN_ACQ_PPG_CYCLE_STR(titan_acq_ppg_cycle_str);
  char str[128];
  INT size,run_state;
  char *s;
  make_plots=debug=FALSE;
  daemon = FALSE;
 
  ptrans = p_transitions;

#ifdef AUTO_TDCBLOCK
  ptdc   = tdcblock;
#endif // AUTO_TDCBLOCK
  /* set default */
  cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);
  /* initialize global variables */
  sprintf(eqp_name,"TITAN_acq");
  single_shot=FALSE;

 if(getenv("HOSTTYPE"))
    {
      char cmd[]="hallo";
      int s;
      strncpy(str, getenv("HOSTTYPE"), sizeof(str));
      
      //printf("str=%s\n",str);
      if(strstr (str,"i386"))
        {
          printf("tri_config:  Detected VMIC\n");
        }
      else
	{
	  printf("\n ** Run tri_config on a VMIC or the softdac and da816 cannot be loaded\n");
	  printf(" ** or define DUMMY in awg.c for dummy softdac and da816\n");
	  printf("\nContinue (Y/N) ? ");
	  scanf("%s",cmd);
	  cmd[0]=toupper(cmd[0]);
	  s=cmd[0];
      
	  switch(s)
	    {
	    case ('Y'):
	      break;
	    
	    default :
	      return 0;
	    }
	}
      
    }

 /* get parameters */
 /* parse command line parameters */
 for (i=1 ; i<argc ; i++)
    {
      if (argv[i][0] == '-' && argv[i][1] == 'd')
	debug = TRUE;
      else if (argv[i][0] == '-' && argv[i][1] == 'D')
	daemon = TRUE;
      else if (argv[i][0] == '-' && argv[i][1] == 's')
        single_shot=TRUE;
     else if (argv[i][0] == '-' && argv[i][1] == 'p')
       make_plots=TRUE;
      else if (argv[i][0] == '-')
	{
	  if (i+1 >= argc || argv[i+1][0] == '-')
	    goto usage;
	  if (strncmp(argv[i],"-f",2) == 0)
	    strcpy(svpath, argv[++i]);
	  else if (strncmp(argv[i],"-e",2) == 0)
	    strcpy(expt_name, argv[++i]);
	  else if (strncmp(argv[i],"-h",2)==0)
	    strcpy(host_name, argv[++i]);
	}
      else
	{
	usage:
	  printf("usage: tri_config -d (debug) -s (single loop) -p (make plots) \n");
	  printf("             [-h Hostname] [-e Experiment] [-D] \n\n");
	  return 0;
	}
    }

 printf("Debug = %d\n",debug);

  if (daemon)
    {
      printf("Becoming a daemon...\n");
      ss_daemon_init(FALSE);
   }

 double elapsed_time;
 static struct timeval t3, t4;

 /*  see how long it takes */
 gettimeofday(&t3, NULL); // start timer

  init_blocks();

  /* connect to experiment */
  status = cm_connect_experiment(host_name, expt_name, "tri_config", 0);
  if (status != CM_SUCCESS)
    return 1;

#ifdef _DEBUG
  cm_set_watchdog_params(TRUE, 0);
#endif

  /* turn off message display, turn on message logging */
  cm_set_msg_print(MT_ALL, 0, NULL);

  
  if(! single_shot)
    {
  /* register transition callbacks
     frontend start is 500,  frontend stop is 500 */


      if(cm_register_transition(TR_START, tr_precheck, 350) != CM_SUCCESS)
	{
	  cm_msg(MERROR, "main", "cannot register to transition for tr_precheck");
	  goto error;
	}
  
      if(cm_register_transition(TR_START, tr_prestart,400) != CM_SUCCESS)
	{
	  cm_msg(MERROR, "main", "cannot register to transition for prestart");
	  goto error;
	}
    }
  
  /* connect to the database */
  cm_get_experiment_database(&hDB, &hKey);
  
  /* Check if the structure "Settings"  exists */
  sprintf(str,"/Equipment/%s/Settings",eqp_name);
  status = db_find_key(hDB,0,str, &hSET);
  if (status != DB_SUCCESS)
    {
      if(debug)printf("main: can't find key %s",str);
      
      /* So try to create settings record */
      sprintf(str,"/Equipment/%s/Settings/",eqp_name);
      if(debug) printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(titan_acq_settings_str));
      if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"main","Could not create settings record (%d)\n",status);
        }
      else
        if (debug)printf("Success from create record for %s\n",str);
      
      /* Check again if the structure "settings"  exists  */
      status = db_find_key(hDB,0,"equipment/TITAN_Acq/settings", &hSET);
      if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"main","Equipment/TITAN_Acq/settings is still not present");
          goto error;
        }
    }



  /* check that the record size for hSET is as expected */
  status = db_get_record_size(hDB, hSET, 0, &size);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "main", "error during get_record_size (%d)",status);
      return status;
    }
  printf("Size of Settings saved structure: %d, size of Settings record: %d\n", 
	 sizeof(TITAN_ACQ_SETTINGS) ,size);
  
  if (sizeof(TITAN_ACQ_SETTINGS) != size) 
    {
      /* create Settings record */
      cm_msg(MINFO,"main","creating record (titan_acq/settings); mismatch between size of structure (%d) & record size (%d)", sizeof(TITAN_ACQ_SETTINGS) ,size);
      status = db_create_record(hDB, 0, str , strcomb(titan_acq_settings_str));
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"main","Could not create settings record (%d)\n",status);
	  return status;
	}
      else
	if (debug)printf("Success from create record for %s\n",str);
    }
    


  
  /* Find PPG directory under Settings */
  status = db_find_key(hDB,0,"equipment/TITAN_Acq/settings/ppg", &hPPG);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","Equipment/TITAN_Acq/settings/ppg is not present");
      goto error;
    }
  
  
  /* check if Output tree is available */
  status = db_find_key(hDB, hPPG, "output", &hOut);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find Output record (%d)",status);
      goto error;
    }

  /* check if Input tree is available */
  status = db_find_key(hDB, hPPG, "input", &hIn);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find Input record (%d)",status);
      goto error;
    }

  /* check if AWG tree is available */
  /* Check if the structure "AWG"  exists */
  status = db_find_key(hDB,hSET,"AWG0", &hAWG0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find AWG0 record (%d)",status);
      goto error;
    }
 
  /* check if AWG tree is available */
  /* Check if the structure "AWG1"  exists */
  status = db_find_key(hDB,hSET,"AWG1", &hAWG1);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR,"main","can't find AWG1 record (%d)",status);
      goto error;
    }

  /* Now check if the structure "TRAP PARAMS"  exists */
  sprintf(str,"/Equipment/%s/ppg cycle",eqp_name);
  status = db_find_key(hDB,0,str, &hCycle);
  if (status != DB_SUCCESS)
    { /*  try to create record for this  */
      cm_msg(MERROR,"main","can't find PPG Cycle params (key %s)...creating record (%d)",str,status);
      /* So try to create trap params record */
      printf("Attempting to create record for %s\n",str);
      
      status = db_create_record(hDB, 0, str , strcomb(titan_acq_ppg_cycle_str));
      if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"main","Could not create \"ppg cycle\" record (%d)\n",status);
        }
      else
        if (debug)printf("Success from create record for %s\n",str);
      
      /* Check again if the structure "ppg cycle"  exists  */
      status = db_find_key(hDB,0,str, &hCycle);
      if (status != DB_SUCCESS)
        {
          cm_msg(MERROR,"main","structure \"%s\"  is not present in the odb",str);
          goto error;
        }
      else
	cm_msg(MINFO,"main","WARNING structure \"%s\" has been recreated; may not be the latest version",str);
    }




        
  hLEGI=0;
  status = db_find_key(hDB,hCycle,"load EG ionize", &hLEGI);
  if (status != DB_SUCCESS)
    printf("did not find super-block \"load EG ionize\"\n");
  else
    printf("found super-block \"load EG ionize\" handle hLEGI=%d \n",hLEGI);
  // assemble this super-block 



  if (single_shot)
    {         /* single sweep */
      
      status = tr_precheck(10, info);

      
      if (status != SUCCESS)
	goto error;
      
      status = tr_prestart(10, info);
      if (status)
        {
          printf("Thank you\n");
 gettimeofday(&t4, NULL);  // end time
 // compute and print the elapsed time in millisec
	      
 elapsed_time = (t4.tv_sec - t3.tv_sec) * 1000.0;      // sec to ms
 elapsed_time += (t4.tv_usec - t3.tv_usec) / 1000.0;   // us to ms
 printf("tri_config: ending, elapsed time   %f ms\n",elapsed_time);
 cm_msg(MINFO,"tri_config","tri_config is ending , total elapsed time  %f ms",elapsed_time);

          goto exit; /* force exit */
        }
      else
        goto error;
    }
  
  /* initialize ss_getchar() */
  ss_getchar(0);
  do
    {
      while (ss_kbhit())
        {
          ch = ss_getchar(0);
	  if (ch == -1)
	    ch = getchar();
	  if (ch == 'R')
	    ss_clear_screen();
	  ss_clear_screen();
	  if ((char) ch == '!')
	    break;
	}
      msg = cm_yield(1000);
    } while (msg != RPC_SHUTDOWN && msg != SS_ABORT && ch != '!');
  
  
  printf("Thank you\n");
  goto exit;
  
 error:
  printf("\n Error detected. Check odb messages for details\n");

 exit:
  if(tfile)
    fclose(tfile); // make sure file is closed
 
  /* reset terminal */
  ss_getchar(TRUE);
  
  cm_disconnect_experiment();
  return 1;
}




