#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BLOCKNAME_TRUNCATE 10
#define BLOCKNAME_LEN 25
int main(void);
void check_string(char* my_string);
void check_string2(char *my_string, const char *block_type, int min);
void check_string0(char* my_ref_string, char *blockname);
void   test_string1(void);
void try_string(char *my_string);
char* orig_bn_array[100];
char* truncated_bn_array[100];
int bn_cntr;

#ifdef GONE
main()
{
    char strings[3][256];
    char my_string[]="This is a test";
 char my_string1[]="transition_test";
 char my_string2[]="pulse string very long indeed";
    
 sprintf(strings[0], "%s", my_string);
 sprintf(strings[1], "%s", my_string1);
 sprintf(strings[2], "%s", my_string2);	

    printf("strings= %s\n%s\n%s\n", strings[0], strings[1], strings[2]);
}
#endif
 

  char long_names[100][256]; // long_names
  char short_names[100][256]; // long_names
  int nn=0;


int main(void)
{
  char string[20];
  char string1[128];
  size_t size;
  int j;

  
  printf("hello world\n");
  size=sizeof(string);
  printf("sizeof string=%d\n",(int)size);
  size=sizeof(string1);
  printf("sizeof string1=%d\n",(int)size);
  
  sprintf(string,"hi");
  try_string(string);

  try_string("");
  test_string1();
  return;

  sprintf(string,"TRANSITION_TEST");
  
  
  if(strncmp(string,"TRANSITION",10)==0)
    {
      char *q=strchr(string, 'O');
      sprintf(string,"TRANS%s",q+2);
      
      printf("Now string= %s length=%d\n",string,strlen(string));
      
    }
  
  sprintf(string,"PULSE_this_is_a_long string");
  printf("string=%s\n",string);
  string[BLOCKNAME_TRUNCATE]='\0';
  printf("truncated string=%s\n",string);
  
  
  //------------------------------------------------------------
  
  sprintf(string,"TRANSITION_TEST_this_is_a_long_string");
  sprintf(long_names[nn], "%s",string);
  printf("\n\nString= %s length=%d\n",string,strlen(string));
  if(strncmp(string,"TRA",3)==0)
    {
      printf("found string starting TRA...\n");       
      check_string2(string,"TRANSITION",3);
    }
  printf("after check_string2, string=%s\n",string);
  
  sprintf(string,"PULSE_this_is_a_long_string");
  sprintf(long_names[nn], "%s",string);
  printf("\n\nString= %s length=%d\n",string,strlen(string));
  if(strncmp(string,"PUL",3)==0)
    {
      printf("found string starting PUL...\n");       
      check_string2(string,"PULSE",3);
    }
  printf("after check_string2, string=%s\n",string);
  
  
  sprintf(string,"STDPULSE_this_is_a_long_string");
  printf("\n\nString= %s length=%d\n",string,strlen(string));
  if(strncmp(string,"STD",3)==0)
    {
      printf("found string starting STD...\n");       
      check_string2(string,"STDPULSE",3);
    }
  printf("after check_string2, string=%s\n",string);
  
  sprintf(string,"PATTERN_this_is_a_long_string");
  sprintf(long_names[nn], "%s",string);
  printf("\n\nString= %s length=%d\n",string,strlen(string));
  if(strncmp(string,"PAT",3)==0)
    {
      printf("found string starting PAT...\n");       
      check_string2(string,"PATTERN",3);
    }
  printf("after check_string2, string=%s\n",string);
  
  
  sprintf(string,"DELAY_this_is_a_long_string");
  printf("\n\nString= %s length=%d\n",string,strlen(string));
  if(strncmp(string,"DEL",3)==0)
    {
      printf("found string starting DEL...\n");       
      check_string2(string,"DELAY",3);
    }
  printf("after check_string2, string=%s\n",string);
  
  sprintf(string,"TIMthis_is_a_long_string");
  printf("\n\nString= %s length=%d\n",string,strlen(string));
  if(strncmp(string,"TIM",3)==0)
    {
      printf("found string starting TIM...\n");       
      check_string2(string,"TIME_REF",3);
    }
  printf("after check_string2, string=%s\n",string);
  
  
  sprintf(string,"BEGIN_LOOP_this_is_a_long_string");
  printf("\n\nString= %s length=%d\n",string,strlen(string));
  if(strncmp(string,"BEGIN",5)==0)
    {
      printf("found string starting BEGIN...\n");       
      check_string2(string,"BEGIN_LOOP",5);
    }
  printf("after check_string2, string=%s\n",string);
  
  sprintf(string,"END_LOOP_this_is_a_long_string");
  printf("\n\nString= %s length=%d\n",string,strlen(string));
  if(strncmp(string,"END",3)==0)
    {
      printf("found string starting END...\n");       
      check_string2(string,"END_LOOP",3);
    }
  printf("after check_string2, string=%s\n",string);
  
  sprintf(string,"EVSTEthis_is_a_long_string");
  printf("\n\nString= %s length=%d\n",string,strlen(string));
  if(strncmp(string,"EVSTE",5)==0)
    {
      printf("found string starting EVSTE...\n");       
      check_string2(string,"EVSTEP",5);
    }
  printf("after check_string2, string=%s\n",string);
  
  sprintf(string,"EVSETtrap_this_is_a_long_string");
  printf("\n\nString= %s length=%d\n",string,strlen(string));
  if(strncmp(string,"EVSET",5)==0)
    {
      printf("found string starting EVSE...\n");       
      check_string2(string,"EVSET",5);
    }
  printf("after check_string2, string=%s\n",string);
  
  
  printf("\n\nIndex       Long Names            Short Names\n");
  for(j=0;j<nn; j++)
    printf("%d  %40.40s   %10.10s\n",j,long_names[j],short_names[j]);

  // check references
  //  sprintf(string,"_TEND_pulse_this_is_a_long_string");
  sprintf(string,"_TSTART_APEVSETTRAP_this_is_a_long_string");
  printf("string=%s\n",string);
  check_string0(string, "my_blocname");
  printf("after check_string0, string=%s\n",string);


  sprintf(string,"_TBEGthis_is_a_long_string");
  printf("string=%s\n",string);
  check_string0(string, "my_blocname");
  printf("after check_string0, string=%s\n",string);








  if(0)
    {
      sprintf(string,"BEGIN_loop_ramp"); // short name
      
      
      sprintf(string,"_TBEGloop_ramp");
      printf("string=%s\n",string);
      check_string(string);
      printf("after check_string, string=%s\n",string);
      
      sprintf(string,"_TENDloop_ramp_starting");  // long name
      printf("string=%s\n",string);
      check_string(string);
      printf("after check_string, string=%s\n",string);
    }
  
  printf("returning from main\n");
  return 1;
}

void try_string(char *my_string)
{
  int i;
  int len=strlen(my_string);
  char *p;
  p=my_string;



  printf("string=%s or %s  strlen=%d  \n",my_string,p,len);

  while (*p)
    {
      printf(" *p=%c\n",*p);
      p++;
    }
 
  return;
}

void check_string2(char *my_string, const char*block_type, int min )
{
  // cf get_short_name  tri_config.c

  int j,len;
  char temp_string[BLOCKNAME_LEN];

  len=strlen(my_string);
  for (j=0; j<len; j++)
    my_string[j] = toupper(my_string[j]);   


  sprintf(long_names[nn], "%s",my_string);
  
  char*p;
  char*q,*r;
  int m=0;

  len=strlen(my_string);
  if(len > BLOCKNAME_TRUNCATE)
    {
      sprintf(temp_string,"%s", my_string);
      sprintf(my_string,"                       ");
      my_string[BLOCKNAME_TRUNCATE]='\0';
      p=r=my_string; q=temp_string;
      for(j=0; j<strlen(block_type); j++)
	{
	//	if(strncmp(temp_string,block_type,j)!= 0) break;
        
	  if(*q == block_type[j])
	    {
              if(m < min)
		{
		  *p=*q;p++;q++;m++;
		}
	    
	      else
		q++;
	    }
	  else
	    break;
	}
    
      printf("after loop p=%s and my_string=%s\n",p,my_string);
      printf("after loop q=%s and temp_string=%s\n",q,temp_string);
      printf("after loop m=%d and j=%d index (p-r)=%d\n",m,j,(p-r));
    
        *p='_';p++;
	printf("added underscore. Now index (p-r)=%d, starting while loop\n",(p-r));

	while (*q) // until end of string
	{
	  printf(" *p=%c  index (p-r)=%d\n",*p,(p-r));

	  if(! *p)
	    {
              printf("Found *p = \n at index %d; breaking with my_string=%s\n", (p-&my_string[0]), my_string);
	    break;
            }
	  
	  if(*q=='_')
	    q++;
	  else
	    {
	      *p=*q; p++; q++;
	    }
	}
      
 
     /*     printf("broke from loop with j=%d temp_string=%s\n",j,temp_string);
      p=&my_string[min];
      printf("after loop p=%s and my_string=%s\n",p,my_string);
      *p='_';p++; // add an underscore
     printf("after adding underscore, p=%s and my_string=%s\n",p,my_string);

      q=&temp_string[j];
      printf(" q=%s and temp_string=%s\n",q,temp_string);
  
      
      for(j=0; j<BLOCKNAME_TRUNCATE; j++)
	{
	  printf("j=%d q=%s with temp_string=%s  and p=%s with my_string=%s\n",j,q,temp_string,p,my_string);
	  if(*q != '_' && *q != '-' ) // get rid of any _ or -
	    {
              printf("*q is not _ or - \n");
	      *p=*q;
	      printf("copied *q to *p, not yet incremented p=%s\n",p);
  printf("j=%d q=%s with temp_string=%s  and p=%s with my_string=%s\n",j,q,temp_string,p,my_string);
 
	      if(*p == '\0') break;
	     p++; 

	    }
	  q++;
	}
      
      *p='\0';
      */ 
    }
     
   *p='\0';
  printf("my_string=%s  and p=%s\n",my_string,p);
  sprintf(short_names[nn], my_string);
  nn++;
  printf("now blockname= %s\n",my_string);
  
}

void check_string0(char* my_ref_string, char *blockname)
{
  int len, maxlen;
  int j;
  char temp_string[128];
  char *p,*q;
  int l=0; // loopname offset

#define MAX_LOOPNAME_LEN 15

  // cf check_time_ref_string in get_params.c

  printf ("check_time_ref_string: starting with my_ref_string=%s\n",my_ref_string);
  len=strlen(my_ref_string);

  for (j=0; j<len; j++)
    my_ref_string[j] = toupper(my_ref_string[j]);   
  

  sprintf(temp_string,"%s",my_ref_string); // copy
      
 

// Look for _TSTART_AP
  if(strncmp(my_ref_string,"_TSTART_AP",10)==0)
    {
      printf("found _TSTART_AP \n");
      p=temp_string;
      p+=10;
      printf("temp_string=%s p=%s\n",temp_string,p);
    }


  // Look for _TEND_AP
  else if(strncmp(my_ref_string,"_TEND_AP",8)==0)
      {
	printf("found _TEND_AP \n");
	p=temp_string;
	p+=8;
	printf("temp_string=%s p=%s\n",temp_string,p);
    }


  // Look for _TEND_
  else if(strncmp(my_ref_string,"_TEND_",6)==0)
    {
      printf("found _TEND_ \n");
      p=temp_string;
      p+=6;
      printf("temp_string=%s p=%s\n",temp_string,p);
    }


 
  // Look for _TSTART_
  else if(strncmp(my_ref_string,"_TSTART_",8)==0)
    {
      printf("found _TSTART_ \n");
      p=temp_string;
      p+=8;
      printf("temp_string=%s p=%s\n",temp_string,p);
    }
  
  
  // Loops - get loop name, then truncate to MAX_LOOPNAME_LEN + "_TBEG"
  else if(strncmp(my_ref_string,"_TEND",5)==0)
    {
      printf("found _TEND (loop) \n");
      p=temp_string;
      p+=5; // this is the block name
      l=4; // offset to loopname from blockname
      printf("temp_string=%s p=%s l=%d\n",temp_string,p,l);
    }

    else if (strncmp(my_ref_string,"_TBEG",5)==0)  // loop
      {
	printf("found  _TBEG (loop) \n");
	p=temp_string;
	p+=5; // this is the block name
	l=6; // offset to loopname from blockname
	printf("temp_string=%s p=%s l=%d\n",temp_string,p,l);
      }


  // Look for _T
  else if(strncmp(my_ref_string,"_T",2)==0)
    {
      printf("found _T \n");
      p=temp_string;
      p+=2;
      printf("temp_string=%s p=%s\n",temp_string,p);
    }

  else
    {
      // cm_msg(MERROR,"check_time_ref_string","time reference string \"%s\" time ref prefix not recognized (block %s)",
      //	     my_ref_string, blockname);
      printf ("check_time_ref_string: time reference string \"%s\" time ref prefix not recognized (block %s)\n",
	      my_ref_string, blockname);
      // return  DB_INVALID_PARAM;
      return;
    }

  for(j=0;j<nn; j++)
    {
      if(strcmp(p,long_names[j])==0)
	{
	  printf("found p=%s at long_names[%d]\n",p,j);
	  printf("equivalent short_name is %s\n",short_names[j]);
	  q=short_names[j];
          if(l) // loop offset
	    { // reference is on the loop name itself so cut off BEGIN_ or END_ from blockname
              q+=l;
	      // loop_params[j].loopname);
	    }
	  strcpy(p,q);
          sprintf(my_ref_string, "%s", temp_string );
	  printf("time ref on short name is  %s\n",temp_string);
          break;
	}
       else
      	printf("Did not find block name p=%s in long_names\n",p);
    }
    
  return;


}



void check_string(char* my_string)
{
     printf("check_string starting with my_string=%s\n",my_string);
 
     int len=strlen(my_string);
     int j;
     char*p;
     char*q;

     for (j=0; j<len; j++)
       my_string[j] = toupper(my_string[j]);   
     my_string[len]='\0';
     int short_flag=0;
     if(strncmp(my_string,"_TEND_",6)==0)
       {

         p=&my_string[6];
	 printf("found string _TEND_... now string=%s\n",p);
         for (j=0; j<len; j++)
	   {
            
	     if(strcmp(p,long_names[j])==0)
	       break;
	     if(strcmp(p,short_names[j])==0)
	       {
		 short_flag=1;
		 break;
	       }
	   }

	 if(j<len)  printf("found name at at index %d short_flag=%d\n",j,short_flag);

	 // my_string[BLOCKNAME_TRUNCATE+7]='\0';
       }
     else if(strncmp(my_string,"_TSTART_",8)==0)
       {
	 printf("found string _TSTART__... truncating\n");
         my_string[BLOCKNAME_TRUNCATE+9]='\0';
       }
  
     else if((strncmp(my_string,"_TEND",5)==0) || (strncmp(my_string,"_TBEG",5)==0))  // loop
       {
	 printf("found string _TBEG or _TEND... truncating\n");
         printf("strlen=%d\n", strlen(my_string));
         if(strlen(my_string) > (BLOCKNAME_TRUNCATE+5))
	   my_string[BLOCKNAME_TRUNCATE+5]='\0';
	 printf("now strlen=%d\n", strlen(my_string));
	 printf("now loop name= %s\n",my_string);
	 /* don't need to do this 
         p=my_string;
         q=p+5;
         for(j=0; j<(BLOCKNAME_TRUNCATE+1); j++)
	   {
             *p=*q;
             printf("j=%d copied char %c\n",j,*p);
             if(*q=='\0') break;
             p++;q++;
           }
         if(*q|='\0')
           my_string[BLOCKNAME_TRUNCATE]='\0';
         printf("now loop name= %s\n",my_string);
         */

       }

     else if(strncmp(my_string,"_T",2)==0)
       {
	 printf("found string _T truncating\n");
         my_string[BLOCKNAME_TRUNCATE+2]='\0';
       }



     return;  
       
}


void test_string1(void)
{
  char Name[15];
  char Keyname[]="This a my key name";
  int j,len;

  len=strlen(Keyname);
    printf("test_string: Keyname:%s  length=%d\n",Keyname,len);
 if(len > (int)sizeof(Name))
    len=sizeof(Name) -1 ; // check size
  for   (j=0; j<len; j++)
    {
      Name[j]  = toupper (Keyname[j]);
      if(Name[j]==' ')
	{
	  Name[j]='_';
	  //printf("replaced space by underscore\n");
	}
    }

  printf("test_string: Name:%s  length=%d\n",Name,strlen(Name));

}
