#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#   Normally invoked from user button
# 
# invoke this script with cmd e.g.
#                  include_path                expt      alarm name      
# reset_alarm.pl  /home/cpet/online/ppg/perl   cpet         fecpet       
#
#######################################################################

my ($inc_dir, $expt, $alarm) = @ARGV;
my $len =$#ARGV;
my $mode;

unless ($inc_dir) { die "reset_alarm: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash

## add this for MIDAS access
if ($expt eq ""){ die "MIDAS_EXPT_NAME environment variable not defined";}
require "$inc_dir/perlmidas.pl";
MIDAS_env(); 

#unless ($expt eq "mpet" || $expt eq "ebit"|| $expt eq "cpet" || $expt eq "pol" )
unless ($expt eq "cpet")
{
    print LOG "ppg_plot:  Experiment name \"$expt\" unknown\n";
    die "ppg_plot:  Experiment name \"$expt\" unknown";
}

my $alarmpath="/alarms/alarms/$alarm/Triggered";
$mode =MIDAS_varget( $alarmpath);
    if ($mode=~/not found/)
    {
	die "reset_alarm: cannot read alarm path for $expt: $mode\n"; 
    }
$mode =MIDAS_varset( $alarmpath,0);
print "Alarm should now be cleared\n";
exit;
