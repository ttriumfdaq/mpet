/********************************************************************\

  Name:         fesdac.c
  Created by:   PAA

  Contents:     DA816 Slow DAC Output (SC equivalent)

  $Id: $

\********************************************************************/
#define  DA816_CODE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h> // time
#include "midas.h"
#include "da816.h"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fesdac";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 000;

/* maximum event size produced by this frontend */
INT max_event_size = 60000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 10 * 20000;


#define PARAM_SETTINGS_STR "\
Names = STRING[8] :\n\
[32] Sdac channel 1\n\
[32] Sdac channel 2\n\
[32] Sdac channel 3\n\
[32] Sdac channel 4\n\
[32] Sdac channel 5\n\
[32] Sdac channel 6\n\
[32] Sdac channel 7\n\
[32] Sdac channel 8\n\
"
typedef struct {
  float *demand;
  float *demand_mirror;
  float *measured;
} LSDAC_SETTINGS;
  LSDAC_SETTINGS *lsdac;

/* Globals */
extern HNDLE hDB;
HNDLE  hEq, hSet, hKeyDemand, hKeyMeasured;
BOOL   debug=0;
ALPHIDA816  *lda816;

/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
INT interrupt_configure(INT cmd, INT source, PTYPE adr);
extern INT poll_event(INT source, INT count, BOOL test);
INT read_sdac_event(char *pevent, INT off);
void lsdac_demand(INT hDB, INT hKey, void *info);
INT localsdac_init(char * eqname);

/*-- Equipment list ------------------------------------------------*/
#undef USE_INT

EQUIPMENT equipment[] = {

   {"SlowDac",                /* equipment name */
    {13, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC ,           /* equipment type */
     0,                      /* event source */
     "MIDAS",                /* format */
     FALSE,                   /* enabled */
     RO_ALWAYS | RO_TRANSITIONS |   /* read when running and on transitions */
     RO_ODB,                 /* and update ODB */
     1000,                  /* read every 10 sec */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* log history */
     "", "", "",},
    read_sdac_event,       /* readout routine */
    },

   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/********************************************************************/
void lsdac_demand(INT hDB, INT hKey, void *info)
{
  double arg;
  int i;

  for (i=0; i<8; i++) {
    if (lsdac->demand[i] != lsdac->demand_mirror[i]) {
      da816_DirectVoltWrite(lda816, (double) lsdac->demand[i], i, &arg);
      lsdac->demand_mirror[i] = lsdac->demand[i]; 
      printf("Channel %d changed to %f\n", i, lsdac->demand[i]);
    }
  }
  printf("odb ... trigger settings touched\n"); 
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
  int status;
  
  status = localsdac_init("SlowDac");
  printf("localmscb_init status:%d\n", status);
  
  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  printf("BOR\n");
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  printf("EOR\n");
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  return SUCCESS;
}

/*------------------------------------------------------------------*/
/********************************************************************\
  Readout routines for different events

\********************************************************************/
/*-- Trigger event routines ----------------------------------------*/
INT poll_event(INT source, INT count, BOOL test)
     /* Polling routine for events. Returns TRUE if event
	is available. If test equals TRUE, don't return. The test
	flag is used to time the polling */
{
  int i;
  int lam = 0;
  
  for (i = 0; i < count; i++, lam++) {
    lam = 1;
    if (lam)
      if (!test)
	return lam;
  }
  
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/
INT read_sdac_event(char *pevent, INT off)
{
  int i;
  float *pdata;
  
  printf("Sdac %d\n", SERIAL_NUMBER(pevent));
  bk_init(pevent);
  
  bk_create(pevent, "SDAS", TID_FLOAT, &pdata);
  // Get value from demand
  for (i=0;i<8; i++) {
    *pdata++ =  lsdac->demand[i];
  }
  bk_close(pevent, pdata);
  
  return bk_size(pevent);
}

/*-- Local MSCB event --------------------------------------------------*/
INT localsdac_init(char *eqname)
{
  int  i, nch = 8, samples;
  static INT status;
  char set_str[80];
  double arg;

  cm_get_experiment_database(&hDB, NULL);

  /* Map /equipment/Trigger/settings for the sequencer */
  sprintf(set_str, "/Equipment/%s", eqname);
  status = db_create_key(hDB, 0, set_str, TID_KEY);
  status = db_find_key (hDB, 0, set_str, &hEq);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"FE","Key %s not found", set_str);

  /* create PARAM settings record */
  status = db_create_record(hDB, hEq, "Settings", PARAM_SETTINGS_STR);
  if (status != DB_SUCCESS)
    return FE_ERR_ODB;

  status = db_find_key (hDB, hEq, "Settings", &hSet);
  lsdac = (LSDAC_SETTINGS *) calloc(1, sizeof(LSDAC_SETTINGS));
  lsdac->demand   = (float *) calloc(nch, sizeof(float));
  lsdac->demand_mirror = (float *) calloc(nch, sizeof(float));
  lsdac->measured = (float *) calloc(nch, sizeof(float));

  status = db_merge_data(hDB, hEq, "Variables/Demand", lsdac->demand, sizeof(float) * nch, nch , TID_FLOAT);
  status = db_find_key(hDB, hEq, "Variables/Demand", &hKeyDemand);

  status = db_open_record(hDB, hKeyDemand, lsdac->demand, nch * sizeof(float), MODE_READ, lsdac_demand,
		 &hKeyDemand);

  //   status = db_merge_data(hDB, hEq, "Variables/Measured", lsdac->measured, sizeof(INT) * nch, nch , TID_FLOAT);

  if (da816_Open(&lda816) < 0)
    {
      cm_msg(MERROR, "fesdac", "Cannot open PMC-DA816 device!");
      return 0;
    }

    samples = da816_MaxSamplesGet();
    samples = 0;
    da816_Setup(lda816, samples, 2);
    da816_ScaleSet(lda816, 10.0/32767. , 0.0);
    
    // Set initial value from ODB Demand
    for (i=0;i<8; i++) {
      lsdac->demand_mirror[i] =  lsdac->demand[i];
      da816_DirectVoltWrite(lda816, (double) lsdac->demand[i], i, &arg);
    }

  printf("End of sdac_init\n");

  return FE_SUCCESS;
}
