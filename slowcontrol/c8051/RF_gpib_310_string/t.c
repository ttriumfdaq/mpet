#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
  int i = 0,l    ;
  char bufstr[1024];
  char  buffer[16]; 
  
  l = atoi(argv[1]);
  memset(bufstr, 0, sizeof(bufstr));
  strcpy(bufstr, "DATA VOLATILE"); 
  for(i = 0; i < l ; i++) 
    { 
      sprintf(buffer, ",%4.2f", (double) ((((double)(2 * i)) / ((double)(l  -1))) - 1)); 
      strcat(bufstr, buffer); 
    } 
  
  printf("buffer:%s\n", buffer);
  printf("len:%d bufstr:%s\n", strlen(bufstr), bufstr);
}
