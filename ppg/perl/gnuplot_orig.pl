#!/usr/bin/perl 

$file="ppcobj/titan.dat";

open (OUT,"|gnuplot") or die;
print OUT<<eot;
set xlabel "time"
set title "transistions"
#set yrange [-1:6]
#set xrange [0:1.7]
plot \\
'$file' using 1:(\$2+0) with steps title "one", \\
'$file' using 1:(\$3+2) with steps title "two", \\
'$file' using 1:(\$4+4) with steps title "three",\\
'$file' using 1:(\$5+6) with steps title "four",\\
'$file' using 1:(\$6+8) with steps title "five",\\
'$file' using 1:(\$7+10) with steps title "six",\\
'$file' using 1:(\$8+12) with steps title "seven",\\
'$file' using 1:(\$9+14) with steps title "eight",\\
'$file' using 1:(\$10+16) with steps title "nine",\\
'$file' using 1:(\$11+18) with steps title "ten"

pause 10;
set term png colour 
set out "titan.png"
replot
eot
