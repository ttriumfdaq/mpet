#!/usr/bin/perl -w
use warnings;
use strict;
#
# Expands one of the loops (usually the inner loop) to either the loopcount supplied or to one input by the user
# Also used if no loops are to be expanded
# Converts the clock cycles to time values
#
# e.g. loops.pl inc_dir infile RAMP 10
#      loops.pl inc_dir infile LOOP2
#      loops.pl inc_dir infile
#
#  NOTE max loopcount for loopexpansion is 250;
#
#   inc_dir is usually ...ppg/perl
#   infile is usually ~/online/ppg/ppgplot/ppgplot.dat
#   i.e. file produced by convert_to_plot.pl
#
sub  readloopinfo($$);  # in readloopinfo.pl
sub write_times($$$$);
# input parameters  e.g. loops.pl ../ppgplot/ppgplot.dat  RAMP 5
# loopname of loop to expand (usually inner loop)  (Outermost  loop is loop 0)
# loopcount of loop can be supplied by user - else uses loopcount in the file

our ($inc_dir, $infile, $plotloopname, $my_lc ) = @ARGV; # input parameters
our $expt; # $expt is supplied by params.pm from environment variable MIDAS_EXPT_NAME
my $nparam=2; # need at least 2 input parameters to expand loops
our $DEBUG; #  supplied by params.pm
our $path;
unless ($inc_dir) { die "ppg_plot: No include directory path has been supplied\n";}
print "loops.pl: include directory is $inc_dir\n";
unless ($infile) { die "ppg_plot: No input file name  has been supplied\n";}
print "loops.pl: input filename is $infile\n";
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/params.pm"; # path
my $perlpath = $path."/perl";


my ($count,$lc);

my $ppgpath = $path;
my $loadpath = $ppgpath."ppgload/";
my $plotpath =  $ppgpath."ppgplot/";
#my $infile = $plotpath."ppgplot.dat";
my $loopfile = $plotpath."ppgloops.dat";
my $outfile = $plotpath."ppgplotloop.dat";
my $plotparams = $plotpath."plotparams.dat";
my $timesfile = $plotpath."ppgplottimes.dat";
my ($tmp,$e,$i,$my_name);
my (@temp,@fields);
my ($plotloopnum,$start,$end);
my @lines;
my @altlines;
my $plindex=0;
my ($last);#,$time,$time_offset,$init_time);
my @last_lines;
my @tempnames;
our %loopinfo; # store the loop information in a hash of hashes
our $nloops;
our $max_loopcount; # maximum combined loopcount to keep files small (in params.pm)
my $j;
$|=1; # flush output buffers
$count = @ARGV;
my $num_inputs_used;
my $one_loop_offset;
my ($ins,$index,$w);
my @cycle = qw (0.5 1);
my ($lidx,$alt);
my ($expand_loop,$string);


require "$inc_dir/readloopinfo.pl";


unless ($infile)
{
    die "FAILURE: not enough parameters supplied; expect at least input filename \n";
}


$nloops=0;


unless (-e $loopfile)
    {
        die "loops.pl:   no such file as $loopfile\n";
    }
# read information from the loopfile

$nloops = readloopinfo($perlpath, $loopfile);
$nloops +=0;
if($nloops < 0)
{   
   die "loops.pl: Error return from readloopinfo reading file $loopfile \n";  
}

print "loops.pl:  readloopinfo has found $nloops loops in file $loopfile \n"; 

print "loops.pl:  count=$count  nparam=$nparam\n";
print "           input filename: $infile  expt=$expt\n";



if ( ( $nloops == 0 ) || ($count <= $nparam) )
{ 
  $expand_loop=0; 
  $lc=1; # one loop
  print "loops.pl: no loops will be expanded\n";
}
else 
{  # loop is to be expanded
    print "loops.pl: $nloops  loop(s) will be expanded\n";
    unless ($plotloopname) { die "No loopname for expansion is supplied. Check count=$count and nparam=$nparam are correct"; }
    $plotloopname = uc $plotloopname;
    $expand_loop = 1;
    if ($nloops == 0) { die "no loops found. Cannot expand loops";}
    
    print "Loop information read from file $loopfile:\n";
    for $w (keys %loopinfo) {
	print "\n loop: $w \n";
	print "start: ",$loopinfo{$w}->{start},"\n";
	print "end: ",$loopinfo{$w}->{end},"\n";
	print "count: ",$loopinfo{$w}->{count},"\n";
	print "index: ",$loopinfo{$w}->{index},"\n";
    }
    unless ($my_lc) 
    { 
	print "No loopcount for expansion is supplied. Using loopcount in file\n"; 
	$my_lc = 0;
    }

    if ($my_lc > 0) 
    {
	$lc = $my_lc; # user-supplied loop count
    }
    else
    {
	$lc = $loopinfo{$plotloopname}->{count}; # loop count from the file
    }
    print "lc = $lc and max is $max_loopcount\n";

    if($lc > $max_loopcount)
    {
	print "loops : loop expansion is limited to $max_loopcount \n";
	$lc=$max_loopcount;
      
    }
    $plotloopnum = $loopinfo{$plotloopname}->{index};
    $start =  $loopinfo{$plotloopname}->{start}  + 1 ; # begin loop instruction is outside looping
    $end =  $loopinfo{$plotloopname}->{end} +0 ;
    print "Loop $plotloopname (loop index $plotloopnum) will be expanded with a loopcount of $lc\n";
    print "Loop $plotloopname starts at ins $start and ends at $end\n";    
}

unless (-e $infile)
{
    die " loops.pl : no such file as $infile\n";
}
open (IN,$infile) or die $!;
print "loops.pl:  opening infile \"$infile\"\n";

# Open output file 
print "loops.pl: opening outfile \"$outfile\"\n";
open (OUT,">$outfile") or ($tmp= $!);
if ($tmp)
{ 
    die "FAILURE cannot open file \"$outfile\"; $tmp"  ;
}
my $line_index=0;
my $hflag=0;
my $iflag=0;
my $lcount=0;
while (<IN>)
{
    chomp $_;
    
    if(/^( +)?\#/) # skip  "#" at beginning of the line
    {
	unless($hflag >= 2)
	{
            if ($hflag == 0)
            {
	       # first line contains # followed by input and loop names
	       @fields=split;
	       $num_inputs_used = $#fields; # not  ($#fields + 1) because first field is the hash
	       print OUT "@fields\n";  # print header
	       print   "@fields\n";  # print header
            }
            else
            {
		#copy second line
		print OUT "$_\n";
	    }
	    $hflag++; # set a flag to say we found the header
	}
	next;
    }

    unless($expand_loop)
    {    # no loops are to be expanded. Copy input to output
	print OUT "$_\n";
	next;
    }


    unless(/\d/)
    {
	print "blank line\n";
	next;
    }
    
    if($DEBUG){print "working on line:\"$_\"\n"; }
    @fields=split;
    my $len = $#fields;
    unless ($iflag)
    {   # first instruction ; find the index to the array that has "#" ; all instructions have a # preceding the instruction number
	$iflag=1;
	$i=$index=0;
	while ($i < $len)
        {
	    if ($fields[$i] eq "#"){ $index = $i + 1; last;}
	    $i++;
	}
	if ($index == 0)  { die "no index to instruction number found";}
    }
    $ins = $fields[$index] + 0;
    if($DEBUG){print "instruction number $ins\n";}
    
    $i=0;
    $lidx = $nloops - $plotloopnum  ; # array index  for cycling loop plot
    $tmp = $fields [$lidx] + 0;
    if($DEBUG){print "$plotloopnum, index $lidx, $tmp\n";}
    #print "@fields\n";
   
    if  ($tmp == 0)
    {  # these lines are outside the loop we want to expand
	if($DEBUG){print "found line OUTSIDE loop to be expanded - ";}
        if ($ins < $start ) 
	{
	    if($DEBUG){print " (before loop) \n";}
	}
        elsif ($ins > $end )  
	{
	    if($DEBUG){print " (after loop) \n";}
	}
        else   
	{
	    die " (ERROR - loop fields indicated should be inside loop to expand...  $ins $start $end ";
	}	
	 
	print OUT "$_\n";      # write the lines in the output file
	next;
    }
    
# we are IN the loop now
    if($DEBUG){print "found line INSIDE loop to be expanded   "; }
    $alt = 1;  # alternate the expanded loop plot between 1 and 0.5 to show each loop
    $lidx = $nloops - $plotloopnum  ; # array index  for cycling loop plot
    if  ($lidx < 1) { die "error with  index $lidx (nloops=$nloops, plotloopnum = $plotloopnum \n";   }
    #print "index $lidx (nloops=$nloops, plotloopnum = $plotloopnum \n";
    
    if($DEBUG)
    {
	if ($ins == $start)  {print " (at start of repeat loop) \n";}
	elsif (($ins > $start) &&  ($ins < $end ) ) {print " (inside repeat loop) \n";}
	elsif ($ins == $end  ) {print " (at end of repeat loop) \n";}
	else   {print " (IL ERROR $ins $start $end) \n";}
    }
    print OUT "$_";
    if ($ins == $start) 
    { 
	$lcount=1;
	print OUT " expanding loop $plotloopname; loop count=$lc\n"; 
    }
    elsif  ($ins == $end)
    {
	print OUT "at end of loop  $plotloopname; loop count=$lc\n";
    }
    else { print OUT "\n"; }
    
    
    $lines[$line_index] = "$_";
    @temp = split ' ', $_; 
    $temp[$lidx] = 0.5;
    $tmp = join " ", @temp;
    $altlines[$line_index] = "$tmp";
    
    if($DEBUG)
    {
	print "line: $lines[$line_index]\n";
	print "altline: $altlines[$line_index]\n";
    }
    $line_index++;
    
    if ($ins == $end)
    { # expand the loop
        
        $lcount++; # starting with loop 2
	while ($lcount <= $lc)
	{
	    $i=0;
            #print "lcount=$lcount; lc=$lc; line_index = $line_index\n";
            $string=" ; expanding $plotloopname: loop $lcount";
	    if ($alt == 1){ $alt=0; }
	    elsif ($alt == 0){ $alt=1; }
	    
	    while ($i < $line_index)
	    {
		if ($alt ==1 ){ print OUT  "$lines[$i] $string\n";}
		else          { print OUT  "$altlines[$i]  $string\n";}		
		$i++;
		$string="";
	    }
	    $lcount++;
	}
    }
} # end of while loop

close IN;
close OUT;

my $total_time = write_times($outfile,$timesfile,$nloops,$expand_loop);

# write plotparams file
print "loops.pl :  opening $plotparams for append\n";
open (OUT,">>$plotparams") or die "Cannot append to file $plotparams   $! ";
#open (OUT,">$plotparams") or die "Cannot open file $plotparams   $! ";
if($expand_loop) { print OUT "$timesfile $num_inputs_used 0 $total_time rn  $nloops $plotloopname $lc \n"; }
else            { print OUT  "$timesfile $num_inputs_used 0 $total_time rn  $nloops  \n"; }
close OUT;
print "Output files are $outfile and  $timesfile \n";
print "Output plotparams file is $plotparams\n";
print "\nloops.pl is ending\n\n";


sub write_times($$$$)
{
    my $infile = shift;
    my $outfile=shift;
    my $nloops = shift;
    my $expand_loop=shift;

    my $total_time =0.0;
    my $clock_cycle = 1/100000; # in ms
    my $total_clock =0;
    my $clock;

    my $tmp;
    my @fields;
    my $i;
    my $tempfile;
    my ($time_zero, $time);
    our $path;

    $tempfile = $path."ppgplot/tempfile.dat";
    print "write_times: tempfile = $tempfile\n";
    
    unless (-e $infile)
    {
	die "write_times: no such file as $infile\n";
    }

    print "write_times: starting with parameters input file: $infile; output file: $outfile; num loops: $nloops; expand loop= $expand_loop\n";

# Open input file $infile 
    open (IN,$infile) or  ($tmp= $!);
    if ($tmp)
    { 
	die "FAILURE cannot open file \"$infile\"; $tmp"  ;
    }

# Open output file $tempfile 
    print "write_times: opening $tempfile\n";
    open (OUT,">$tempfile") or ($tmp= $!);
    if ($tmp)
    { 
	die "FAILURE cannot open file \"$outfile\"; $tmp"  ;
    }

    while (<IN>)
    {
	chomp $_;
	
	if(/^( +)?\#/) # skip  "#" at beginning of the line
	{
	    if($DEBUG){ print "$_\n";}
	    if(/(Clk Cycles )/) 
	    { 
		if($DEBUG)
		{
		    print "gotcha $1 \n";  s/$1/  Time(ms) /;
		    print "now $_ \n";
		} 
                s/ L/Loop/g;

		if($DEBUG){print "now $_ \n"; }
	    }
	    print OUT "$_\n";  # write the lines to the output file
	    next;
	}
	unless(/\d/)
	{
	    if($DEBUG){print "blank line\n";}
	    next;
	}
	
	@fields = split;
	$clock = shift @fields ;
        $clock += 3; # 3 clock cycles per instruction
      
	$total_clock += $clock;
	$total_time = $total_clock * $clock_cycle;  # in ms
   
       	printf OUT ("%12.6f ",$total_time); 

        unless($expand_loop)
	{
	    printf OUT "@fields\n";
	    next;
	}
               
	$i=0;
        while ($i<$nloops)
	{
	    $tmp = shift @fields;
        
	    printf OUT ("%3.1f ",$tmp);
	    $i++;
	}
        print OUT "@fields\n";
    } # end of while IN
    close IN;
    close OUT;


#   Now reopen OUT file $tempfile as IN
    open (IN,$tempfile) or  ($tmp= $!);
    if ($tmp)
    { 
	die "FAILURE cannot open file \"$tempfile\"; $tmp"  ;
    }

#  Open $outfile as OUT
    print "write_times: opening $outfile\n";
    open (OUT,">$outfile") or ($tmp= $!);
    if ($tmp)
    { 
	die "FAILURE cannot open file \"$outfile\"; $tmp"  ;
    }

#   now add time zero and shift everything down
    $time_zero = 0;
    while (<IN>)
    {
	chomp $_;
	
	if(/^( +)?\#/) # skip  "#" at beginning of the line
	{
            print OUT "$_\n";  # write the comment lines to the output file
	    next;
	}

	unless(/\d/)
	{
	    if($DEBUG){print "blank line\n";}
	    next;
	}

        @fields = split;
        $time = shift @fields;
       	printf OUT ("%12.6f ",$time_zero);
        $time_zero = $time;
        print OUT "@fields\n";        
    } # end of while IN

# print last time 
    printf OUT ("%12.6f ",$time_zero);
    print OUT "@fields\n";   # keep the same pattern
    close IN;
    close OUT;

    print "write_times: returning total time = $total_time\n";
    return $total_time;
}
