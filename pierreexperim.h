/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Tue Jul 17 12:00:15 2007

\********************************************************************/

#define EXP_EDIT_DEFINED

typedef struct {
  INT       loop_count;
  BOOL      pedestals_run;
  BOOL      write_data;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) char *_name[] = {\
"[.]",\
"num ppg cycles = LINK : [53] /Equipment/TITAN_acq/ppg cycle/begin_scan/loop count",\
"Pedestals run = BOOL : n",\
"Write Data = LINK : [19] /Logger/Write data",\
"",\
NULL }

#ifndef EXCL_TRIGGER

#define TRIGGER_SETTINGS_DEFINED

typedef struct {
  BOOL      vt2_keep_alive;
  BOOL      vt2_cycle_reset;
} TRIGGER_SETTINGS;

#define TRIGGER_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"vt2 keep alive = BOOL : n",\
"vt2 cycle reset = BOOL : y",\
"",\
NULL }

#define TRIGGER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 2000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fempet",\
"Frontend file name = STRING : [256] fempet.c",\
"",\
NULL }

#endif

#ifndef EXCL_TITAN_ACQ

#define TITAN_ACQ_PPG_CYCLE_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    struct {
      double    time_offset__ms_;
      char      time_reference[20];
      INT       loop_count;
    } begin_lstep;
    struct {
      char      time_reference[20];
      char      ppg_signal_name[15];
    } trans_gate_on;
    struct {
      double    time_offset__ms_;
      char      time_reference[20];
      INT       awg_unit;
      char      start_value__volts_[128];
      char      end_value__volts_[128];
      char      nsteps_before_ramp[128];
      char      nsteps_after_ramp[128];
    } evstep;
    struct {
      double    time_offset__ms_;
      char      time_reference[20];
    } end_lstep;
    struct {
      char      time_reference[20];
      char      ppg_signal_name[15];
    } trans_gate_off;
  } step_awg;
  struct {
    double    time_offset__ms_;
  } end_scan;
  struct {
  } skip;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    INT       loop_count;
  } begin_loop;
  struct {
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_low;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      start_value__volts_[128];
    char      end_value__volts_[128];
    char      nsteps_before_ramp[128];
    char      nsteps_after_ramp[128];
  } evstep;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_high;
  struct {
    double    time_offset__ms_;
  } end_loop;
  struct {
    INT       awg_unit;
    char      set_values__volts_[128];
    double    time_offset__ms_;
  } evset_zero;
} TITAN_ACQ_PPG_CYCLE;

#define TITAN_ACQ_PPG_CYCLE_STR(_name) char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0.01",\
"loop count = INT : 1",\
"",\
"[step AWG/begin lstep]",\
"time offset (ms) = DOUBLE : 0.0001",\
"time reference = STRING : [20] ",\
"loop count = INT : 2",\
"",\
"[step AWG/trans_gate_on]",\
"time reference = STRING : [20] _TBEGLSTEP",\
"ppg signal name = STRING : [15] tdcgate",\
"",\
"[step AWG/evstep]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGLSTEP",\
"AWG unit = INT : 0",\
"start value (Volts) = STRING : [128] (0,-10)",\
"end value (Volts) = STRING : [128] (0,10)",\
"Nsteps before ramp = STRING : [128] (0,0)",\
"Nsteps after ramp = STRING : [128] (0,0)",\
"",\
"[step AWG/end_lstep]",\
"time offset (ms) = DOUBLE : 0.0001",\
"time reference = STRING : [20] ",\
"",\
"[step AWG/trans_gate_off]",\
"time reference = STRING : [20] _TENDLSTEP",\
"ppg signal name = STRING : [15] tdcgate",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 0.01",\
"",\
"[begin_loop]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] ",\
"loop count = INT : 10000000",\
"",\
"[evset_low]",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-10)",\
"",\
"[evstep]",\
"time offset (ms) = DOUBLE : 0",\
"AWG unit = INT : 0",\
"start value (Volts) = STRING : [128] (0,-10)",\
"end value (Volts) = STRING : [128] (0,10)",\
"Nsteps before ramp = STRING : [128] (0,0)",\
"Nsteps after ramp = STRING : [128] (0,0)",\
"",\
"[evset_high]",\
"time offset (ms) = DOUBLE : 0.01",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,10)",\
"",\
"[end_loop]",\
"time offset (ms) = DOUBLE : 0.002",\
"",\
"[evset_zero]",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,0)",\
"time offset (ms) = DOUBLE : 0.01",\
"",\
NULL }

#define TITAN_ACQ_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      experiment_name[32];
      char      ppgload_path[50];
      char      ppg_path[50];
      char      ppg_perl_path[50];
      float     daq_service_time__ms_;
      float     standard_pulse_width__ms_;
      float     awg_clock_width__ms_;
      float     tdcblock_width__ms_;
      float     ppg_clock__mhz_;
    } input;
    struct {
      char      compiled_file_time[32];
      DWORD     compiled_file_time__binary_;
      float     time_slice__ms_;
      float     ppg_nominal_frequency__mhz_;
      float     minimal_delay__ms_;
      float     ppg_freq_conversion_factor;
    } output;
  } ppg;
  struct {
    DWORD     number_of_steps;
    double    step_delay__ms_;
    double    vset__volts_[8];
    double    vend__volts_[8];
    INT       nsteps_before_ramp[8];
    INT       nsteps_after_ramp[8];
  } awg0;
  struct {
    DWORD     number_of_steps;
    double    step_delay__ms_;
    double    vset__volts_[8];
    double    vend__volts_[8];
    INT       nsteps_before_ramp[8];
    INT       nsteps_after_ramp[8];
  } awg1;
} TITAN_ACQ_SETTINGS;

#define TITAN_ACQ_SETTINGS_STR(_name) char *_name[] = {\
"[ppg/input]",\
"Experiment name = STRING : [32] mpet",\
"PPGLOAD path = STRING : [50] /home/mpet/online/ppg/ppgload",\
"PPG path = STRING : [50] /home/mpet/online",\
"PPG perl path = STRING : [50] /home/mpet/online/ppg/perl",\
"DAQ service time (ms) = FLOAT : 0",\
"standard pulse width (ms) = FLOAT : 5",\
"AWG clock width (ms) = FLOAT : 10",\
"TDCBLOCK width (ms) = FLOAT : 0.001",\
"PPG clock (MHz) = FLOAT : 20",\
"",\
"[ppg/output]",\
"compiled file time = STRING : [32] Fri Jul 13 10:37:05 2007",\
"compiled file time (binary) = DWORD : 1184348225",\
"Time Slice (ms) = FLOAT : 5e-05",\
"PPG nominal frequency (MHz) = FLOAT : 10",\
"Minimal Delay (ms) = FLOAT : 0.00025",\
"PPG freq conversion factor = FLOAT : 2",\
"",\
"[awg0]",\
"number of steps = DWORD : 5",\
"step delay (ms) = DOUBLE : 125",\
"Vset (volts) = DOUBLE[8] :",\
"[0] -10",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"Vend (volts) = DOUBLE[8] :",\
"[0] 10",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"Nsteps before ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 1",\
"[6] 2",\
"[7] 3",\
"Nsteps after ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 3",\
"[6] 2",\
"[7] 1",\
"",\
"[awg1]",\
"number of steps = DWORD : 1",\
"step delay (ms) = DOUBLE : 100",\
"Vset (volts) = DOUBLE[8] :",\
"[0] 1",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 1.5",\
"[6] 0",\
"[7] 0",\
"Vend (volts) = DOUBLE[8] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"Nsteps before ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"Nsteps after ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"",\
NULL }

#define TITAN_ACQ_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TITAN_ACQ_COMMON;

#define TITAN_ACQ_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 11",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fempet",\
"Frontend file name = STRING : [256] fempet.c",\
"",\
NULL }

#endif

#ifndef EXCL_TRICAN

#define TRICAN_EVENT_DEFINED

typedef struct {
  float     demand_volt[10];
  BOOL      demand_switch[10];
  BOOL      demand_pol[10];
  float     measured_volt[10];
  float     measured_temp[10];
  BOOL      measured_switch[10];
  BOOL      measured_pol_switch[10];
  BOOL      measured_device[10];
} TRICAN_EVENT;

#define TRICAN_EVENT_STR(_name) char *_name[] = {\
"[.]",\
"Demand Volt = FLOAT[10] :",\
"[0] 100",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"Demand Switch = BOOL[10] :",\
"[0] y",\
"[1] n",\
"[2] n",\
"[3] n",\
"[4] n",\
"[5] n",\
"[6] n",\
"[7] n",\
"[8] n",\
"[9] n",\
"Demand Pol = BOOL[10] :",\
"[0] y",\
"[1] n",\
"[2] n",\
"[3] n",\
"[4] n",\
"[5] n",\
"[6] n",\
"[7] n",\
"[8] n",\
"[9] n",\
"Measured Volt = FLOAT[10] :",\
"[0] 8",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"Measured Temp = FLOAT[10] :",\
"[0] 8",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"Measured Switch = BOOL[10] :",\
"[0] y",\
"[1] n",\
"[2] n",\
"[3] n",\
"[4] n",\
"[5] n",\
"[6] n",\
"[7] n",\
"[8] n",\
"[9] n",\
"Measured Pol switch = BOOL[10] :",\
"[0] y",\
"[1] n",\
"[2] n",\
"[3] n",\
"[4] n",\
"[5] n",\
"[6] n",\
"[7] n",\
"[8] n",\
"[9] n",\
"Measured Device = BOOL[10] :",\
"[0] n",\
"[1] n",\
"[2] n",\
"[3] n",\
"[4] n",\
"[5] n",\
"[6] n",\
"[7] n",\
"[8] n",\
"[9] n",\
"",\
NULL }

#define TRICAN_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TRICAN_COMMON;

#define TRICAN_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 8",\
"Source = INT : 0",\
"Format = STRING : [8] FIXED",\
"Enabled = BOOL : y",\
"Read on = INT : 121",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] titan01.triumf.ca",\
"Frontend name = STRING : [32] TRICAN Frontend",\
"Frontend file name = STRING : [256] frontend.c",\
"",\
NULL }

#define TRICAN_SETTINGS_DEFINED

typedef struct {
  struct {
    INT       tpmc;
  } channels;
  char      names[10][32];
  float     update_threshold_measured;
  float     voltage_limit[10];
  struct {
    struct {
      struct {
        char      device_name[32];
        INT       device_number;
        INT       bittiming_kbps_;
      } dd;
      INT       canbus_addresses[10];
      DWORD     ioflags[10];
      struct {
        INT       bittimingindex;
        BOOL      threesamplesperbittime;
        char      device[32];
        INT       bittimingreg0;
        INT       bittimingreg1;
      } bd;
    } tpmc;
  } devices;
  float     update_threshold_measured_volt[10];
  float     update_threshold_measured_temp[10];
} TRICAN_SETTINGS;

#define TRICAN_SETTINGS_STR(_name) char *_name[] = {\
"[Channels]",\
"TPMC = INT : 10",\
"",\
"[.]",\
"Names = STRING[10] :",\
"[32] EL pwr",\
"[32] InjectionDT",\
"[32] Lorentz St",\
"[32] Pulsed DT 1",\
"[32] Pulsed DT 2",\
"[32] MPET Entr",\
"[32] MPET Cap",\
"[32] MPET Dnut",\
"[32] MPET Exit",\
"[32] MPET Seg 1",\
"Update Threshold Measured = FLOAT : 2",\
"Voltage Limit = FLOAT[10] :",\
"[0] 3000",\
"[1] 3000",\
"[2] 3000",\
"[3] 3000",\
"[4] 3000",\
"[5] 3000",\
"[6] 3000",\
"[7] 3000",\
"[8] 3000",\
"[9] 3000",\
"",\
"[Devices/TPMC/DD]",\
"device name = STRING : [32] /dev/tpmc810",\
"device number = INT : 0",\
"BitTiming(Kbps) = INT : 100",\
"",\
"[Devices/TPMC]",\
"canbus addresses = INT[10] :",\
"[0] 1",\
"[1] 77",\
"[2] 86",\
"[3] 33",\
"[4] 90",\
"[5] 6",\
"[6] 21",\
"[7] 67",\
"[8] 38",\
"[9] 11",\
"ioflags = DWORD[10] :",\
"[0] 2",\
"[1] 2",\
"[2] 2",\
"[3] 2",\
"[4] 2",\
"[5] 2",\
"[6] 2",\
"[7] 2",\
"[8] 2",\
"[9] 2",\
"",\
"[Devices/TPMC/BD]",\
"BitTimingIndex = INT : 5",\
"ThreeSamplesPerBitTime = BOOL : n",\
"device = STRING : [32] /dev/tpmc810_0",\
"BitTimingReg0 = INT : 0",\
"BitTimingReg1 = INT : 0",\
"",\
"[.]",\
"Update Threshold Measured Volt = FLOAT[10] :",\
"[0] 2",\
"[1] 2",\
"[2] 2",\
"[3] 2",\
"[4] 2",\
"[5] 2",\
"[6] 2",\
"[7] 2",\
"[8] 2",\
"[9] 2",\
"Update Threshold Measured Temp = FLOAT[10] :",\
"[0] 3",\
"[1] 3",\
"[2] 3",\
"[3] 3",\
"[4] 3",\
"[5] 3",\
"[6] 3",\
"[7] 3",\
"[8] 3",\
"[9] 3",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fempet",\
"Frontend file name = STRING : [256] fempet.c",\
"",\
NULL }

#endif

#ifndef EXCL_BEAMLINE

#define BEAMLINE_EVENT_DEFINED

typedef struct {
  float     demand[20];
  float     measured[20];
} BEAMLINE_EVENT;

#define BEAMLINE_EVENT_STR(_name) char *_name[] = {\
"[.]",\
"Demand = FLOAT[20] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 30",\
"Measured = FLOAT[20] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"",\
NULL }

#define BEAMLINE_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      channel_name[20][32];
    } beamline;
  } devices;
  char      names[20][32];
  float     update_threshold_measured[20];
} BEAMLINE_SETTINGS;

#define BEAMLINE_SETTINGS_STR(_name) char *_name[] = {\
"[Devices/Beamline]",\
"Channel name = STRING[20] :",\
"[32] TITAN:VAR1",\
"[32] TITAN:VAR2",\
"[32] TITAN:VAR3",\
"[32] TITAN:VAR4",\
"[32] TITAN:VAR5",\
"[32] TITAN:VAR6",\
"[32] TITAN:VAR7",\
"[32] TITAN:VAR8",\
"[32] TITAN:VAR9",\
"[32] TITAN:VAR10",\
"[32] Channel10",\
"[32] Channel11",\
"[32] Channel12",\
"[32] Channel13",\
"[32] Channel14",\
"[32] Channel15",\
"[32] Channel16",\
"[32] Channel17",\
"[32] Channel18",\
"[32] Channel19",\
"",\
"[.]",\
"Names = STRING[20] :",\
"[32] VAR1",\
"[32] VAR2",\
"[32] VAR3",\
"[32] VAR4",\
"[32] VAR5",\
"[32] VAR6",\
"[32] VAR7",\
"[32] VAR8",\
"[32] VAR9",\
"[32] Watchdog",\
"[32] Default%CH 10",\
"[32] Default%CH 11",\
"[32] Default%CH 12",\
"[32] Default%CH 13",\
"[32] Default%CH 14",\
"[32] Default%CH 15",\
"[32] Default%CH 16",\
"[32] Default%CH 17",\
"[32] Default%CH 18",\
"[32] Default%CH 19",\
"Update Threshold Measured = FLOAT[20] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"[16] 1",\
"[17] 1",\
"[18] 1",\
"[19] 1",\
"",\
NULL }

#define BEAMLINE_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} BEAMLINE_COMMON;

#define BEAMLINE_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] FIXED",\
"Enabled = BOOL : y",\
"Read on = INT : 121",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] titan01.triumf.ca",\
"Frontend name = STRING : [32] feEpics",\
"Frontend file name = STRING : [256] frontend.c",\
"",\
NULL }

#endif

#ifndef EXCL_SLOWDAC

#define SLOWDAC_SETTINGS_DEFINED

typedef struct {
  char      names[8][32];
} SLOWDAC_SETTINGS;

#define SLOWDAC_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Names = STRING[8] :",\
"[32] Sdac channel 1",\
"[32] Sdac channel 2",\
"[32] Sdac channel 3",\
"[32] Sdac channel 4",\
"[32] Sdac channel 5",\
"[32] Sdac channel 6",\
"[32] Sdac channel 7",\
"[32] Sdac channel 8",\
"",\
NULL }

#define SLOWDAC_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SLOWDAC_COMMON;

#define SLOWDAC_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 16",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 511",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 10",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fesdac",\
"Frontend file name = STRING : [256] fesdac.c",\
"",\
NULL }

#endif

#ifndef EXCL_RF

#define RF_SETTINGS_DEFINED

typedef struct {
  char      names[2][32];
  struct {
    char      mscb_device[32];
    char      mscb_pwd[32];
    INT       base_address;
  } dd;
} RF_SETTINGS;

#define RF_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Names = STRING[5] :",\
"[32] RF_1 monitor (V)",\
"[32] RF_2 monitor (V)",\
"[32] RF_3 monitor (V)",\
"[32] RF_4 monitor (V)",\
"[32] uC Temperature (C)",\
"",\
"[DD]",\
"MSCB Device = STRING : [32] usb0",\
"MSCB Pwd = STRING : [32] ",\
"Base Address = INT : 4096",\
"",\
NULL }

#define RF_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} RF_COMMON;

#define RF_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 33",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 511",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] feRf",\
"Frontend file name = STRING : [256] ferf.c",\
"",\
NULL }

#endif

