/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Wed Mar 20 16:52:22 2019

\********************************************************************/

#define EXP_PARAM_DEFINED

typedef struct {
} EXP_PARAM;

#define EXP_PARAM_STR(_name) const char *_name[] = {\
"",\
NULL }

#define EXP_EDIT_DEFINED

typedef struct {
  char      num_ppg_cycles[53];
  char      write_data[19];
  char      convert_file[35];
  char      capture_delay__ms_[67];
  char      plt_delay__ms_[63];
  char      species[30];
  char      charge[29];
  char      number_frequency_points[53];
  char      afg_on[29];
  char      quad_rf_time__ms_[65];
  char      centre_frequency__hz_[39];
  char      frequency_deviation__hz_[42];
  BOOL      amplitude_override;
  char      mpetrfamp__v_[32];
  char      dipole_on[41];
  char      dipole_rf_time__ms_[64];
  char      dipole_species[42];
  char      dipole_amplitude__v_[51];
  char      dipole_freqc__hz_[56];
  char      dipole_freqmod__hz_[60];
  char      feedbackfilename[21];
  char      ppg_start_delay__ms_[66];
  BOOL      pedestals_run;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) const char *_name[] = {\
"[.]",\
"num ppg cycles = LINK : [53] /Equipment/TITAN_acq/ppg cycle/begin_scan/loop count",\
"Write Data = LINK : [19] /Logger/Write data",\
"Convert File = LINK : [35] /Experiment/Variables/Convert File",\
"Capture delay (ms) = LINK : [67] /Equipment/TITAN_ACQ/ppg cycle/evset_Trap_Capture/time offset (ms)",\
"PLT delay (ms) = LINK : [63] /Equipment/TITAN_ACQ/ppg cycle/pulse_PLT_Down/time offset (ms)",\
"Species = LINK : [30] /Experiment/Variables/Species",\
"Charge = LINK : [29] /Experiment/Variables/Charge",\
"Number Frequency Points = LINK : [53] /Equipment/TITAN_ACQ/ppg cycle/begin_ramp/loop count",\
"AFG On = LINK : [29] /Experiment/Variables/AFG On",\
"Quad RF Time (ms) = LINK : [65] /Equipment/TITAN_ACQ/ppg cycle/transition_QUAD2/time offset (ms)",\
"Centre Frequency (Hz) = LINK : [39] /Experiment/Variables/Center Frequency",\
"Frequency Deviation (Hz) = LINK : [42] /Experiment/Variables/Frequency Deviation",\
"Amplitude Override = BOOL : n",\
"MPETRFAmp (V) = LINK : [32] /Experiment/Variables/MPETRFAmp",\
"Dipole On = LINK : [41] /Experiment/Variables/SwiftDipole/AFG On",\
"Dipole RF Time (ms) = LINK : [64] /Equipment/TITAN_ACQ/ppg cycle/transition_DPL2/time offset (ms)",\
"Dipole Species = LINK : [42] /Experiment/Variables/SwiftDipole/Species",\
"Dipole Amplitude (V) = LINK : [51] /Experiment/Variables/SwiftDipole/RF Amplitude (V)",\
"Dipole FreqC (Hz) = LINK : [56] /Experiment/Variables/SwiftDipole/Center Frequency (Hz)",\
"Dipole FreqMod (Hz) = LINK : [60] /Experiment/Variables/SwiftDipole/Frequency Modulation (Hz)",\
"Feedbackfilename = LINK : [21] /Feedback/fbfilename",\
"PPG Start Delay (ms) = LINK : [66] /Equipment/TITAN_ACQ/ppg cycle/transition_PLT_Up/time offset (ms)",\
"Pedestals run = BOOL : n",\
"",\
NULL }

#ifndef EXCL_TRIGGER

#define TRIGGER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fempet",\
"Frontend file name = STRING : [256] fempet.c",\
"Status = STRING : [256] fempet@lxmpet.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#define TRIGGER_SETTINGS_DEFINED

typedef struct {
  struct {
    BOOL      keep_alive;
    BOOL      cycle_reset;
  } vt2;
  struct {
    INT       pretrigger;
  } vf48;
  struct {
    char      range[32];
  } softdac;
} TRIGGER_SETTINGS;

#define TRIGGER_SETTINGS_STR(_name) const char *_name[] = {\
"[vt2]",\
"keep alive = BOOL : n",\
"cycle reset = BOOL : y",\
"",\
"[vf48]",\
"pretrigger = INT : 0",\
"",\
"[softdac]",\
"range = STRING : [32] PM10V",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fempet",\
"Frontend file name = STRING : [256] fempet.c",\
"Status = STRING : [256] fempet@lxmpet.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_SLOWDAC

#define SLOWDAC_SETTINGS_DEFINED

typedef struct {
  char      names[8][32];
} SLOWDAC_SETTINGS;

#define SLOWDAC_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Names = STRING[8] :",\
"[32] Sdac channel 1",\
"[32] Sdac channel 2",\
"[32] Sdac channel 3",\
"[32] Sdac channel 4",\
"[32] Sdac channel 5",\
"[32] Sdac channel 6",\
"[32] Sdac channel 7",\
"[32] Sdac channel 8",\
"",\
NULL }

#define SLOWDAC_COMMON_SAVE_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} SLOWDAC_COMMON_SAVE;

#define SLOWDAC_COMMON_SAVE_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 13",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 511",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 10",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fesdac",\
"Frontend file name = STRING : [256] fesdac.c",\
"Status = STRING : [256] ",\
"Status color = STRING : [32] ",\
"Hidden = BOOL : n",\
"",\
NULL }

#define SLOWDAC_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} SLOWDAC_COMMON;

#define SLOWDAC_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 13",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 511",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fesdac",\
"Frontend file name = STRING : [256] fesdac.c",\
"Status = STRING : [256] fesdac@lxmpet.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_BEAMLINE

#define BEAMLINE_EVENT_DEFINED

typedef struct {
  float     demand[150];
  float     measured[150];
} BEAMLINE_EVENT;

#define BEAMLINE_EVENT_STR(_name) const char *_name[] = {\
"[.]",\
"Demand = FLOAT[150] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 3",\
"[20] -35",\
"[21] -200",\
"[22] -140",\
"[23] -18",\
"[24] -355",\
"[25] 1970",\
"[26] 1995",\
"[27] 1935",\
"[28] 1990",\
"[29] 1940",\
"[30] 0",\
"[31] 450",\
"[32] 0",\
"[33] 0",\
"[34] 2100",\
"[35] 250",\
"[36] 265",\
"[37] 268",\
"[38] 0.06",\
"[39] 2450",\
"[40] 250",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 3100",\
"[46] 100",\
"[47] 100",\
"[48] 204",\
"[49] 328",\
"[50] 246.98",\
"[51] 291",\
"[52] 10",\
"[53] 122",\
"[54] 221",\
"[55] 131",\
"[56] 17835",\
"[57] -5200",\
"[58] 3.076923e-10",\
"[59] 9.8",\
"[60] 652",\
"[61] 260",\
"[62] 10000",\
"[63] 100",\
"[64] 304.0055",\
"[65] 1000",\
"[66] 0",\
"[67] 267",\
"[68] 265",\
"[69] -2600",\
"[70] -2600",\
"[71] -3200",\
"[72] -900",\
"[73] -900",\
"[74] 2000",\
"[75] -2700",\
"[76] 400",\
"[77] 160",\
"[78] 150",\
"[79] 300",\
"[80] 2680",\
"[81] 0",\
"[82] 4.882812e-12",\
"[83] 500",\
"[84] 270",\
"[85] 270",\
"[86] 400",\
"[87] 400",\
"[88] 95",\
"[89] 140",\
"[90] 350",\
"[91] 630",\
"[92] 245",\
"[93] 575",\
"[94] 240",\
"[95] 20",\
"[96] 390",\
"[97] 870",\
"[98] 350",\
"[99] 1180",\
"[100] 450",\
"[101] 2",\
"[102] 6",\
"[103] 0",\
"[104] 380",\
"[105] 25",\
"[106] -38",\
"[107] 8",\
"[108] 530",\
"[109] 105",\
"[110] 65",\
"[111] 0",\
"[112] 0",\
"[113] 16",\
"[114] 175",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 2159",\
"[119] 260",\
"[120] 0",\
"[121] 0",\
"[122] 221",\
"[123] 20",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 5500",\
"[128] 0",\
"[129] 0",\
"[130] 0",\
"[131] 0",\
"[132] 0",\
"[133] 90",\
"[134] 0",\
"[135] 0",\
"[136] 0",\
"[137] 0",\
"[138] 0",\
"[139] 0",\
"[140] 0",\
"[141] 0",\
"[142] 0",\
"[143] 0",\
"[144] 0",\
"[145] 0",\
"[146] 0",\
"[147] 0",\
"[148] 0",\
"[149] 0",\
"Measured = FLOAT[150] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 19",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 5",\
"[34] 1700",\
"[35] 250",\
"[36] 250",\
"[37] 269",\
"[38] 0",\
"[39] 2000",\
"[40] 250",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 1900",\
"[46] 1200",\
"[47] 100",\
"[48] 0",\
"[49] 331",\
"[50] 385.615",\
"[51] 200",\
"[52] 4",\
"[53] 100",\
"[54] 201",\
"[55] 201",\
"[56] 17800",\
"[57] -2700",\
"[58] 1.868132e-08",\
"[59] 0",\
"[60] 0",\
"[61] 295",\
"[62] 15000",\
"[63] 0",\
"[64] 500",\
"[65] 600",\
"[66] 0",\
"[67] 258",\
"[68] 268",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 270",\
"[78] 270",\
"[79] 311",\
"[80] 1200",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 300",\
"[85] 300",\
"[86] 350",\
"[87] 325",\
"[88] 160",\
"[89] 150",\
"[90] 44",\
"[91] 44",\
"[92] 44",\
"[93] 44",\
"[94] 270",\
"[95] 0",\
"[96] 100",\
"[97] 40",\
"[98] 322",\
"[99] 2900",\
"[100] 1540",\
"[101] 0",\
"[102] -60",\
"[103] 0",\
"[104] 1340",\
"[105] 16",\
"[106] 0",\
"[107] -6",\
"[108] 1700",\
"[109] 136",\
"[110] 85",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 175",\
"[115] 1980",\
"[116] 0",\
"[117] 1760",\
"[118] 100",\
"[119] 40",\
"[120] 2800",\
"[121] 100",\
"[122] 100",\
"[123] 0",\
"[124] 231",\
"[125] 13500",\
"[126] 0",\
"[127] 3650",\
"[128] 2.33745e-07",\
"[129] 1200",\
"[130] 0",\
"[131] 0",\
"[132] 0",\
"[133] 0.02996406",\
"[134] 0",\
"[135] 0",\
"[136] 0",\
"[137] 0",\
"[138] 0",\
"[139] 0",\
"[140] 0",\
"[141] 0",\
"[142] 0",\
"[143] 0",\
"[144] 0",\
"[145] 0",\
"[146] 0",\
"[147] 0",\
"[148] 0",\
"[149] 0",\
"",\
NULL }

#define BEAMLINE_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      channel_name[150][32];
      BOOL      enabled;
    } beamline;
  } devices;
  char      names[150][32];
  float     update_threshold_measured[150];
} BEAMLINE_SETTINGS;

#define BEAMLINE_SETTINGS_STR(_name) const char *_name[] = {\
"[Devices/Beamline]",\
"Channel name = STRING[150] :",\
"[32] TITAN:VAR1",\
"[32] TITAN:VAR2",\
"[32] TITAN:VAR3",\
"[32] TITAN:VAR4",\
"[32] TITAN:VAR5",\
"[32] TITAN:VAR6",\
"[32] TITAN:VAR7",\
"[32] TITAN:VAR8",\
"[32] TITAN:VAR9",\
"[32] TITAN:VAR10",\
"[32] TITAN:VAR11",\
"[32] TITAN:VAR12",\
"[32] TITAN:VAR13",\
"[32] TITAN:VAR14",\
"[32] TITAN:VAR15",\
"[32] TITAN:VAR16",\
"[32] TITAN:VAR17",\
"[32] TITAN:VAR18",\
"[32] TITAN:VAR19",\
"[32] TITAN:VAR20",\
"[32] MPET:XZT1AND3:VOL",\
"[32] MPET:XZT2:VOL",\
"[32] MPET:XZT1AND3:VOL",\
"[32] MPET:XDC:VOL",\
"[32] MPET:PLTM:VOL",\
"[32] MPET:PLTP:VOL",\
"[32] MPET:LSYN:VOL",\
"[32] MPET:LSYP:VOL",\
"[32] MPET:LSXN:VOL",\
"[32] MPET:LSXP:VOL",\
"[32] MPET:NDC:VOL",\
"[32] MPET:NDT:VOL",\
"[32] MPET:CATHB:VOL",\
"[32] MPET:CATHI:VOL",\
"[32] MPETBL:EL2:VOL",\
"[32] MPETBL:CCB3:VOL",\
"[32] MPETBL:XCB3:VOL",\
"[32] MPETBL:YCB3:VOL",\
"[32] MPETBL:EL4:VOL",\
"[32] TSYBL:EL1:VOL",\
"[32] TSYBL:CCB2:VOL",\
"[32] TSYBL:XCB2:VOL",\
"[32] TSYBL:YCB2:VOL",\
"[32] TSYBL:XCB4:VOL",\
"[32] TSYBL:YCB4:VOL",\
"[32] TSYBL:EL3:VOL",\
"[32] TSYBL:EL4:VOL",\
"[32] TRFCBL:CCB0:VOL",\
"[32] TRFCBL:XCB0:VOL",\
"[32] TRFCBL:B1:POS:VOL",\
"[32] TRFCBL:B1:NEG:VOL",\
"[32] TRFCBL:Q1:C",\
"[32] TRFCBL:Q1:A",\
"[32] TRFCBL:XCB4:VOL",\
"[32] TRFCBL:YCB4B:VOL",\
"[32] TRFCBL:YCB4T:VOL",\
"[32] TRFC:PB5:VOL",\
"[32] TRFC:EL5:VOL",\
"[32] MPET:IG1:RDVAC",\
"[32] CCS2ISAC:BL2ACURRENT",\
"[32] TIS1:XCB0:VOL",\
"[32] TRFC:RFDC:VOL",\
"[32] TRFC:RFQFTRIG:FDRTDELAYSET",\
"[32] TRFC:RFQFTRIG:FCAMDELAYSET",\
"[32] ILE2T:XCB5:VOL",\
"[32] TRFC:RFG:FRFQSET",\
"[32] MPETBL:BNG:C",\
"[32] TSYBL:XCB9:VOL",\
"[32] TSYBL:YCB9:VOL",\
"[32] MPET:CB4TE:VOL",\
"[32] MPET:CB4TW:VOL",\
"[32] MPET:CB4B:VOL",\
"[32] MPET:EL4:VOL",\
"[32] MPET:EL1AND3:VOL",\
"[32] MPET:EL2:VOL",\
"[32] MPET:DALY:VOL",\
"[32] TSYBL:XCB8L:VOL",\
"[32] TSYBL:Q6:POS:VOL",\
"[32] TSYBL:Q5:POS:VOL",\
"[32] TSYBL:B8:POS:VOL",\
"[32] TSYBL:EL4:VOL",\
"[32] TIS1:ANODE:VOL",\
"[32] IIS:FC2:SCALECUR",\
"[32] MPET:NDC:VOL",\
"[32] TSYBL:XCB4SI:VOL",\
"[32] TSYBL:XCB4NI:VOL",\
"[32] TSYBL:XCB4SO:VOL",\
"[32] TSYBL:XCB4NO:VOL",\
"[32] TSYBL:YCB4:A",\
"[32] TSYBL:YCB4:B",\
"[32] EBITBL:Q2POS:A",\
"[32] EBITBL:Q2NEG:A",\
"[32] EBITBL:Q3POS:B",\
"[32] EBITBL:Q3NEG:B",\
"[32] TSYBL:Q5:C",\
"[32] TSYBL:Q5:A",\
"[32] EBIT:DT7E6PL:VOL",\
"[32] EBIT:DT76PH:DELTA",\
"[32] TSYBL:B1:POS:VOL",\
"[32] EBIT:SL1:VOL",\
"[32] EBIT:SL2INJ:F",\
"[32] EBIT:SL2INJ:Y",\
"[32] EBIT:SL2INJ:X",\
"[32] EBIT:SL2INJ:S",\
"[32] EBIT:SL2EXTR:F",\
"[32] EBIT:SL2EXTR:Y",\
"[32] EBIT:SL2EXTR:X",\
"[32] EBIT:SL2EXTR:S",\
"[32] EBIT:SL3:VOL",\
"[32] EBIT:SL4:VOL",\
"[32] EBIT:SIKL5:F",\
"[32] EBIT:SIKL5:Y",\
"[32] EBIT:SIKL5:X",\
"[32] EBIT:SIKL5:S",\
"[32] EBIT:SL6:VOL",\
"[32] EBIT:DT7E1:VOL",\
"[32] EBIT:DT7E2:VOL",\
"[32] EBIT:DT7E3:VOL",\
"[32] EBIT:DT7E4PL:VOL",\
"[32] EBIT:DT74PH:DELTA",\
"[32] EBIT:DT7E5PL:VOL",\
"[32] EBIT:DT75PH:DELTA",\
"[32] TSYBL:Q1:C",\
"[32] TSYBL:Q1:A",\
"[32] TRFC:RFPSD:VOL",\
"[32] TRFC:DCL3:VOL",\
"[32] TIS1:YCB0:VOL",\
"[32] TIS1:EL1:VOL",\
"[32] TIS1:IG1:RDVAC",\
"[32] TIS1:FIL0:CUR",\
"[32] Channel130",\
"[32] Channel131",\
"[32] Channel132",\
"[32] TSYBL:EMIT:MOTOR.VAL",\
"[32] Channel134",\
"[32] Channel135",\
"[32] Channel136",\
"[32] Channel137",\
"[32] Channel138",\
"[32] Channel139",\
"[32] Channel140",\
"[32] Channel141",\
"[32] Channel142",\
"[32] Channel143",\
"[32] Channel144",\
"[32] Channel145",\
"[32] Channel146",\
"[32] Channel147",\
"[32] NVM:EF12ON",\
"[32] NVM:EF13ON",\
"Enabled = BOOL : y",\
"",\
"[.]",\
"Names = STRING[150] :",\
"[32] Published%VAR1",\
"[32] Published%VAR2",\
"[32] Published%VAR3",\
"[32] Published%VAR4",\
"[32] Published%VAR5",\
"[32] Published%VAR6",\
"[32] Published%VAR7",\
"[32] Published%VAR8",\
"[32] Published%VAR9",\
"[32] Published%VAR10",\
"[32] Published%VAR11",\
"[32] Published%VAR12",\
"[32] Published%VAR13",\
"[32] Published%VAR14",\
"[32] Published%VAR15",\
"[32] Published%VAR16",\
"[32] Published%VAR17",\
"[32] Published%VAR18",\
"[32] Published%VAR19",\
"[32] Watchdog",\
"[32] MPET%Extr_einZel_3",\
"[32] MPET%Extr_einZel_2",\
"[32] MPET%Extr_einZel_1",\
"[32] MPET%Extr_cone",\
"[32] MPET%Extr_tube",\
"[32] MPET%Pulsed_Tube",\
"[32] MPET%Lorentz_YNeg",\
"[32] MPET%Lorentz_YPos",\
"[32] MPET%Lorentz_XNeg",\
"[32] MPET%Lorentz_XPos",\
"[32] MPET%eNtrance_Tube",\
"[32] MPET%eNtrance_Cone",\
"[32] MPET%CathodeBias",\
"[32] MPET%CathodeCurrent",\
"[32] MPETBL%BL EL2",\
"[32] MPETBL%BL CCB3",\
"[32] MPETBL%BL XCB3",\
"[32] MPETBL%BL YCB3",\
"[32] MPETBL%BL EL4",\
"[32] MPETBL%TSY EL1",\
"[32] MPETBL%TSY CCB2",\
"[32] MPETBL%TSY XCB2",\
"[32] MPETBL%TSY YCB2",\
"[32] MPETBL%TSY XCB4",\
"[32] MPETBL%TSY YCB4",\
"[32] MPETBL%TSY EL3",\
"[32] MPETBL%TSY EL4",\
"[32] MPETBL%RFCBL CCB0",\
"[32] MPETBL%RFCBL XCB0",\
"[32] MPETBL%RFCBL B1pos",\
"[32] MPETBL%RFCBL B1neg",\
"[32] TRFCBL%TRFCBL Q1 C",\
"[32] TRFCBL%TRFCBL Q1 A",\
"[32] MPETBL%RFCBL XCB4",\
"[32] MPETBL%RFCBL YCB4B",\
"[32] MPETBL%RFCBL YCB4T",\
"[32] MPETBL%RFC PB5",\
"[32] MPETBL%RFC EL5",\
"[32] MPETVAC%MPETIG",\
"[32] General%ProtonCurrent",\
"[32] TISBL%XCB0",\
"[32] RFQ%RFQampl",\
"[32] RFQ%PB5delay",\
"[32] RFQ%RFQCamDelay (ns)",\
"[32] TISBL%XCB1",\
"[32] RFQ%RFQfreq",\
"[32] MPETBL%BNG",\
"[32] TSYBL%TSYBL:XCB9 (V)",\
"[32] TSYBL%TSYBL:YCB9 (V)",\
"[32] MPET%MPET:CB4TE (V)",\
"[32] MPET%MPET:CB4TW (V)",\
"[32] MPET%MPET:CB4B (V)",\
"[32] MPET%MPET:EL4 (V)",\
"[32] MPET%MPET:EL1AND3 (V)",\
"[32] MPET%MPET:EL2 (V)",\
"[32] MPET%MPET:DALY (V)",\
"[32] TSYBL%TSYBL:XCB8L",\
"[32] Default%CH 77",\
"[32] Default%CH 78",\
"[32] TSYBL%TSYBL:B8 (V)",\
"[32] TSYBL%TSYBL:EL4 (V)",\
"[32] TIS1%TIS1:ANODE (V)",\
"[32] Gen%IMSBeamDumpCur",\
"[32] MPET:NDC",\
"[32] TSYBL%TSYBL XCB4SI",\
"[32] TSYBL%TSYBL XCB4NI",\
"[32] TSYBL%TSYBL XCB4SO",\
"[32] TSYBL%TSYBL XCB4NO",\
"[32] TSYBL%TSYBL YCB4HI",\
"[32] TSYBL%TSYBL YCB4LO",\
"[32] EBITBL%EBITBL Q2POSHI",\
"[32] EBITBL%EBITBL Q2NEGHI",\
"[32] EBITBL%EBITBL Q3POSLO",\
"[32] EBITBL%EBITBL Q3NEGLO",\
"[32] TSYBL%TSYBL Q5:C",\
"[32] TSYBL%TSYBL Q5:A",\
"[32] EBIT%EBIT:DT7E6PL:VOL",\
"[32] EBIT%EBIT:DT76PH:DELTA",\
"[32] TSYBL%TSYBL:B1",\
"[32] EBIT%EBIT:SL1:VOL",\
"[32] EBIT%EBIT:SL2INJ:F",\
"[32] EBIT%EBIT:SL2INJ:Y",\
"[32] EBIT%EBIT:SL2INJ:X",\
"[32] EBIT%EBIT:SL2INJ:S",\
"[32] EBIT%EBIT:SL2EXTR:F",\
"[32] EBIT%EBIT:SL2EXTR:Y",\
"[32] EBIT%EBIT:SL2EXTR:X",\
"[32] EBIT%EBIT:SL2EXTR:S",\
"[32] EBIT%EBIT:SL3:VOL",\
"[32] EBIT%EBIT:SL4:VOL",\
"[32] EBIT%EBIT:SIKL5:F",\
"[32] EBIT%EBIT:SIKL5:Y",\
"[32] EBIT%EBIT:SIKL5:X",\
"[32] EBIT%EBIT:SIKL5:S",\
"[32] EBIT%EBIT:SL6:VOL",\
"[32] EBIT%EBIT:DT7E1:VOL",\
"[32] EBIT%EBIT:DT7E2:VOL",\
"[32] EBIT%EBIT:DT7E3:VOL",\
"[32] EBIT%EBIT:DT7E4PL:VOL",\
"[32] EBIT%EBIT:DT74PH:DELTA",\
"[32] EBIT%EBIT:DT7E5PL:VOL",\
"[32] EBIT%EBIT:DT75PH:DELTA",\
"[32] TSYBL%TSYBL:Q1:C",\
"[32] TSYBL%TSYBL:Q1:A",\
"[32] RFQ%RFQpsd",\
"[32] TRFC%TRFC:DCL3:VOL",\
"[32] TIS%TIS1:YCB0:VOL",\
"[32] TIS%TIS1:EL1:VOL",\
"[32] TIS1VAC%TIS:IG",\
"[32] TIS1%TIS1:FIL (mA)",\
"[32] TRFC%TRFC:Q1:VOL",\
"[32] TRFC%TRFC:Q2:POS:VOL",\
"[32] EBIT%EBIT:DT76PH:DELTA",\
"[32] TSYBL%EMIT:MOTOR.VAL",\
"[32] Default%CH 134",\
"[32] Default%CH 135",\
"[32] Default%CH 136",\
"[32] Default%CH 137",\
"[32] Default%CH 138",\
"[32] Default%CH 139",\
"[32] Default%CH 140",\
"[32] Default%CH 141",\
"[32] Default%CH 142",\
"[32] Default%CH 143",\
"[32] Default%CH 144",\
"[32] Default%CH 145",\
"[32] Default%CH 146",\
"[32] Default%CH 147",\
"[32] Default%CH 148",\
"[32] Default%CH 149",\
"Update Threshold Measured = FLOAT[150] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"[16] 1",\
"[17] 1",\
"[18] 1",\
"[19] 1",\
"[20] 1",\
"[21] 1",\
"[22] 1",\
"[23] 1",\
"[24] 1",\
"[25] 1",\
"[26] 1",\
"[27] 1",\
"[28] 1",\
"[29] 1",\
"[30] 1",\
"[31] 1",\
"[32] 1",\
"[33] 1",\
"[34] 1",\
"[35] 1",\
"[36] 1",\
"[37] 1",\
"[38] 1",\
"[39] 1",\
"[40] 1",\
"[41] 1",\
"[42] 1",\
"[43] 1",\
"[44] 1",\
"[45] 1",\
"[46] 1",\
"[47] 1",\
"[48] 1",\
"[49] 1",\
"[50] 1",\
"[51] 1",\
"[52] 1",\
"[53] 1",\
"[54] 1",\
"[55] 1",\
"[56] 1",\
"[57] 1",\
"[58] 1",\
"[59] 1",\
"[60] 1",\
"[61] 1",\
"[62] 1",\
"[63] 1",\
"[64] 1",\
"[65] 1",\
"[66] 1",\
"[67] 1",\
"[68] 1",\
"[69] 1",\
"[70] 1",\
"[71] 1",\
"[72] 1",\
"[73] 1",\
"[74] 1",\
"[75] 1",\
"[76] 1",\
"[77] 1",\
"[78] 1",\
"[79] 1",\
"[80] 1",\
"[81] 1",\
"[82] 1",\
"[83] 1",\
"[84] 1",\
"[85] 1",\
"[86] 1",\
"[87] 1",\
"[88] 1",\
"[89] 1",\
"[90] 1",\
"[91] 1",\
"[92] 1",\
"[93] 1",\
"[94] 1",\
"[95] 1",\
"[96] 1",\
"[97] 1",\
"[98] 1",\
"[99] 1",\
"[100] 1",\
"[101] 1",\
"[102] 1",\
"[103] 1",\
"[104] 1",\
"[105] 1",\
"[106] 1",\
"[107] 1",\
"[108] 1",\
"[109] 1",\
"[110] 1",\
"[111] 1",\
"[112] 1",\
"[113] 1",\
"[114] 1",\
"[115] 1",\
"[116] 1",\
"[117] 1",\
"[118] 1",\
"[119] 1",\
"[120] 1",\
"[121] 1",\
"[122] 1",\
"[123] 1",\
"[124] 1",\
"[125] 1",\
"[126] 1",\
"[127] 1",\
"[128] 1",\
"[129] 1",\
"[130] 1",\
"[131] 1",\
"[132] 1",\
"[133] 1",\
"[134] 1",\
"[135] 1",\
"[136] 1",\
"[137] 1",\
"[138] 1",\
"[139] 1",\
"[140] 1",\
"[141] 1",\
"[142] 1",\
"[143] 1",\
"[144] 1",\
"[145] 1",\
"[146] 1",\
"[147] 1",\
"[148] 1",\
"[149] 1",\
"",\
NULL }

#define BEAMLINE_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} BEAMLINE_COMMON;

#define BEAMLINE_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 11",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] FIXED",\
"Enabled = BOOL : y",\
"Read on = INT : 121",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] localhost",\
"Frontend name = STRING : [32] scEpics",\
"Frontend file name = STRING : [256] scepics.c",\
"Status = STRING : [256] Ok",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_SCAN

#define SCAN_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} SCAN_COMMON;

#define SCAN_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 5",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 250",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fempet",\
"Frontend file name = STRING : [256] fempet.c",\
"Status = STRING : [256] fempet@lxmpet.triumf.ca",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"",\
NULL }

#endif

#ifndef EXCL_TITAN_ACQ

#define TITAN_ACQ_PPG_CYCLE_DEFINED

typedef struct {
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_mbr;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_plt_up;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
  } std_resetscalarcounter;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_ramp;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_ebittrig;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_trap_injection;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_frequency_list_step;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[16];
  } std_mbr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_isacgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tsybl;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tofgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_plt_down;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      time_reference[20];
    char      observation[5][128];
  } evset_trap_capture;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dpl1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dpl2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad3;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad4;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq2;
  struct {
    char      ppg_signal_name[16];
    double    time_offset__ms_;
    char      time_reference[20];
  } trans_tdcgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[47];
  } end_ramp;
  struct {
    double    time_offset__ms_;
  } end_scan;
} TITAN_ACQ_PPG_CYCLE;

#define TITAN_ACQ_PPG_CYCLE_STR(_name) const char *_name[] = {\
"[transition_MBR]",\
"ppg signal name = STRING : [15] CH11",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[transition_PLT_Up]",\
"time offset (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [15] CH12",\
"",\
"[std_ResetScalarCounter]",\
"time offset (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 25000000",\
"",\
"[begin_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"loop count = INT : 30",\
"",\
"[pulse_EBITtrig]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[evset_Trap_Injection]",\
"time offset (ms) = DOUBLE : 0.001",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.2) (1,-1.0000) (2,-0.7625) (3,1.0000) (4,1.0) (5,-1.0) (7,0.0)",\
"",\
"[pulse_Frequency_List_Step]",\
"time offset (ms) = DOUBLE : 0.005",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH10",\
"",\
"[std_MBR]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse_ISACGATE]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse_TSYBL]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse_TOFGate]",\
"time offset (ms) = DOUBLE : 0.0248",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse_PLT_Down]",\
"time offset (ms) = DOUBLE : 0.08699999999999999",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[evset_Trap_Capture]",\
"time offset (ms) = DOUBLE : 0.0905",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.209) (1,0.99) (2,-0.7625) (3,0.99) (4,1.3844) (5,1.3844) (7,-3)",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"Observation = STRING[5] :",\
"[128] Values for Caps (channels 1 and 3) and Ring (ch.2) are",\
"[128] set this way to account for offsets in the amplifiers.",\
"[128] Amplification factor is 20.00 for all.",\
"[128] Offset for Ring is -0.5V, for both Caps is +0.2V.",\
"[128] See wiki entry of 02 Nov 2016 of MPET e-log.",\
"",\
"[transition_DPL1]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[transition_DPL2]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[transition_DQ1]",\
"ppg signal name = STRING : [15] CH7",\
"time offset (ms) = DOUBLE : 0.01",\
"",\
"[transition_QUAD1]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[transition_QUAD2]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 20",\
"",\
"[transition_QUAD3]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 4",\
"",\
"[transition_QUAD4]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 2",\
"",\
"[transition_DQ2]",\
"ppg signal name = STRING : [15] CH7",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[trans_TDCGate]",\
"ppg signal name = STRING : [16] CH13",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TTRANSITION_DQ2",\
"",\
"[end_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"time_reference = STRING : [47] _TEND_pul_TDCGate (this is not doing anything)",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 0.001",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_DW_DEFINED

typedef struct {
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_mbr;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_plt_up;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
  } std_resetscalarcounter;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_ramp;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_ebittrig;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_mrtoftrig;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      backup[142];
  } evset_trap_injection;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_frequency_list_step;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_dipolefreq_step1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[16];
    char      comment[35];
  } std_mbr;
  struct {
    char      ppg_signal_name[32];
    char      time_reference[32];
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      backup[159];
  } pul_tdcgate_mbr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_isacgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tsybl;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tofgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_plt_down;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      time_reference[20];
    char      observation[5][128];
  } evset_trap_capture;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dpl1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    char      backup[44];
  } pulse_dipolefreq_step2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
    char      time_reference[32];
  } transition_dpl2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad3;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad4;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq2;
  struct {
    INT       awg_unit;
    char      set_values__volts_[128];
    double    time_offset__ms_;
    char      backup[68];
  } evset_trap_ejection;
  struct {
    char      ppg_signal_name[16];
    char      time_reference[20];
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      backup[68];
  } pul_tdcgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[47];
  } end_ramp;
  struct {
    double    time_offset__ms_;
  } end_scan;
  struct {
    char      ppg_signal_name[16];
    double    time_offset__ms_;
    char      time_reference[20];
  } trans_tdcgate;
  struct {
    INT       dummy;
  } skip;
  struct {
    double    time_offset__ms_;
    INT       _loop_count;
  } begin_slow_extraction;
  struct {
    INT       _awg_unit;
    char      start_value__volts_[32];
    char      end_value__volts_[32];
    double    time_offset__ms_;
  } evstep_slow_trap_ejection;
  struct {
    double    time_offset__ms_;
  } end_slow_extraction;
  struct {
    char      ppg_signal_name[16];
    double    time_offset__ms_;
  } trans_tdcgate2;
  struct {
    INT       awg_unit;
    char      set_values__volts_[75];
    double    time_offset__ms_;
  } evset_trap_ejection2;
  struct {
    INT       dummy;
  } unskip;
} TITAN_ACQ_PPG_CYCLE_DW;

#define TITAN_ACQ_PPG_CYCLE_DW_STR(_name) const char *_name[] = {\
"[transition_MBR]",\
"ppg signal name = STRING : [15] CH11",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[transition_PLT_Up]",\
"time offset (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [15] CH12",\
"",\
"[std_ResetScalarCounter]",\
"time offset (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 2500",\
"",\
"[begin_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"loop count = INT : 3",\
"",\
"[pulse_EBITtrig]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse_MRToFtrig]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH15",\
"",\
"[evset_Trap_Injection]",\
"time offset (ms) = DOUBLE : 0.001",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.2) (1,-1.0000) (2,-0.7625) (3,1.0000) (4,1.0) (5,-1.0) (7,0.0)",\
"Backup = STRING : [142] (0,-1.02) (1,-1.2000) (2,-0.7625) (3, 1.2) (4, 1.2) (5,-1.2) (7, 0.0) ",\
"",\
"[pulse_Frequency_List_Step]",\
"time offset (ms) = DOUBLE : 0.005",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH10",\
"",\
"[pulse_DipoleFreq_Step1]",\
"time offset (ms) = DOUBLE : 0.005",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH14",\
"",\
"[std_MBR]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"ppg signal name = STRING : [16] CH11",\
"Comment = STRING : [35] 0.1213 is an offset after breeding",\
"",\
"[pul_TDCGate_MBR]",\
"ppg signal name = STRING : [32] CH15",\
"time reference = STRING : [32] _TSTART_std_MBR",\
"time offset (ms) = DOUBLE : 0.0001",\
"pulse width (ms) = DOUBLE : 0.15",\
"Backup = STRING : [159] CH13 is the active TDC one, when not using put on CH15, a spare one. pul_TDCGate is the one data analysis software will look at, so width needs to be the same",\
"",\
"[pulse_ISACGATE]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse_TSYBL]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse_TOFGate]",\
"time offset (ms) = DOUBLE : 0.0248",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse_PLT_Down]",\
"time offset (ms) = DOUBLE : 0.08699999999999999",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[evset_Trap_Capture]",\
"time offset (ms) = DOUBLE : 0.0905",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.209) (1,0.99) (2,-0.7625) (3,0.99) (4,1.3844) (5,1.3844) (7,-3)",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"Observation = STRING[5] :",\
"[128] Values for Caps (channels 1 and 3) and Ring (ch.2) are",\
"[128] set this way to account for offsets in the amplifiers.",\
"[128] Amplification factor is 20.00 for all.",\
"[128] Offset for Ring is -0.5V, for both Caps is +0.2V.",\
"[128] See wiki entry of 02 Nov 2016 of MPET e-log.",\
"",\
"[transition_DPL1]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[pulse_DipoleFreq_Step2]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TTRANSITION_DPL1",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH14",\
"Backup = STRING : [44] signal name should be CH14, CH16 is a spare",\
"",\
"[transition_DPL2]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [32] _TTRANSITION_DPL1",\
"",\
"[transition_DQ1]",\
"ppg signal name = STRING : [15] CH7",\
"time offset (ms) = DOUBLE : 0.01",\
"",\
"[transition_QUAD1]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[transition_QUAD2]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 20",\
"",\
"[transition_QUAD3]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 300",\
"",\
"[transition_QUAD4]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 100",\
"",\
"[transition_DQ2]",\
"ppg signal name = STRING : [15] CH7",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[evset_Trap_Ejection]",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.209) (1,1.0000) (2,-0.7625) (3,1.0000) (4,1.3844) (5,1.3844) (7,-3)",\
"time offset (ms) = DOUBLE : 0.01",\
"Backup = STRING : [68] (0,-0.20) (1,1.0) (2,-0.775) (3,0.000) (4,-0.0000) (5,1.0) (7,-3.0)",\
"",\
"[pul_TDCGate]",\
"ppg signal name = STRING : [16] CH13",\
"time reference = STRING : [20] _TTRANSITION_DQ2",\
"time offset (ms) = DOUBLE : 0.1",\
"pulse width (ms) = DOUBLE : 5.12",\
"Backup = STRING : [68] CH13 is the active TDC one, when not using put on CH15, a spare one",\
"",\
"[end_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"time_reference = STRING : [47] _TEND_pul_TDCGate (this is not doing anything)",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 0.001",\
"",\
"[trans_TDCGate]",\
"ppg signal name = STRING : [16] CH13",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TTRANSITION_DQ2",\
"",\
"[skip]",\
"dummy = INT : 0",\
"",\
"[begin_slow_extraction]",\
"time offset (ms) = DOUBLE : 0",\
" loop count = INT : 5",\
"",\
"[evstep_Slow_Trap_Ejection]",\
" AWG unit = INT : 0",\
"start value (Volts) = STRING : [32] (2,-0.7625)(3,0.99) (4,1.2)",\
"end value (Volts) = STRING : [32] (2,0.0)(3,-1) (4,-2.0)",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[end_slow_extraction]",\
"time offset (ms) = DOUBLE : 0",\
"",\
"[trans_TDCGate2]",\
"ppg signal name = STRING : [16] CH13",\
"time offset (ms) = DOUBLE : 0",\
"",\
"[evset_Trap_Ejection2]",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [75] (0,-0.209) (1,0.99) (2,0) (3,0) (4,-0.6) (5,1.0) (7,-3)",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[unskip]",\
"dummy = INT : 0",\
"",\
NULL }

#define TITAN_ACQ_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} TITAN_ACQ_COMMON;

#define TITAN_ACQ_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 10",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fempet",\
"Frontend file name = STRING : [256] fempet.c",\
"Status = STRING : [256] fempet@lxmpet.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_SHORTNAMES_DEFINED

typedef struct {
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } trans_mbr;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } trans_plt_up;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
  } std_resetsc;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_ramp;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_ebittrig;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evsettrapinj;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulsefrqls;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[16];
  } std_mbr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_isacgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tsybl;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tofgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_plt_down;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      time_reference[20];
    char      observation[5][128];
  } evset_trap_capture;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } trans_dpl1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } trans_dpl2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } trans_dq1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } tra_quad1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } trans_quad2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } trans_dq2;
  struct {
    char      ppg_signal_name[16];
    double    time_offset__ms_;
    char      time_reference[20];
  } trans_tdcgate;
  struct {
    double    time_offset__ms_;
    INT       _loop_count;
  } begin_slow_extraction;
  struct {
    INT       _awg_unit;
    char      start_value__volts_[32];
    char      end_value__volts_[32];
    double    time_offset__ms_;
  } evstep_slow_trap_ejection;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
    char      time_reference[24];
  } trans_mbx;
  struct {
    double    time_offset__ms_;
  } end_slow_extraction;
  struct {
    char      ppg_signal_name[16];
    double    time_offset__ms_;
  } trans_tdcgate2;
  struct {
    INT       dummy;
  } skip;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      backup[68];
  } evset_trap_ejection;
  struct {
    INT       awg_unit;
    char      set_values__volts_[75];
    double    time_offset__ms_;
  } evset_trap_ejection2;
  struct {
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    double    time_offset__ms_;
    char      time_reference[20];
  } pul_tdcgate;
  struct {
    INT       dummy;
  } unskip;
  struct {
    double    time_offset__ms_;
    char      time_reference[47];
  } end_ramp;
  struct {
    double    time_offset__ms_;
  } end_scan;
} TITAN_ACQ_PPG_CYCLE_SHORTNAMES;

#define TITAN_ACQ_PPG_CYCLE_SHORTNAMES_STR(_name) const char *_name[] = {\
"[trans_MBR]",\
"ppg signal name = STRING : [15] awgam",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[trans_PLT_Up]",\
"time offset (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [15] HV",\
"",\
"[std_ResetSC]",\
"time offset (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] RFTRIG2",\
"",\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 2500",\
"",\
"[begin_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"loop count = INT : 41",\
"",\
"[pulse_EBITtrig]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH17",\
"",\
"[evsetTrapInj]",\
"time offset (ms) = DOUBLE : 0.001",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.2) (1,-1.0000) (2,-0.7625) (3,1.0000) (4,1.0) (5,-1.0) (7,0.0)",\
"",\
"[pulseFrqLS]",\
"time offset (ms) = DOUBLE : 0.005",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] RFGATE4",\
"",\
"[std_MBR]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"ppg signal name = STRING : [16] awgam",\
"",\
"[pulse_ISACGATE]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] RFGATE3",\
"",\
"[pulse_TSYBL]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH18",\
"",\
"[pulse_TOFGate]",\
"time offset (ms) = DOUBLE : 0.0248",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH20",\
"",\
"[pulse_PLT_Down]",\
"time offset (ms) = DOUBLE : 0.08699999999999999",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] HV",\
"",\
"[evset_Trap_Capture]",\
"time offset (ms) = DOUBLE : 0.0905",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.209) (1,0.99) (2,-0.7625) (3,0.99) (4,1.3844) (5,1.3844) (7,-3)",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"Observation = STRING[5] :",\
"[128] Values for Caps (channels 1 and 3) and Ring (ch.2) are ",\
"[128] set this way to account for offsets in the amplifiers.",\
"[128] Amplification factor is 20.00 for all.",\
"[128] Offset for Ring is -0.5V, for both Caps is +0.2V.",\
"[128] See wiki entry of 02 Nov 2016 of MPET e-log.",\
"",\
"[trans_DPL1]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[trans_DPL2]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[trans_DQ1]",\
"ppg signal name = STRING : [15] RFTRIG3",\
"time offset (ms) = DOUBLE : 0.01",\
"",\
"[tra_QUAD1]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[trans_QUAD2]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 100",\
"",\
"[trans_DQ2]",\
"ppg signal name = STRING : [15] RFTRIG3",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[trans_TDCGate]",\
"ppg signal name = STRING : [16] TDCGATE",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TTRANS_DQ2",\
"",\
"[begin_slow_extraction]",\
"time offset (ms) = DOUBLE : 0",\
" loop count = INT : 5",\
"",\
"[evstep_Slow_Trap_Ejection]",\
" AWG unit = INT : 0",\
"start value (Volts) = STRING : [32] (2,-0.7625)(3,0.99) (4,1.2)",\
"end value (Volts) = STRING : [32] (2,0.0)(3,-1) (4,-2.0)",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[trans_mbx]",\
"ppg signal name = STRING : [15] awgam",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [24] _TBEGslow_extraction",\
"",\
"[end_slow_extraction]",\
"time offset (ms) = DOUBLE : 0",\
"",\
"[trans_TDCGate2]",\
"ppg signal name = STRING : [16] TDCGATE",\
"time offset (ms) = DOUBLE : 0",\
"",\
"[skip]",\
"dummy = INT : 0",\
"",\
"[evset_Trap_Ejection]",\
"time offset (ms) = DOUBLE : 0.01",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.209) (1,1.0000) (2,-0.7625) (3,1.0000) (4,1.3844) (5,1.3844) (7,-3)",\
"Backup = STRING : [68] (0,-0.20) (1,1.0) (2,-0.775) (3,0.000) (4,-0.0000) (5,1.0) (7,-3.0)",\
"",\
"[evset_Trap_Ejection2]",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [75] (0,-0.209) (1,0.99) (2,0) (3,0) (4,-0.6) (5,1.0) (7,-3)",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[pul_TDCGate]",\
"pulse width (ms) = DOUBLE : 5.12",\
"ppg signal name = STRING : [16] TDCGATE",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TTRANSITION_DQ2",\
"",\
"[unskip]",\
"dummy = INT : 0",\
"",\
"[end_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"time_reference = STRING : [47] _TEND_pul_TDCGate (this is not doing anything)",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 0.001",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_ORIG_DEFINED

typedef struct {
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_mbr;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_plt_up;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
  } std_resetscalarcounter;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_ramp;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_ebittrig;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_trap_injection;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_frequency_list_step;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[16];
  } std_mbr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_isacgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tsybl;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tofgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_plt_down;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      time_reference[20];
    char      observation[5][128];
  } evset_trap_capture;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dpl1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dpl2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq2;
  struct {
    char      ppg_signal_name[16];
    double    time_offset__ms_;
    char      time_reference[20];
  } trans_tdcgate;
  struct {
    INT       dummy;
  } skip;
  struct {
    double    time_offset__ms_;
    INT       _loop_count;
  } begin_slow_extraction;
  struct {
    INT       _awg_unit;
    char      start_value__volts_[32];
    char      end_value__volts_[32];
    double    time_offset__ms_;
  } evstep_slow_trap_ejection;
  struct {
    double    time_offset__ms_;
  } end_slow_extraction;
  struct {
    char      ppg_signal_name[16];
    double    time_offset__ms_;
  } trans_tdcgate2;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      backup[68];
  } evset_trap_ejection;
  struct {
    INT       awg_unit;
    char      set_values__volts_[75];
    double    time_offset__ms_;
  } evset_trap_ejection2;
  struct {
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    char      time_reference[20];
    double    time_offset__ms_;
  } pul_tdcgate;
  struct {
    INT       dummy;
  } unskip;
  struct {
    double    time_offset__ms_;
    char      time_reference[47];
  } end_ramp;
  struct {
    double    time_offset__ms_;
  } end_scan;
} TITAN_ACQ_PPG_CYCLE_ORIG;

#define TITAN_ACQ_PPG_CYCLE_ORIG_STR(_name) const char *_name[] = {\
"[transition_MBR]",\
"ppg signal name = STRING : [15] awgam",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[transition_PLT_Up]",\
"time offset (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [15] HV",\
"",\
"[std_ResetScalarCounter]",\
"time offset (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] RFTRIG2",\
"",\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 2500",\
"",\
"[begin_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"loop count = INT : 3",\
"",\
"[pulse_EBITtrig]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH17",\
"",\
"[evset_Trap_Injection]",\
"time offset (ms) = DOUBLE : 0.001",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.2) (1,-1.0000) (2,-0.7625) (3,1.0000) (4,1.0) (5,-1.0) (7,0.0)",\
"",\
"[pulse_Frequency_List_Step]",\
"time offset (ms) = DOUBLE : 0.005",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] RFGATE4",\
"",\
"[std_MBR]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"ppg signal name = STRING : [16] awgam",\
"",\
"[pulse_ISACGATE]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] RFGATE3",\
"",\
"[pulse_TSYBL]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH18",\
"",\
"[pulse_TOFGate]",\
"time offset (ms) = DOUBLE : 0.0248",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH20",\
"",\
"[pulse_PLT_Down]",\
"time offset (ms) = DOUBLE : 0.08699999999999999",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] HV",\
"",\
"[evset_Trap_Capture]",\
"time offset (ms) = DOUBLE : 0.0905",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.209) (1,0.99) (2,-0.7625) (3,0.99) (4,1.3844) (5,1.3844) (7,-3)",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"Observation = STRING[5] :",\
"[128] Values for Caps (channels 1 and 3) and Ring (ch.2) are",\
"[128] set this way to account for offsets in the amplifiers.",\
"[128] Amplification factor is 20.00 for all.",\
"[128] Offset for Ring is -0.5V, for both Caps is +0.2V.",\
"[128] See wiki entry of 02 Nov 2016 of MPET e-log.",\
"",\
"[transition_DPL1]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[transition_DPL2]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[transition_DQ1]",\
"ppg signal name = STRING : [15] RFTRIG3",\
"time offset (ms) = DOUBLE : 0.01",\
"",\
"[transition_QUAD1]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[transition_QUAD2]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 10",\
"",\
"[transition_DQ2]",\
"ppg signal name = STRING : [15] RFTRIG3",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[trans_TDCGate]",\
"ppg signal name = STRING : [16] TDCGATE",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TTRANSITION_DQ2",\
"",\
"[skip]",\
"dummy = INT : 0",\
"",\
"[begin_slow_extraction]",\
"time offset (ms) = DOUBLE : 0",\
" loop count = INT : 5",\
"",\
"[evstep_Slow_Trap_Ejection]",\
" AWG unit = INT : 0",\
"start value (Volts) = STRING : [32] (2,-0.7625)(3,0.99) (4,1.2)",\
"end value (Volts) = STRING : [32] (2,0.0)(3,-1) (4,-2.0)",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[end_slow_extraction]",\
"time offset (ms) = DOUBLE : 0",\
"",\
"[trans_TDCGate2]",\
"ppg signal name = STRING : [16] TDCGATE",\
"time offset (ms) = DOUBLE : 0",\
"",\
"[evset_Trap_Ejection]",\
"time offset (ms) = DOUBLE : 0.01",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.209) (1,1.0000) (2,-0.7625) (3,1.0000) (4,1.3844) (5,1.3844) (7,-3)",\
"Backup = STRING : [68] (0,-0.20) (1,1.0) (2,-0.775) (3,0.000) (4,-0.0000) (5,1.0) (7,-3.0)",\
"",\
"[evset_Trap_Ejection2]",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [75] (0,-0.209) (1,0.99) (2,0) (3,0) (4,-0.6) (5,1.0) (7,-3)",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[pul_TDCGate]",\
"pulse width (ms) = DOUBLE : 5.12",\
"ppg signal name = STRING : [16] TDCGATE",\
"time reference = STRING : [20] _TTRANSITION_DQ2",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[unskip]",\
"dummy = INT : 0",\
"",\
"[end_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"time_reference = STRING : [47] _TEND_pul_TDCGate (this is not doing anything)",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 0.001",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_TRY_DEFINED

typedef struct {
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_mbr;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_plt_up;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
  } std_resetscalarcounter;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_ramp;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_ebittrig;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    float     set_values__volts_[8];
    char      old_set_values[128];
  } evset_trap_injection;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_frequency_list_step;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[16];
  } std_mbr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_isacgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tsybl;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tofgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_plt_down;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    float     set_values__volts_[8];
    char      time_reference[20];
    char      observation[5][128];
    char      old_set_values[128];
  } evset_trap_capture;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dpl1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dpl2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq2;
  struct {
    char      ppg_signal_name[16];
    double    time_offset__ms_;
    char      time_reference[20];
  } trans_tdcgate;
  struct {
    double    time_offset__ms_;
    INT       _loop_count;
  } begin_slow_extraction;
  struct {
    INT       _awg_unit;
    char      start_value__volts_[32];
    char      end_value__volts_[32];
    double    time_offset__ms_;
  } evstep_slow_trap_ejection;
  struct {
    double    time_offset__ms_;
  } end_slow_extraction;
  struct {
    char      ppg_signal_name[16];
    double    time_offset__ms_;
  } trans_tdcgate2;
  struct {
    INT       dummy;
  } skip;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      backup[68];
  } evset_trap_ejection;
  struct {
    INT       awg_unit;
    char      set_values__volts_[75];
    double    time_offset__ms_;
  } evset_trap_ejection2;
  struct {
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    double    time_offset__ms_;
    char      time_reference[20];
  } pul_tdcgate;
  struct {
    INT       dummy;
  } unskip;
  struct {
    double    time_offset__ms_;
    char      time_reference[47];
  } end_ramp;
  struct {
    double    time_offset__ms_;
  } end_scan;
} TITAN_ACQ_PPG_CYCLE_TRY;

#define TITAN_ACQ_PPG_CYCLE_TRY_STR(_name) const char *_name[] = {\
"[transition_MBR]",\
"ppg signal name = STRING : [15] awgam",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[transition_PLT_Up]",\
"time offset (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [15] HV",\
"",\
"[std_ResetScalarCounter]",\
"time offset (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] RFTRIG2",\
"",\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 2500",\
"",\
"[begin_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"loop count = INT : 41",\
"",\
"[pulse_EBITtrig]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH17",\
"",\
"[evset_Trap_Injection]",\
"time offset (ms) = DOUBLE : 0.001",\
"AWG unit = INT : 0",\
"Set Values (Volts) = FLOAT[8] :",\
"[0] -0.2",\
"[1] -1",\
"[2] -0.7625",\
"[3] 1",\
"[4] 1",\
"[5] -1",\
"[6] 0",\
"[7] 0",\
"OLD Set Values = STRING : [128] (0,-0.2) (1,-1.0000) (2,-0.7625) (3,1.0000) (4,1.0) (5,-1.0) (7,0.0)",\
"",\
"[pulse_Frequency_List_Step]",\
"time offset (ms) = DOUBLE : 0.005",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] RFGATE4",\
"",\
"[std_MBR]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"ppg signal name = STRING : [16] awgam",\
"",\
"[pulse_ISACGATE]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] RFGATE3",\
"",\
"[pulse_TSYBL]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH18",\
"",\
"[pulse_TOFGate]",\
"time offset (ms) = DOUBLE : 0.0248",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH20",\
"",\
"[pulse_PLT_Down]",\
"time offset (ms) = DOUBLE : 0.08699999999999999",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] HV",\
"",\
"[evset_Trap_Capture]",\
"time offset (ms) = DOUBLE : 0.0905",\
"AWG unit = INT : 0",\
"Set Values (Volts) = FLOAT[8] :",\
"[0] -0.209",\
"[1] 0.99",\
"[2] -0.7625",\
"[3] 0.99",\
"[4] 1.3844",\
"[5] 1.3844",\
"[6] 0",\
"[7] -3",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"Observation = STRING[5] :",\
"[128] Values for Caps (channels 1 and 3) and Ring (ch.2) are",\
"[128] set this way to account for offsets in the amplifiers.",\
"[128] Amplification factor is 20.00 for all.",\
"[128] Offset for Ring is -0.5V, for both Caps is +0.2V.",\
"[128] See wiki entry of 02 Nov 2016 of MPET e-log.",\
"OLD Set Values = STRING : [128] (0,-0.209) (1,0.99) (2,-0.7625) (3,0.99) (4,1.3844) (5,1.3844) (7,-3)",\
"",\
"[transition_DPL1]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[transition_DPL2]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[transition_DQ1]",\
"ppg signal name = STRING : [15] RFTRIG3",\
"time offset (ms) = DOUBLE : 0.01",\
"",\
"[transition_QUAD1]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[transition_QUAD2]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 10",\
"",\
"[transition_DQ2]",\
"ppg signal name = STRING : [15] RFTRIG3",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[trans_TDCGate]",\
"ppg signal name = STRING : [16] TDCGATE",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TTRANSITION_DQ2",\
"",\
"[begin_slow_extraction]",\
"time offset (ms) = DOUBLE : 0",\
" loop count = INT : 5",\
"",\
"[evstep_Slow_Trap_Ejection]",\
" AWG unit = INT : 0",\
"start value (Volts) = STRING : [32] (2,-0.7625)(3,0.99) (4,1.2)",\
"end value (Volts) = STRING : [32] (2,0.0)(3,-1) (4,-2.0)",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[end_slow_extraction]",\
"time offset (ms) = DOUBLE : 0",\
"",\
"[trans_TDCGate2]",\
"ppg signal name = STRING : [16] TDCGATE",\
"time offset (ms) = DOUBLE : 0",\
"",\
"[skip]",\
"dummy = INT : 0",\
"",\
"[evset_Trap_Ejection]",\
"time offset (ms) = DOUBLE : 0.01",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.209) (1,1.0000) (2,-0.7625) (3,1.0000) (4,1.3844) (5,1.3844) (7,-3)",\
"Backup = STRING : [68] (0,-0.20) (1,1.0) (2,-0.775) (3,0.000) (4,-0.0000) (5,1.0) (7,-3.0)",\
"",\
"[evset_Trap_Ejection2]",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [75] (0,-0.209) (1,0.99) (2,0) (3,0) (4,-0.6) (5,1.0) (7,-3)",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[pul_TDCGate]",\
"pulse width (ms) = DOUBLE : 5.12",\
"ppg signal name = STRING : [16] TDCGATE",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TTRANSITION_DQ2",\
"",\
"[unskip]",\
"dummy = INT : 0",\
"",\
"[end_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"time_reference = STRING : [47] _TEND_pul_TDCGate (this is not doing anything)",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 0.001",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_PATTERN_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    DWORD     bit_pattern;
  } pattern1;
  struct {
    double    time_offset__ms_;
    DWORD     bit_pattern;
  } pattern2;
  struct {
    double    time_offset__ms_;
    DWORD     bit_pattern;
  } pattern3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse12;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse13;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse14;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse15;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse16;
  struct {
    double    time_offset__ms_;
  } end_scan;
  struct {
    double    time_offset__ms_;
    DWORD     bit_pattern;
  } pattern4;
} TITAN_ACQ_PPG_CYCLE_PATTERN;

#define TITAN_ACQ_PPG_CYCLE_PATTERN_STR(_name) const char *_name[] = {\
"[pattern1]",\
"time offset (ms) = DOUBLE : 0.1",\
"bit pattern = DWORD : 0",\
"",\
"[pattern2]",\
"time offset (ms) = DOUBLE : 5000",\
"bit pattern = DWORD : 4294967295",\
"",\
"[pattern3]",\
"time offset (ms) = DOUBLE : 5000",\
"bit pattern = DWORD : 0",\
"",\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 2500",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH1",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 1",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 1",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH10",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 1",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse12]",\
"time offset (ms) = DOUBLE : 1",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse13]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[pulse14]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH14",\
"",\
"[pulse15]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH15",\
"",\
"[pulse16]",\
"time offset (ms) = DOUBLE : 10",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH16",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[pattern4]",\
"time offset (ms) = DOUBLE : 10",\
"bit pattern = DWORD : 0",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_TEST_DEFINED

typedef struct {
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_mbr;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_plt_up;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
  } std_resetscalarcounter;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
    double    pulse_width__ms_;
  } pulse_test_ch16;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_ramp;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
    double    pulse_width__ms_;
  } pulse_test_ch15;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_ebittrig;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      backup[142];
  } evset_trap_injection;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_frequency_list_step;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_dipolefreq_step1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[16];
    char      comment[35];
  } std_mbr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_isacgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tsybl;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tofgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_plt_down;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      time_reference[20];
    char      observation[5][128];
  } evset_trap_capture;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dpl1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    char      backup[44];
  } pulse_dipolefreq_step2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
    char      time_reference[32];
  } transition_dpl2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad3;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad4;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq2;
  struct {
    INT       awg_unit;
    char      set_values__volts_[86];
    double    time_offset__ms_;
    char      backup[100];
  } evset_trap_ejection;
  struct {
    char      ppg_signal_name[32];
    char      time_reference[32];
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      backup[68];
  } pul_tdcgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[47];
  } end_ramp;
  struct {
    double    time_offset__ms_;
  } end_scan;
  struct {
    char      ppg_signal_name[32];
    char      time_reference[32];
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      backup[159];
  } pul_tdcgate_mbr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_mrtoftrig;
} TITAN_ACQ_PPG_CYCLE_TEST;

#define TITAN_ACQ_PPG_CYCLE_TEST_STR(_name) const char *_name[] = {\
"[transition_MBR]",\
"ppg signal name = STRING : [15] CH11",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[transition_PLT_Up]",\
"time offset (ms) = DOUBLE : 1000",\
"ppg signal name = STRING : [15] CH12",\
"",\
"[std_ResetScalarCounter]",\
"time offset (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 30",\
"",\
"[pulse_test_ch16]",\
"time offset (ms) = DOUBLE : 1e-05",\
"ppg signal name = STRING : [16] CH16",\
"pulse width (ms) = DOUBLE : 0.001",\
"",\
"[begin_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"loop count = INT : 5",\
"",\
"[pulse_test_ch15]",\
"time offset (ms) = DOUBLE : 0.0001",\
"ppg signal name = STRING : [16] CH15",\
"pulse width (ms) = DOUBLE : 0.0001",\
"",\
"[pulse_EBITtrig]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[evset_Trap_Injection]",\
"time offset (ms) = DOUBLE : 0.001",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-1.02) (1,-1.2000) (2,-0.7625) (3, 1.2) (4, 1.2) (5,-1.2) (7, 0.0) ",\
"Backup = STRING : [142] (0,-1.02) (1,-1.2000) (2,-0.7625) (3, 1.2) (4, 1.2) (5,-1.2) (7, 0.0) ",\
"",\
"[pulse_Frequency_List_Step]",\
"time offset (ms) = DOUBLE : 0.005",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH10",\
"",\
"[pulse_DipoleFreq_Step1]",\
"time offset (ms) = DOUBLE : 0.005",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH14",\
"",\
"[std_MBR]",\
"time offset (ms) = DOUBLE : 1.0001",\
"time reference = STRING : [20] _TBEGRamp",\
"ppg signal name = STRING : [16] CH11",\
"Comment = STRING : [35] 0.1213 is an offset after breeding",\
"",\
"[pulse_ISACGATE]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse_TSYBL]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse_TOFGate]",\
"time offset (ms) = DOUBLE : 0.0612",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse_PLT_Down]",\
"time offset (ms) = DOUBLE : 0.08699999999999999",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[evset_Trap_Capture]",\
"time offset (ms) = DOUBLE : 0.09",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-1.02) (1,0.9900) (2,-0.7625) (3,0.9900) (4,1.52) (5,1.52) (7,-3.0000)",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"Observation = STRING[5] :",\
"[128] Values for Caps (channels 1 and 3) and Ring (ch.2) are",\
"[128] set this way to account for offsets in the amplifiers.",\
"[128] Amplification factor is 20.00 for all.",\
"[128] Offset for Ring is -0.5V, for both Caps is +0.2V.",\
"[128] See wiki entry of 02 Nov 2016 of MPET e-log. save:",\
"",\
"[transition_DPL1]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[pulse_DipoleFreq_Step2]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TTRANSITION_DPL1",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH16",\
"Backup = STRING : [44] signal name should be CH14, CH16 is a spare",\
"",\
"[transition_DPL2]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [32] _TTRANSITION_DPL1",\
"",\
"[transition_DQ1]",\
"ppg signal name = STRING : [15] CH7",\
"time offset (ms) = DOUBLE : 0.01",\
"",\
"[transition_QUAD1]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[transition_QUAD2]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 100",\
"",\
"[transition_QUAD3]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 300",\
"",\
"[transition_QUAD4]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 100",\
"",\
"[transition_DQ2]",\
"ppg signal name = STRING : [15] CH7",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[evset_Trap_Ejection]",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [86] (0,-1.0200) (1,0.6500) (2,0.2500) (3,-0.2500) (4,-0.2500) (5,0.6500) (7,-0.2500)",\
"time offset (ms) = DOUBLE : 0.0001",\
"Backup = STRING : [100] (0,-1.0200) (1,0.6500) (2,0.2500) (3,-0.2500) (4,-0.2500) (5,0.6500) (7,-0.2500)     ",\
"",\
"[pul_TDCGate]",\
"ppg signal name = STRING : [32] CH13",\
"time reference = STRING : [32] _TTRANSITION_DQ2",\
"time offset (ms) = DOUBLE : 0.0001",\
"pulse width (ms) = DOUBLE : 0.15",\
"Backup = STRING : [68] CH13 is the active TDC one, when not using put on CH15, a spare one",\
"",\
"[end_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"time_reference = STRING : [47] _TEND_pul_TDCGate (this is not doing anything)",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 0.001",\
"",\
"[pul_TDCGate_MBR]",\
"ppg signal name = STRING : [32] CH15",\
"time reference = STRING : [32] _TSTART_std_MBR",\
"time offset (ms) = DOUBLE : 0.0001",\
"pulse width (ms) = DOUBLE : 0.15",\
"Backup = STRING : [159] CH13 is the active TDC one, when not using put on CH15, a spare one. pul_TDCGate is the one data analysis software will look at, so width needs to be the same",\
"",\
"[pulse_MRToFtrig]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH15",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_SKIP_DEFINED

typedef struct {
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_mbr;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_plt_up;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
  } std_resetscalarcounter;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_ramp;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_ebittrig;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_trap_injection;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_frequency_list_step;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[16];
  } std_mbr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_isacgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tsybl;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tofgate;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_plt_down;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      time_reference[20];
    char      observation[5][128];
  } evset_trap_capture;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dpl1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dpl2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad2;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad3;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_quad4;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_dq2;
  struct {
    char      ppg_signal_name[16];
    double    time_offset__ms_;
    char      time_reference[20];
  } trans_tdcgate;
  struct {
    INT       dummy;
  } skip;
  struct {
    double    time_offset__ms_;
    INT       _loop_count;
  } begin_slow_extraction;
  struct {
    INT       _awg_unit;
    char      start_value__volts_[32];
    char      end_value__volts_[32];
    double    time_offset__ms_;
  } evstep_slow_trap_ejection;
  struct {
    double    time_offset__ms_;
  } end_slow_extraction;
  struct {
    char      ppg_signal_name[16];
    double    time_offset__ms_;
  } trans_tdcgate2;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      backup[68];
  } evset_trap_ejection;
  struct {
    INT       awg_unit;
    char      set_values__volts_[75];
    double    time_offset__ms_;
  } evset_trap_ejection2;
  struct {
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    double    time_offset__ms_;
    char      time_reference[20];
  } pul_tdcgate;
  struct {
    INT       dummy;
  } unskip;
  struct {
    double    time_offset__ms_;
    char      time_reference[47];
  } end_ramp;
  struct {
    double    time_offset__ms_;
  } end_scan;
} TITAN_ACQ_PPG_CYCLE_SKIP;

#define TITAN_ACQ_PPG_CYCLE_SKIP_STR(_name) const char *_name[] = {\
"[transition_MBR]",\
"ppg signal name = STRING : [15] CH11",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[transition_PLT_Up]",\
"time offset (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [15] CH12",\
"",\
"[std_ResetScalarCounter]",\
"time offset (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 2500",\
"",\
"[begin_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"loop count = INT : 3",\
"",\
"[pulse_EBITtrig]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[evset_Trap_Injection]",\
"time offset (ms) = DOUBLE : 0.001",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.2) (1,-1.0000) (2,-0.7625) (3,1.0000) (4,1.0) (5,-1.0) (7,0.0)",\
"",\
"[pulse_Frequency_List_Step]",\
"time offset (ms) = DOUBLE : 0.005",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH10",\
"",\
"[std_MBR]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGRamp",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse_ISACGATE]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGRamp",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse_TSYBL]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse_TOFGate]",\
"time offset (ms) = DOUBLE : 0.0248",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse_PLT_Down]",\
"time offset (ms) = DOUBLE : 0.08699999999999999",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[evset_Trap_Capture]",\
"time offset (ms) = DOUBLE : 0.0905",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.209) (1,0.99) (2,-0.7625) (3,0.99) (4,1.3844) (5,1.3844) (7,-3)",\
"time reference = STRING : [20] _TSTART_std_MBR",\
"Observation = STRING[5] :",\
"[128] Values for Caps (channels 1 and 3) and Ring (ch.2) are",\
"[128] set this way to account for offsets in the amplifiers.",\
"[128] Amplification factor is 20.00 for all.",\
"[128] Offset for Ring is -0.5V, for both Caps is +0.2V.",\
"[128] See wiki entry of 02 Nov 2016 of MPET e-log.",\
"",\
"[transition_DPL1]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[transition_DPL2]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[transition_DQ1]",\
"ppg signal name = STRING : [15] CH7",\
"time offset (ms) = DOUBLE : 0.01",\
"",\
"[transition_QUAD1]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[transition_QUAD2]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 2",\
"",\
"[transition_QUAD3]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 6",\
"",\
"[transition_QUAD4]",\
"ppg signal name = STRING : [15] CH6",\
"time offset (ms) = DOUBLE : 2",\
"",\
"[transition_DQ2]",\
"ppg signal name = STRING : [15] CH7",\
"time offset (ms) = DOUBLE : 0.005",\
"",\
"[trans_TDCGate]",\
"ppg signal name = STRING : [16] CH13",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TTRANSITION_DQ2",\
"",\
"[skip]",\
"dummy = INT : 0",\
"",\
"[begin_slow_extraction]",\
"time offset (ms) = DOUBLE : 0",\
" loop count = INT : 5",\
"",\
"[evstep_Slow_Trap_Ejection]",\
" AWG unit = INT : 0",\
"start value (Volts) = STRING : [32] (2,-0.7625)(3,0.99) (4,1.2)",\
"end value (Volts) = STRING : [32] (2,0.0)(3,-1) (4,-2.0)",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[end_slow_extraction]",\
"time offset (ms) = DOUBLE : 0",\
"",\
"[trans_TDCGate2]",\
"ppg signal name = STRING : [16] CH13",\
"time offset (ms) = DOUBLE : 0",\
"",\
"[evset_Trap_Ejection]",\
"time offset (ms) = DOUBLE : 0.01",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-0.209) (1,1.0000) (2,-0.7625) (3,1.0000) (4,1.3844) (5,1.3844) (7,-3)",\
"Backup = STRING : [68] (0,-0.20) (1,1.0) (2,-0.775) (3,0.000) (4,-0.0000) (5,1.0) (7,-3.0)",\
"",\
"[evset_Trap_Ejection2]",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [75] (0,-0.209) (1,0.99) (2,0) (3,0) (4,-0.6) (5,1.0) (7,-3)",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[pul_TDCGate]",\
"pulse width (ms) = DOUBLE : 5.12",\
"ppg signal name = STRING : [16] CH13",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TTRANSITION_DQ2",\
"",\
"[unskip]",\
"dummy = INT : 0",\
"",\
"[end_ramp]",\
"time offset (ms) = DOUBLE : 0.001",\
"time_reference = STRING : [47] _TEND_pul_TDCGate (this is not doing anything)",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 0.001",\
"",\
NULL }

#define TITAN_ACQ_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      experiment_name[32];
      char      ppgload_path[50];
      char      ppg_path[50];
      char      ppg_perl_path[50];
      float     daq_service_time__ms_;
      float     standard_pulse_width__ms_;
      float     awg_clock_width__ms_;
      float     tdcblock_width__ms_;
      float     ppg_clock__mhz_;
      DWORD     polarity;
      BOOL      external_trigger;
      BOOL      external_clock;
      INT       afgload_wait__ms_;
    } input;
    struct {
      char      compiled_file_time[32];
      DWORD     compiled_file_time__binary_;
      float     time_slice__ms_;
      float     ppg_nominal_frequency__mhz_;
      float     minimal_delay__ms_;
      float     ppg_freq_conversion_factor;
      BOOL      ppg_running;
      double    last_transition__ms_;
      BOOL      external_clock;
      BOOL      good_external_clock;
      struct {
        BOOL      ppg_status_alarm;
        BOOL      external_clock_alarm;
      } alarms;
    } output;
  } ppg;
  struct {
    DWORD     number_of_steps;
    DWORD     pad;
    double    step_delay__ms_;
    double    vset__volts_[8];
    double    vend__volts_[8];
    INT       nsteps_before_ramp[8];
    INT       nsteps_after_ramp[8];
  } awg0;
  struct {
    DWORD     number_of_steps;
    DWORD     pad;
    double    step_delay__ms_;
    double    vset__volts_[8];
    double    vend__volts_[8];
    INT       nsteps_before_ramp[8];
    INT       nsteps_after_ramp[8];
  } awg1;
} TITAN_ACQ_SETTINGS;

#define TITAN_ACQ_SETTINGS_STR(_name) const char *_name[] = {\
"[ppg/input]",\
"Experiment name = STRING : [32] mpet",\
"PPGLOAD path = STRING : [50] /home/mpet/online/ppg/ppgload",\
"PPG path = STRING : [50] /home/mpet/online/ppg",\
"PPG perl path = STRING : [50] /home/mpet/online/ppg/perl",\
"DAQ service time (ms) = FLOAT : 0",\
"standard pulse width (ms) = FLOAT : 0.0015",\
"AWG clock width (ms) = FLOAT : 0.001",\
"TDCBLOCK width (ms) = FLOAT : 0.001",\
"PPG clock (MHz) = FLOAT : 100",\
"Polarity = DWORD : 0",\
"external trigger = BOOL : n",\
"external clock = BOOL : y",\
"afgload_wait (ms) = INT : 100",\
"",\
"[ppg/output]",\
"compiled file time = STRING : [32] Mon Jan 16 16:03:11 2017",\
"compiled file time (binary) = DWORD : 1484611391",\
"Time Slice (ms) = FLOAT : 1e-05",\
"PPG nominal frequency (MHz) = FLOAT : 10",\
"Minimal Delay (ms) = FLOAT : 5e-05",\
"PPG freq conversion factor = FLOAT : 10",\
"PPG running = BOOL : n",\
"last transition (ms) = DOUBLE : 0",\
"external clock = BOOL : y",\
"good external clock = BOOL : y",\
"",\
"[ppg/output/alarms]",\
"ppg status alarm = BOOL : n",\
"external clock alarm = BOOL : n",\
"",\
"[awg0]",\
"number of steps = DWORD : 5",\
"pad = DWORD : 0",\
"step delay (ms) = DOUBLE : 0.01",\
"Vset (volts) = DOUBLE[8] :",\
"[0] -0.209",\
"[1] 0.99",\
"[2] -0.7625",\
"[3] 0.99",\
"[4] 1.3844",\
"[5] 1.3844",\
"[6] 0.1",\
"[7] -3",\
"Vend (volts) = DOUBLE[8] :",\
"[0] 1.2",\
"[1] 0.8",\
"[2] 0",\
"[3] -0.1",\
"[4] -2",\
"[5] 0",\
"[6] 0.1",\
"[7] -0.05",\
"Nsteps before ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"Nsteps after ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"",\
"[awg1]",\
"number of steps = DWORD : 1",\
"pad = DWORD : 0",\
"step delay (ms) = DOUBLE : 100",\
"Vset (volts) = DOUBLE[8] :",\
"[0] 1",\
"[1] 1",\
"[2] -0.77",\
"[3] 1",\
"[4] 1",\
"[5] 0",\
"[6] 0",\
"[7] -0.5",\
"Vend (volts) = DOUBLE[8] :",\
"[0] 2",\
"[1] 2",\
"[2] 2",\
"[3] 2",\
"[4] 2",\
"[5] 1",\
"[6] 1",\
"[7] -0.5",\
"Nsteps before ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"Nsteps after ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"",\
NULL }

#endif

