#####################################################################
#
#  Name:         Makefile
#  Created by:   Stefan Ritt
#
#  Contents:     Makefile for MIDAS 
#
#  $Id: Makefile 2826 2005-10-25 19:55:44Z amaudruz $
#
#####################################################################
#
#--------------------------------------------------------------------
# The MIDASSYS should be defined prior the use of this Makefile
ifndef MIDASSYS
missmidas::
	@echo "...";
	@echo "Missing definition of environment variable 'MIDASSYS' !";
	@echo "...";
endif

#--------------------------------------------------------------------
# The following lines contain specific switches for different UNIX
# systems. Find the one which matches your OS and outcomment the 
# lines below.

#-----------------------------------------
# This is for Linux
ifeq ($(OSTYPE),Linux)
OSTYPE = linux
endif

ifeq ($(OSTYPE),linux)

OS_DIR = linux-m32
OSFLAGS = -DOS_LINUX -DHAVE_LIBUSB -Dextname
CFLAGS = -m32 -g -O2 -Wall 
LIBS = -lvme -lusb -lutil -lnsl -lpthread -lrt -lstdc++
endif

#-----------------------
# MacOSX/Darwin is just a funny Linux
#
ifeq ($(OSTYPE),Darwin)
OSTYPE = darwin
endif

ifeq ($(OSTYPE),darwin)
OS_DIR = darwin
FF = cc
OSFLAGS = -DOS_LINUX -DOS_DARWIN -DHAVE_STRLCPY -DAbsoftUNIXFortran -fPIC -Wno-unused-function
LIBS = -lpthread
SPECIFIC_OS_PRG = $(BIN_DIR)/mlxspeaker
NEED_STRLCPY=
NEED_RANLIB=1
NEED_SHLIB=
NEED_RPATH=

endif

#-----------------------------------------
# ROOT flags and libs
#
ifdef ROOTSYS
ROOTCFLAGS := $(shell  $(ROOTSYS)/bin/root-config --cflags)
ROOTCFLAGS += -DHAVE_ROOT -DUSE_ROOT
ROOTLIBS   := $(shell  $(ROOTSYS)/bin/root-config --libs) -Wl,-rpath,$(ROOTSYS)/lib
ROOTLIBS   += -lThread
#else
#missroot:
#	@echo "...";
#	@echo "Missing definition of environment variable 'ROOTSYS' !";
#	@echo "...";
endif

#-------------------------------------------------------------------
# The following lines define directories. Adjust if necessary
#                 
DRV_DIR   = $(MIDASSYS)/drivers
MIDAS_DRV_PMC = $(DRV_DIR)/pci
MSCB_DIR  = $(MIDASSYS)/mscb
INC_DIR   = $(MIDASSYS)/include
LIB_DIR   = $(MIDASSYS)/$(OS_DIR)/lib
SRC_DIR   = $(MIDASSYS)/src
#-----------------------------------------------------------------
#ifdef SOFTDAC
SOFTINC =  -I$(MIDAS_DRV_PMC)/pmcsoftdacm
DA816INC = -I$(MIDAS_DRV_PMC)/pmcda816
#-------------------------------------------------------------------
# Frontend code name defaulted to frontend in this example.
# comment out the line and run your own frontend as follow:
# gmake UFE=my_frontend
#
DEFINES = -D PPG_CODE -D NEW_PPG -D MPET
CFLAGS += $(DEFINES)
UFE = fempet
# newppg
PPG_DIR    = ./
PPGOBJ =  common.o newppg.o  ppg_code.o
# old ppg
#PPGOBJ = vppg.o ppg_code.o
####################################################################
# Lines below here should not be edited
####################################################################

# MIDAS library
LIB = $(LIB_DIR)/libmidas.a 

# compiler
make cleanCC = gcc
CXX = g++
CFLAGS += -g -I. -I$(INC_DIR) -I$(MIDASSYS)/../mxml -I$(DRV_DIR) -I$(DRV_DIR)/devices \
	-I$(DRV_DIR)/class -I$(MSCB_DIR) -I$(DRV_DIR)/vme -I$(DRV_DIR)/vme/vmic $(SOFTINC)\
	$(DA816INC)
LDFLAGS +=

all: $(UFE)

$(UFE): $(LIB) $(LIB_DIR)/mfe.o vmeio.o vt2.o lrs1190.o lrs1151.o $(PPGOBJ) vmicvme.o \
	mscbdev.o mscbbus.o mscbrpc.o mscb.o \
	mxml.o strlcpy.o musbstd.o softdac.o da816.o $(UFE).c 

	$(CC) $(CFLAGS) $(OSFLAGS) -o $(UFE) $(UFE).c \
	vmicvme.o  $(PPGOBJ) vmeio.o vt2.o lrs1190.o lrs1151.o \
	mscbdev.o mscbrpc.o mscb.o musbstd.o  softdac.o da816.o mxml.o strlcpy.o \
	$(LIB_DIR)/mfe.o $(LIB) $(LDFEFLAGS) $(LIBS)

vmicvme.o: $(DRV_DIR)/vme/vmic/vmicvme.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
vmeio.o: $(DRV_DIR)/vme/vmeio.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
lrs1190.o: $(DRV_DIR)/vme/lrs1190.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
lrs1151.o: $(DRV_DIR)/vme/lrs1151.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
vt2.o: $(DRV_DIR)/vme/vt2.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
da816.o: $(DRV_DIR)/pci/pmcda816/da816.cxx
	$(CXX) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
vppg.o: $(DRV_DIR)/vme/vppg.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
mscbdev.o: $(DRV_DIR)/device/mscbdev.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
musbstd.o: $(DRV_DIR)/usb/musbstd.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
mscbbus.o: $(DRV_DIR)/bus/mscbbus.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
mscbrpc.o: $(MSCB_DIR)/mscbrpc.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
mscb.o: $(MSCB_DIR)/mscb.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
mxml.o: $(MIDASSYS)/../mxml/mxml.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<
strlcpy.o: $(MIDASSYS)/../mxml/strlcpy.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<

# softdac
softdac.o: $(MIDAS_DRV_PMC)/pmcsoftdacm/softdac.cxx
	$(CXX) -m32 -O2 -Wall -Wuninitialized -c $< -o $@

#newppg
newppg.o: $(PPG_DIR)/newppg.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<

common.o: $(PPG_DIR)/common.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<

ppg_code.o: $(PPG_DIR)/ppg_code.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<


%.o: %.c experim.h
	$(CXX) $(USERFLAGS) $(ROOTCFLAGS) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

clean::
	rm -f *.o *~ \#*

#end file
