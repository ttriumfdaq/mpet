#ifndef PPG_COMMON_INCLUDE_H
#define PPG_COMMON_INCLUDE_H

#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED
typedef unsigned short int WORD;
typedef int                INT;
typedef char               BYTE;
typedef unsigned int       DWORD;
typedef unsigned int       BOOL;
#endif /* MIDAS_TYPE_DEFINED */
#define FAILURE 0
#define SUCCESS 1
typedef struct
{
    DWORD    pc;
    DWORD   setpat;
    DWORD   clrpat;
    DWORD   delay;
    DWORD   ins_data; // instruction and data
}COMMAND;

// prototypes for common.c
int getinsline(char *line, int max, FILE *file);
COMMAND lineRead(char *line);
void  lineWrite (COMMAND *data_struct);
#endif

