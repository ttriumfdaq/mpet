/********************************************************************\

  Name:         odb_test.c
  Created by:   Stefan Ritt

  Contents:     Small demo program to show how to connect to the ODB


\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "midas.h"



int main()
{
   int status, size;
   HNDLE hDB, hKey;
   int run_number;
   char path[80];
   char str[80];

   /* connect to default experiment */
   status = cm_connect_experiment("", "", "ODBTest", NULL);
   if (status != CM_SUCCESS)
      return 1;

   /* get handle to online database */
   cm_get_experiment_database(&hDB, &hKey);


   // Get data from the ODB

   /* read key "/runinfo/run number"  (INTEGER) */
   size = sizeof(run_number);
   status =
       db_get_value(hDB, 0, "/runinfo/run number", &run_number, &size, TID_INT, TRUE);
   if (status != DB_SUCCESS) {
      printf("Cannot read run number");
      return status;
   }

   printf("Current run number is %d\n", run_number);

 
   /* read key "/equipment/titan_acq/settings/ppg/input"  (STRING) */
  size = sizeof(path);
  sprintf(str,"/equipment/titan_acq/settings/ppg/input/PPGLOAD path");
  status = db_get_value(hDB, 0, str, path, &size, TID_STRING, FALSE);
  if(status != DB_SUCCESS)
  {
    /* send a message to the MIDAS message log
           use MINFO for informational message
	   MERROR for error message */
    cm_msg(MERROR,"odb_test","cannot read key at %s (%d)",str,status);
    return status;
  }
  printf("Read PPG path as:\"%s\"\n",path);

  /* Write data to the ODB  */

  // commented out for MPET as we don't want to change the run number

  /* set new run number */
  //  run_number++;
  //   db_set_value(hDB, 0, "/runinfo/run number", &run_number, size, 1, TID_INT);

   /* disconnect from experiment */
   cm_disconnect_experiment();

   return 1;
}
