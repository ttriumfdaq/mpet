/********************************************************************\

  Name:         fempet.c
  Created by:   PAA

  Contents:     MCP, VT2

  $Id: fempet.c 83 2007-11-06 21:15:40Z midas $

\********************************************************************/
#define    VMEIO_CODE
#define      VT2_CODE
#define    DA816_CODE
//#define      PPG_CODE // defined in Makefile  have a PPG
//#define      NEW_PPG  // defined in Makefile  have TRIUMF VMEPPG32 
#define     VMCP_CODE
#define  SOFTDAC_CODE

#undef VF48_CODE
#undef VERBOSE

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h> // time
#include <sys/time.h>
#include  <time.h> // gettimeofday
#include "midas.h"
#include "mvmestd.h"
#include "vmicvme.h"
#ifdef VMEIO_CODE
#include "vmeio.h"
#endif
#ifdef VF48_CODE
#include "vf48.h"
#endif
#ifdef VT2_CODE
#include "vt2.h"
#endif
#ifdef DA816_CODE
#include "da816.h"
ALPHIDA816 *da816=NULL;
#endif
#if defined SOFTDAC_CODE
#include "softdac.h"
ALPHISOFTDAC  *softdac = NULL;
char *SOFTDAC_RANGE[] = {"P5V", "P10V", "PM5V", "PM10V", "PM2P5V", "M2P5P7P5V", "off"};     
#endif

#ifdef VMCP_CODE
#include "lrs1190.h"
#endif


#include "experim.h"

TITAN_ACQ_SETTINGS tas;
TITAN_ACQ_SETTINGS_STR(titan_acq_settings_str); 

/* Interrupt vector */
int trig_level =  0;
#define TRIG_LEVEL  (int) 1
#define INT_LEVEL   (int) 3
#define INT_VECTOR  (int) 0x16
extern INT_INFO int_info;
int myinfo = VME_INTERRUPT_SIGEVENT;

/* Globals */
extern INT run_state;
extern char host_name[HOST_NAME_LENGTH];
extern char exp_name[NAME_LENGTH];

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fempet";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 000;

/* maximum event size produced by this frontend */
INT max_event_size = 60000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 10 * 20000;

/* Hardware */
MVME_INTERFACE *myvme;


/* VME base address */
DWORD VMEIO_BASE = 0x780000;
DWORD VT2_BASE   = 0xE00000;
DWORD VMCP_BASE  = 0x790000;
DWORD VF48_BASE  = 0xA00000;
#ifdef NEW_PPG
DWORD PPG_BASE     = 0x00100000; // NEW PPG VMEPPG32
#else
DWORD PPG_BASE   = 0x008000; // OLD Pulseblaster PPG
#endif

/* Globals */
extern HNDLE hDB;
  HNDLE hSet, hTASet, hOut;
TRIGGER_SETTINGS ts;
BOOL  end_of_cycle = FALSE;
BOOL  transition_PS_requested= FALSE;
INT   gbl_cycle;
BOOL   debug=0;

#ifdef PPG_CODE
char data_dir[256];
INT ext_warn=0; // used to send warning message about PPG ext. trig

#include "ppg_code.h" // prototypes
#include "unistd.h" // for sleep 
FILE *ppginput;

char tri_cmd[128];
char ppgfile[128];
BOOL ppg_running,active;
char PPGpath[80];
char plot_cmd[132];
char copy_cmd[132];
BOOL ppg_error_flag=TRUE; // default

// these added when adding newppg
BOOL ppg_external_clock = FALSE; 
BOOL ppg_external_trig = FALSE;  
DWORD ppg_polarity_mask;

#ifdef NEW_PPG
#include "newppg.h"   // prototypes
#include "ppg_modify_file.h" // prototypes
#else
ppg_external_clock = TRUE; // fixed by hardware for old PPG 
#include "vppg.h"  // old PPG only
#endif // NEW_PPG
#endif // PPG_CODE

/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
extern void interrupt_routine(void);

INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);
INT read_titan_event(char *pevent, INT off);
INT get_titan_settings(void);

#ifdef PPG_CODE
  INT ppg_load(char *ppgfile);
  INT tr_checkppg(INT run_number, char *error);
  INT tr_poststart(INT run_number, char *error);
#endif

BANK_LIST trigger_bank_list[] = {

   /* online banks */
   {"MPET", TID_DWORD,  100, NULL} ,
   {"MCPP", TID_DWORD,  100, NULL} ,
   {"FWAV", TID_DWORD, 2000, NULL} ,
   {""}
   ,
};
  /* globals */
  INT start_time,elapsed_time;
  BOOL stop_flag=FALSE;

/*-- Equipment list ------------------------------------------------*/

#undef USE_INT

EQUIPMENT equipment[] = {

   {"Trigger",               /* equipment name */
    {1, 0,                  /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
#ifdef USE_INT
     EQ_INTERRUPT,           /* equipment type */
#else
     EQ_PERIODIC, //POLLED,     /* equipment type */
#endif
     LAM_SOURCE(0, 0x0),     /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING,             /* read only when running */
     500,                    /* poll for 500ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",},
    read_trigger_event,      /* readout routine */
    NULL, NULL,
    trigger_bank_list,
    }
   ,

   {"Scaler",                /* equipment name */
    {2, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC ,           /* equipment type */
     0,                      /* event source */
     "MIDAS",                /* format */
     FALSE,                   /* enabled */
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
     RO_ODB,                 /* and update ODB */
     10000,                  /* read every 10 sec */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* log history */
     "", "", "",},
    read_scaler_event,       /* readout routine */
    },

  {"Titan_acq",                /* equipment name */
    {10, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC ,           /* equipment type */
     0,                      /* event source */
     "MIDAS",                /* format */
     FALSE,                   /* enabled */
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
     RO_ODB,                 /* and update ODB */
     10000,                  /* read every 10 sec */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* log history */
     "", "", "",},
    read_titan_event,       /* readout routine */
    },

   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/********************************************************************/

/*-- Sequencer callback info  --------------------------------------*/
void seq_callback(INT hDB, INT hseq, void *info)
{
  printf("odb ... trigger settings touched\n");
}


/*-- Deferred transition -------------------------------------------*/
/* will be called repeatively  while transition is requested until transition is issued */
BOOL wait_end_cycle(int transition, BOOL first)
{
  if (first) {
    /* Will go through here the first time wait_end_cycle() is called */
    /* setup user flags */
    transition_PS_requested = TRUE;
    printf("PS requested\n");
    /* return false as long as the requested transition should be postponed */
    return FALSE;
  }

  /* Check the user flags for issuing the requested transition */
  if (end_of_cycle)  {
    transition_PS_requested = FALSE;
    end_of_cycle = FALSE;
    /* Tell system to issue transition */
    printf(" do transition ....\n");
    return TRUE;
  }
  else {
    /* in any other case don't do anything ==> no transition */
      printf(" waiting for end of cycle...\n");
    return FALSE;
    }
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
  int size, status;
  char set_str[80];
  // char path[80];

  /* register for deferred transition - not necessary */

   status = cm_register_deferred_transition(TR_STOP, wait_end_cycle);
   status = cm_register_deferred_transition(TR_PAUSE, wait_end_cycle);

  // Open VME interface
  status = mvme_open(&myvme, 0);

#ifdef PPG_CODE
  status = cm_register_transition(TR_START, tr_checkppg, 350);
  if(status != CM_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init", "cannot register to transition for tr_checkppg");
      return status;
    }
  status = cm_register_transition(TR_START, tr_poststart, 600);
  if(status != CM_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init", "cannot register to transition for tr_poststart");
      return status;
    }

#endif

  /* Book Setting space */
  TRIGGER_SETTINGS_STR(trigger_settings_str);

  /* Map /equipment/Trigger/settings for the sequencer */
  sprintf(set_str, "/Equipment/Trigger/Settings");
  status = db_create_record(hDB, 0, set_str, strcomb(trigger_settings_str));
  status = db_find_key (hDB, 0, set_str, &hSet);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"FE","Key %s not found", set_str);



  /* Enable hot-link on settings/ of the equipment */
  size = sizeof(TRIGGER_SETTINGS);
  if ((status = db_open_record(hDB, hSet, &ts, size, MODE_READ
                               , seq_callback, NULL)) != DB_SUCCESS)
    return status;

#ifdef PPG_CODE
   status = get_titan_settings(); // create records, get ODB keys etc.
  if(status !=SUCCESS)
    return status;
 // get the data directory for later
  size = sizeof(data_dir);
  status = db_get_value(hDB, 0, "/logger/data dir",
                                   data_dir, &size, TID_STRING, 0);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","error getting \"data dir\" (%d)",status);
      return status;
    }

   status = init_ppg(); // initialize but do not load PPG
   if(status != SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","error return from init_ppg (%d)",status);
      return status;
    }
#endif // PPG_CODE

  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
#ifdef PPG_CODE

  /*Disable the PPG module just in case */
#ifdef NEW_PPG
  TPPGStopSequencer(myvme,   PPG_BASE); // Halt pgm and stop
#else
  VPPGStopSequencer(myvme, PPG_BASE);
#endif
#endif
   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  int status, size;
  // BYTE value;

  /* Get current  settings */
  /*  sprintf(set_str, "/Equipment/TITAN_ACQ/Settings"); 
  size = sizeof(TITAN_ACQ_SETTINGS);
  status = db_get_record(hDB, hTASet, &tas, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "begin_of_run", "cannot retrieve %s record (structure size=%d; size of record=%d ) %d", 
	     set_str, sizeof(TITAN_ACQ_SETTINGS),size,status);
      return status;
    }
  */
  stop_flag=FALSE;

   /* put here clear scalers etc. */

  /* read Triggger settings */
  size = sizeof(TRIGGER_SETTINGS);
  if ((status = db_get_record (hDB, hSet, &ts, &size, 0)) != DB_SUCCESS)
    return status;

#if defined DA816_CODE
#endif

#if defined SOFTDAC_CODE
  int i, range;
#endif


#if defined PPG_CODE
 
  if(ppg_error_flag)  // tr_ppgcheck failed; do not load and start PPG
    {
      printf("*** Error from check_ppg (tri_config) *** \n");
   
      cm_msg(MERROR,"begin_of_run","Failure from tr_ppgcheck. Check tri_config errors");
      return FE_ERR_HW;
    }

  printf("begin_of_run: setting up PPG\n");
  status = setup_ppg();
  if (status != SUCCESS)
    return status;
 

  printf("begin_of_run: loading PPG\n");
  if(ppg_load(ppgfile) != SUCCESS)
    return FE_ERR_HW;
  else 
    printf("Successfully loaded ppg\n");

  if(ppg_external_trig)
    printf("PPG External trigger is selected\n");
  else
       printf("PPG Internal trigger is selected\n");
#endif

#if defined VF48_CODE
  /* Initialize the VF48 --------------------------------------------*/
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);

  vf48_Reset(myvme, VF48_BASE);

  vf48_ExtTrgSet(myvme, VF48_BASE);

  // Set segment size to 16 for now
  for (i=0;i<6;i++)
    vf48_SegSizeSet(myvme, VF48_BASE, i, ts.vf48.nsamples);

  for (i=0;i<6;i++)
    printf("SegSize grp %i:%d\n", i, vf48_SegSizeRead(myvme, VF48_BASE, i));

  // Set Threshold high for external trigger, as the
  // the self-trigger is always sending info to the collector
  // and overwhelming the link. (max maybe 10 bits (0x3FF))
  for (i=0;i<6;i++)
    vf48_HitThresholdSet(myvme, VF48_BASE, i, 0xFF);

  // Set pre-trigger (default:32)
  printf("Pre-Trigger : %d\n", ts.vf48.pretrigger);
  vf48_PreTriggerSet(myvme, VF48_BASE, 0, ts.vf48.pretrigger);

  printf("vf48: CSR:0x%x\n", vf48_CsrRead(myvme, VF48_BASE));

  // Start ACQ for VF48
  vf48_AcqStart(myvme, VF48_BASE);
#endif

#if defined VT2_CODE
  //-------- VMEIO ---------------------------------------------
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);

  vt2_KeepAlive(myvme, VT2_BASE, ts.vt2.keep_alive);
  vt2_CycleReset(myvme, VT2_BASE, ts.vt2.cycle_reset);

  printf("vt2: CSR:0x%x\n", vt2_CSRRead(myvme, VT2_BASE));

#endif

#if defined VMCP_CODE
  //-------- VMCP ---------------------------------------------
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  lrs1190_Reset(myvme, VMCP_BASE);

  printf("vmcp: Count:0x%x\n", lrs1190_CountRead(myvme, VMCP_BASE));

  // Enable the module
  lrs1190_Enable(myvme, VMCP_BASE);
#endif

#if defined VMEIO_CODE
  //-------- VMEIO ---------------------------------------------
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);

  // Set 0xF in pulse mode
  vmeio_OutputSet(myvme, VMEIO_BASE, 0x1);
#endif

  //------ FINAL ACTIONS before BOR -----------
  /* reset flag */
  end_of_cycle = FALSE;

  /* reset the global cycle counter */
  gbl_cycle = 0;

#if defined VT2_CODE
  mvme_set_am(myvme, MVME_AM_A24_ND);
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  vt2_ManReset(myvme, VT2_BASE);
#endif


#if defined SOFTDAC_CODE
  if (softdac_Open(&softdac) < 0)
    {
      cm_msg(MERROR,"fempet","Cannot open PMC-SOFTDAC device!");
      return FE_ERR_HW;
    }
  softdac_SMEnable(softdac);
  printf("softdac SM enabled\n");
  softdac_Status(softdac,1);

  // Load Voltage range
  
  for (i=0;i<SOFTDAC_RANGE_MAX+1;i++) {
    if (strcmp(ts.softdac.range, SOFTDAC_RANGE[i]) == 0) {
      range = SOFTDAC_RANGE_P5V + i;
      softdac_ScaleSet(softdac, range, 0., 0.);
      cm_msg(MINFO, "mpet", "Softdac Range set to %s", ts.softdac.range);
    }    
  }
  if (i == SOFTDAC_RANGE_MAX) {
    cm_msg(MERROR, "mpet", "Softdac Range unknown (%s) uses previous value", ts.softdac.range);
  }
  if (softdac) softdac_Close(softdac);
#endif
  
#if defined DA816_CODE
  if (da816_Open(&da816) < 0)
    {
      cm_msg(MERROR,"fempet","Cannot open PMC-DA816 device!");
      return FE_ERR_HW;
    }
  da816_BankSwitch(da816, 9999);
  da816_AddReset(da816);
  da816_ClkSMEnable(da816,0);
  printf("da816  SM enabled\n");
  da816_Status(da816,1);
  if (da816) da816_Close(da816);
#endif

#ifdef PPG_CODE
 if(ppg_external_trig)
    {
      printf("\n\n   PPG is waiting for an external trigger...\n\n");
      printf("End of BOR\n"); 
      return SUCCESS;
    }
 else
   {      
     /* start the PPG  */ 
     printf("Starting PPG\n");
#ifdef NEW_PPG
     TPPGStartSequencer(myvme, PPG_BASE );
#else
     VPPGStartSequencer(myvme, PPG_BASE);
#endif

     start_time=ss_time();
     check_ppg_running();  // fills global ppg_running
     printf("begin_of_run: PPG is running\n");
     /* status = db_set_value(hDB, hOut, "PPG running", &ppg_running, sizeof(ppg_running), 1, TID_BOOL); 
      if(status == DB_SUCCESS) 
	{ 
	  active=1;
	  status = db_set_value(hDB, 0, "/Alarms/Alarms/PPG/active", &active, sizeof(active), 1, TID_BOOL);
	  if(status != DB_SUCCESS) 
	    cm_msg(MERROR,"frontend_init","cannot set /Alarms/Alarms/PPG/active =TRUE  (%d)",status); 
	}
      else
	{ 
	  cm_msg(MERROR,"frontend_init","cannot set ODB key \"PPG_running\" to %d  (%d)",ppg_running, status); 
	  // return FE_ERR_ODB;
	  } */
   }

#endif // PPG_CODE

  printf("End of BOR\n");
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{

#ifdef PPG_CODE
#ifdef NEW_PPG
  ppg_stop(); // also sets required output pattern in case stopped mid-cycle 
#else
  VPPGStopSequencer(myvme,PPG_BASE); /* stops sequencer; later disable ext. trigger if used */
#endif
 // disable the alarm system while the run is stopped
  active= FALSE;
  /*  status = db_set_value(hDB, 0, "/Alarms/Alarms/PPG/active", &active, sizeof(active), 1, TID_BOOL);
  if(status != DB_SUCCESS) 
    cm_msg(MERROR,"frontend_init","cannot set /Alarms/Alarms/PPG/active to %d  (%d)",active,status); 
  status = db_set_value(hDB, 0, "/Alarms/Alarms/PPG/triggered", &clear, sizeof(clear), 1, TID_INT);
  if(status != DB_SUCCESS)    
  cm_msg(MERROR,"frontend_init","cannot set /Alarms/Alarms/PPG/triggered to %d  (%d)",active,clear); 
  */
  ppg_running = FALSE;
  /*
  status = db_set_value(hDB, hOut, "PPG running", &ppg_running, sizeof(ppg_running), 1, TID_BOOL); 
  if(status != DB_SUCCESS) 
    cm_msg(MERROR,"frontend_init","cannot set PPG running FALSE  (%d)",status); 


  printf("\n end_of_run: set ppg_running and ppg alarm false\n");
  */
#endif // PPG_CODE

#ifdef VMCP_CODE
  // Disable the module
  lrs1190_Disable(myvme, VMCP_BASE);
#endif

#ifdef PPG_CODE
  printf("\n end_of_run: PPG status is \n");
#ifdef NEW_PPG
  DWORD data;
  data =  TPPGStatusRead( myvme, PPG_BASE); // writes status of clock and trigger
#else
  VPPGStatusRead(myvme,PPG_BASE);
#endif
#endif
  stop_flag=FALSE;
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  /* reset flag */
  end_of_cycle = FALSE;
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  char str[80];
  //  BYTE value;


#ifdef PPG_CODE
  INT status;
  if (run_state == STATE_RUNNING)
    { // internal trigger - mpet runs with a scan loop i.e. started once per run
      check_ppg_running(); // sets/clears flag "ppg_running" 
      if  (ppg_running) 
	{ 
	  if (debug) 
	    printf("frontend_loop:  PPG IS running\n"); 
	} 
      else
	{ /* PPG not running */
	  elapsed_time = ss_time() - start_time;
	      
	  cm_msg(MINFO,"frontend_loop",
		 "PPG is NOT running; attempting to stop run, elapsed time=%d sec",
		 elapsed_time);
	      
	  if(!stop_flag)
	    {
	      printf("frontend_loop: PPG has stopped. Attempting to stop the run...\n");
	      
	      // ATG 2016-04-01: OR=ing the TR_DEFERRED caused the cm_transition2(...) to reset the 
	      // deferred transition flag in /Run Info/. By removing this, the registered deferred
	      // transition at run_stop are now called.
	      //status = cm_transition(TR_STOP | TR_DEFERRED, 0, str, sizeof(str), ASYNC, 0);
	      //status = cm_transition(TR_STOP, 0, str, sizeof(str), ASYNC, 0);
	      
	      ///status = cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, 0);
	      ///if(status !=  CM_SUCCESS) 
	      ///cm_msg(MERROR, "FEloop", "cannot stop run immediately due to deferred transition: %s (%d)", str,status);
	      if (cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, 0) != CM_SUCCESS) 
		status = cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, 0); 
	      if((status !=  CM_SUCCESS) && (status != CM_DEFERRED_TRANSITION)) 
		cm_msg(MERROR, "frontend_loop", "cannot stop run immediately: %s (%d)", str,status); 
	      stop_flag=TRUE; // set a flag: trying to stop the run
	    }
	  ss_sleep(100); // run takes time to stop
	}
    }
  //-PAA slow down the ACC on the PPG (no effect on the DAQ)
  ss_sleep(100);
  //  if (debug)
  //  {
  //    check_ppg_running(); // sets/clears flag ppg_running 
  //    if(!ppg_running) ("PPG IS NOT running\n");
  //  }
#endif
  return SUCCESS;
}

/*------------------------------------------------------------------*/
/********************************************************************\

  Readout routines for different events

\********************************************************************/
/*-- Trigger event routines ----------------------------------------*/
INT poll_event(INT source, INT count, BOOL test)
     /* Polling routine for events. Returns TRUE if event
  is available. If test equals TRUE, don't return. The test
  flag is used to time the polling */
    //    if (vmeio_CsrRead(myvme, VMEIO_BASE))
    //    if (lam > 10)
{
  int i;
  int lam = 0;

  for (i = 0; i < count; i++, lam++) {
    lam = vmeio_CsrRead(myvme, VMEIO_BASE);
    if (lam)
      if (!test)
  return lam;
  }

  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/
INT read_trigger_event(char *pevent, INT off)
{
#if defined VMEIO_CODE
INT  latched_word, evNoStrobe;
#endif

#if defined VT2_CODE
  DWORD *pdata;
  INT level;
#endif

#if defined VMCP_CODE
  INT count;
  DWORD *pmdata;
#endif

#if defined VF48_CODE
  DWORD *pwave;
  DWORD nframe, nentry;
#endif

  /* FIRST THING TO DO */
  bk_init(pevent);

  /* In case we're coming as a Periodic equipment
     Check if the cycle is finished */

#if defined VMEIO_CODE
  ///-------------------------------
  vmeio_AsyncWrite(myvme, VMEIO_BASE, 0x2);
  vmeio_SyncWrite(myvme,  VMEIO_BASE, 0x1);
  vmeio_AsyncWrite(myvme, VMEIO_BASE, 0x0);

  if ((vmeio_CsrRead(myvme, VMEIO_BASE) & 0xF)!= 0) {
    latched_word = vmeio_SyncRead(myvme, VMEIO_BASE);
#if defined VERBOSE
    printf("vmeio read 0x%x\n",latched_word);
#endif
  } else {
    evNoStrobe ++;
#if defined VERBOSE
    printf(" No vmeio strobe found \n");
#endif
  }
  vmeio_StrobeClear(myvme, VMEIO_BASE);
#endif

#if defined VT2_CODE
  ///-------------------------------
  level = vt2_FifoLevelRead(myvme, VT2_BASE);
#if defined VERBOSE
  printf("Level/Cycle VT2: %d / %d (0x%x) Level:0x%x\n", level, vt2_CycleNumberRead(myvme, VT2_BASE)
   , vt2_CSRRead(myvme, VT2_BASE)
   , mvme_read_value(myvme, VT2_BASE+VT2_FIFOSTATUS_RO));
#endif
  if ( level > 0) {
    bk_create(pevent, "MPET", TID_DWORD, &pdata);
    vt2_FifoRead(myvme, VT2_BASE, pdata, level);
    pdata += level;
    bk_close(pevent, pdata);
  }
#endif

#if defined VMCP_CODE
  ///-------------------------------
  count = lrs1190_CountRead(myvme, VMCP_BASE);
  if (count > 0) {
    bk_create(pevent, "MCPP", TID_DWORD, &pmdata);
      lrs1190_I4Read(myvme, VMCP_BASE, pmdata, count);
      pmdata += count;
      bk_close(pevent, pmdata);
  }
#endif

#if defined VF48_CODE
  ///-------------------------------
  nframe = VF48_NFrameRead(myvme, VF48_BASE);
  if (nframe > 0) {
    /* create variable length WAVE bank */
    bk_create(pevent, "WAVE", TID_DWORD, &pwave);
    status = vf48_EventRead(myvme, VF48_BASE, pwave, 0, &nentry);
    pwave += nentry;
    bk_close(pevent, pwave);
  }
#endif

  if (transition_PS_requested) {
    /* transition requested: What do we do?
       set end_of_cycle = TRUE will issue the postponed transition
       keep end_of_cycle = FALSE will defer the transition to later */
    printf("%d postpone the transition for now!\n", gbl_cycle++); // increments gbl_cycle

    if (gbl_cycle >= 1) 
      end_of_cycle = TRUE;
  }

  if (bk_size(pevent) > 8)
    return bk_size(pevent);
  else
   return 0;
}
/*-- Dummy Titan Event --------------------------------------------*/
INT read_titan_event(char *pevent, INT off)
{
  return 0;
}

/*-- Scaler event --------------------------------------------------*/
INT read_scaler_event(char *pevent, INT off)
{
  DWORD *pdata;

  /* init bank structure */
  bk_init(pevent);

  /* create SCLR bank */
  bk_create(pevent, "SCLR", TID_DWORD, &pdata);

  /* read scaler bank */
  bk_close(pevent, pdata);

  //   return bk_size(pevent);
  // Nothing to send for now
  return 0;
}


#ifdef PPG_CODE

INT tr_checkppg(INT run_number, char *error)
{
  struct stat stbuf;
  int status;
  time_t timbuf;
  char timbuf_ascii[30];
  int elapsed_time;


  ppg_error_flag=TRUE; // set flag in case of error; prevents ppg_load from running before run is aborted

 double time_taken;
 static struct timeval t3, t4;
 /*  see how long it takes */
 
 gettimeofday(&t3, NULL); // start timer

 printf("tr_checkppg: sending system command:\"%s\" \n",tri_cmd);
 status =  system(tri_cmd); // run tri_config to generate bytecode.dat

 gettimeofday(&t4, NULL);  // stop timer
 // compute and print the elapsed time in millisec
	      
 time_taken = (t4.tv_sec - t3.tv_sec) * 1000.0;      // sec to ms
 time_taken += (t4.tv_usec - t3.tv_usec) / 1000.0;   // us to ms
 printf("tr_checkppg: time taken for tri_config was  %f ms\n",time_taken);
 cm_msg(MINFO,"tr_checkppg","time for tri_config was  %f ms",time_taken);
	    

  stat (ppgfile,&stbuf);
  printf("tr_checkppg: file %s  size %d\n",ppgfile, (int)stbuf.st_size);


  if(stbuf.st_size <= 0)
    {
      cm_msg(MERROR,"tr_checkppg","PPG load file %s cannot be found; check tri_config status",ppgfile);
      return DB_FILE_ERROR ;
    }

  // TEMP for testing
  // return DB_FILE_ERROR ;

  strcpy(timbuf_ascii, (char *) (ctime(&stbuf.st_mtime)) );
  printf ("tr_checkppg: PPG loadfile last modified:  %s or (binary) %d \n",timbuf_ascii,(int)stbuf.st_mtime);
  cm_msg(MINFO,"tr_checkppg"," PPG loadfile last modified:  %s or (binary) %d",timbuf_ascii,(int)stbuf.st_mtime);

  // get present time
  time (&timbuf);

  strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) );
  printf ("tr_checkppg: Present time:  %s or (binary) %d \n",timbuf_ascii,(int)timbuf);
  cm_msg(MINFO,"tr_checkppg","Present time:  %s or (binary) %d",timbuf_ascii,(int)timbuf);

  elapsed_time= (int) ( timbuf - stbuf.st_mtime );

  printf("tr_precheck: time since ppg loadfile last updated: %d seconds \n",elapsed_time);
  cm_msg(MINFO,"tr_checkppg","time since ppg loadfile last updated: %d seconds",elapsed_time);

  if(elapsed_time > 5)
    {
      cm_msg(MERROR,"tr_checkppg","PPG load file %s is older than 5 seconds",ppgfile);
      return DB_FILE_ERROR ;
    }

  ppg_error_flag = FALSE;
  return SUCCESS;
}


INT get_titan_settings(void)
{
  INT status,size;
  char set_str[128];

  /* Book Setting space */ 

  //  TITAN_ACQ_SETTINGS_STR(titan_acq_settings_str); 

 
  /* Map /equipment/Titan_acq/settings for the sequencer */ 
  sprintf(set_str, "/Equipment/TITAN_ACQ/Settings"); 
  status = db_create_record(hDB, 0, set_str, strcomb(titan_acq_settings_str)); 
if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_titan_settings","Could not create %s record (%d)",set_str,status);
	  return status;
	}
      else
	printf("Success from create record for %s\n",set_str);

  status = db_find_key (hDB, 0, set_str, &hTASet); 
  if (status != DB_SUCCESS)
    { 
      cm_msg(MERROR,"get_titan_settings","Key %s not found", set_str); 
      return status;
    }
  else 
    printf("Key hTASet for %s found\n",set_str);

  /* check that the record size is as expected */
  status = db_get_record_size(hDB, hTASet, 0, &size);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "get_titan_settings", "error during get_record_size (%d)",status);
      return status;
    }
  
  printf("Size of titan_acq/settings saved structure: %d, size of titan_acq/settings record: %d\n", 
	 sizeof(TITAN_ACQ_SETTINGS) ,size);
  if (sizeof(TITAN_ACQ_SETTINGS) != size)
    {
      /* create record */
      cm_msg(MINFO,"frontend_init",
	     "creating record for %s; mismatch between size of structure (%d) & record size (%d)", 
	     set_str, sizeof(TITAN_ACQ_SETTINGS) ,size);
      status = db_create_record(hDB, 0, set_str, strcomb(titan_acq_settings_str)); 
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_titan_settings","Could not create %s record (%d)",set_str,status);
	  return status;
	}
      else
	printf("Success from create record for %s\n",set_str);
    }

  /* Get current  settings */
  status = 0;
  size = sizeof(TITAN_ACQ_SETTINGS);
  status = db_get_record(hDB, hTASet, &tas, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "get_titan_settings", "cannot retrieve %s record (structure size=%d; size of record=%d ) %d", 
	     set_str, sizeof(TITAN_ACQ_SETTINGS),size,status);
      return status;
    }
  printf("db_get_record status:%d\n", status);
  printf("PPGpath = %s \n", tas.ppg.input.ppg_path);  
  printf("PPGLOAD path = %s \n", tas.ppg.input.ppgload_path);

  // Assign the name of the load file
#ifdef PPG_CODE
#ifdef NEW_PPG
  sprintf(ppgfile,"%s/ppgload.dat", tas.ppg.input.ppgload_path);
#else 
  sprintf(ppgfile,"%s/bytecode.dat", tas.ppg.input.ppgload_path); 
#endif
  printf("ppgfile:%s\n",ppgfile); 
#endif

  
   /* note that tri_config will produce bytecode.dat for old PPG
     for new ppg, tri_config will also convert bytecode.dat to ppgload.dat
  */
 

  /* find key for ODB output directory */
  sprintf(set_str,"ppg/output"); 
  status = db_find_key (hDB, hTASet, set_str, &hOut); 
  if (status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"get_titan_settings","cannot get key at %s (%d)",set_str,status); 
      return FE_ERR_ODB; 
    }
  printf("got the key hOut\n");


  printf("PPGpath:%s  Exp name = %s \n", tas.ppg.input.ppg_path, exp_name); 

  sprintf(tri_cmd,"%s/tri_config  -e %s -s ", tas.ppg.input.ppg_path,exp_name); 
#ifdef PLOT
  strcat(tri_cmd, " -p");  // add -p for plot
#endif
  strcat(tri_cmd, " >/dev/null"); // send output to /dev/null to speed up tri_config
  printf("get_titan_settings: tri_config cmd:%s\n",tri_cmd); 
 
  return SUCCESS;
}



INT tr_poststart(INT run_number, char *error)
{
  INT status;
  char copy_cmd[132];

#ifdef PPG_CODE

  if(run_state == STATE_RUNNING)
    {
#ifdef PLOT
      // cm_msg(MINFO,"tr_poststart","plotting PPG data");
      //printf("tr_poststart: sending system command:\"%s\" \n",plot_cmd);
      //status =  system(plot_cmd);
      //printf("tr_poststart: status after system command=%d\n",status);
#endif

#ifdef NEW_PPG 
      sprintf(copy_cmd,"cp %s/ppgload/ppgload.dat %s/info/run%d_ppgload.dat &", 
	      tas.ppg.input.ppg_path,data_dir,run_number); 
      status =  system(copy_cmd); 
      printf("tr_poststart: status after system command \"%s\" is %d\n",copy_cmd, status); 
#else
      sprintf(copy_cmd,"cp %s/ppgload/bytecode.dat /home/mpet/online/tmp/run%d_bytecode.dat &",
        PPGpath,run_number);
      status =  system(copy_cmd);
      printf("tr_poststart: status after system command \"%s\" is %d\n",copy_cmd, status);

      sprintf(copy_cmd,"cp %s/ppgload/mpet.ppg /home/mpet/online/tmp/run%d_mpet.ppg &",PPGpath,run_number);
      status =  system(copy_cmd);
      printf("tr_poststart: status after system command \"%s\" is %d\n",copy_cmd, status);
#endif
    }
#endif
  return SUCCESS;
}



#endif

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */


