/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Tue Oct  2 16:23:23 2007

\********************************************************************/

#define EXP_EDIT_DEFINED

typedef struct {
  INT       loop_count;
  BOOL      pedestals_run;
  BOOL      write_data;
  double    time_offset__ms_;
  double    time_offset__ms_;
  double    startfreq__mhz_;
  double    endfreq__mhz_;
  INT       nfreq;
  INT       ndwell;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) char *_name[] = {\
"[.]",\
"num ppg cycles = LINK : [53] /Equipment/TITAN_acq/ppg cycle/begin_scan/loop count",\
"Pedestals run = BOOL : n",\
"Write Data = LINK : [19] /Logger/Write data",\
"Capture delay (ms) = LINK : [56] /Equipment/TITAN_acq/ppg cycle/evset_2/time offset (ms)",\
"PLT pulsedown delay (ms) = LINK : [56] /Equipment/TITAN_acq/ppg cycle/pulse_1/time offset (ms)",\
"Start Frequency in MHz = LINK : [38] /Experiment/Variables/StartFreq (MHz)",\
"End Frequency in MHz = LINK : [36] /Experiment/Variables/EndFreq (MHz)",\
"Number of frequency steps = LINK : [28] /Experiment/Variables/NFreq",\
"Cycles to dwell on a frequency = LINK : [29] /Experiment/Variables/NDwell",\
"",\
NULL }

#ifndef EXCL_TRIGGER

#define TRIGGER_SETTINGS_DEFINED

typedef struct {
  BOOL      vt2_keep_alive;
  BOOL      vt2_cycle_reset;
} TRIGGER_SETTINGS;

#define TRIGGER_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"vt2 keep alive = BOOL : n",\
"vt2 cycle reset = BOOL : y",\
"",\
NULL }

#define TRIGGER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 1233",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fempet",\
"Frontend file name = STRING : [256] fempet.c",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fempet",\
"Frontend file name = STRING : [256] fempet.c",\
"",\
NULL }

#endif

#ifndef EXCL_BEAMLINE

#define BEAMLINE_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      channel_name[34][32];
    } beamline;
  } devices;
  char      names[34][32];
  float     update_threshold_measured[34];
} BEAMLINE_SETTINGS;

#define BEAMLINE_SETTINGS_STR(_name) char *_name[] = {\
"[Devices/Beamline]",\
"Channel name = STRING[34] :",\
"[32] TITAN:VAR1",\
"[32] TITAN:VAR2",\
"[32] TITAN:VAR3",\
"[32] TITAN:VAR4",\
"[32] TITAN:VAR5",\
"[32] TITAN:VAR6",\
"[32] TITAN:VAR7",\
"[32] TITAN:VAR8",\
"[32] TITAN:VAR9",\
"[32] TITAN:VAR10",\
"[32] TITAN:VAR11",\
"[32] TITAN:VAR12",\
"[32] TITAN:VAR13",\
"[32] TITAN:VAR14",\
"[32] TITAN:VAR15",\
"[32] TITAN:VAR16",\
"[32] TITAN:VAR17",\
"[32] TITAN:VAR18",\
"[32] TITAN:VAR19",\
"[32] TITAN:VAR20",\
"[32] MPET:XZT3:VOL",\
"[32] MPET:XZT2:VOL",\
"[32] MPET:XZT1:VOL",\
"[32] MPET:XDC:VOL",\
"[32] MPET:XDT:VOL",\
"[32] MPET:PLT:VOL",\
"[32] MPET:LSYN:VOL",\
"[32] MPET:LSYP:VOL",\
"[32] MPET:LSXN:VOL",\
"[32] MPET:LSXP:VOL",\
"[32] MPET:NDC:VOL",\
"[32] MPET:NDT:VOL",\
"[32] MPET:CATHB:VOL",\
"[32] MPET:CATHI:VOL",\
"",\
"[.]",\
"Names = STRING[34] :",\
"[32] Published%VAR1",\
"[32] Published%VAR2",\
"[32] Published%VAR3",\
"[32] Published%VAR4",\
"[32] Published%VAR5",\
"[32] Published%VAR6",\
"[32] Published%VAR7",\
"[32] Published%VAR8",\
"[32] Published%VAR9",\
"[32] Published%VAR10",\
"[32] Published%VAR11",\
"[32] Published%VAR12",\
"[32] Published%VAR13",\
"[32] Published%VAR14",\
"[32] Published%VAR15",\
"[32] Published%VAR16",\
"[32] Published%VAR17",\
"[32] Published%VAR18",\
"[32] Published%VAR19",\
"[32] Watchdog",\
"[32] MPET%Extr_einZel_3",\
"[32] MPET%Extr_einZel_2",\
"[32] MPET%Extr_einZel_1",\
"[32] MPET%Extr_cone",\
"[32] MPET%Extr_tube",\
"[32] MPET%Pulsed_Tube",\
"[32] MPET%Lorentz_YNeg",\
"[32] MPET%Lorentz_YPos",\
"[32] MPET%Lorentz_XNeg",\
"[32] MPET%Lorentz_XPos",\
"[32] MPET%eNtrance_Tube",\
"[32] MPET%eNtrance_Cone",\
"[32] Mpet_Beamline%CH 32",\
"[32] Mpet_Beamline%CH 33",\
"Update Threshold Measured = FLOAT[34] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"[16] 1",\
"[17] 1",\
"[18] 1",\
"[19] 1",\
"[20] 1",\
"[21] 1",\
"[22] 1",\
"[23] 1",\
"[24] 1",\
"[25] 1",\
"[26] 1",\
"[27] 1",\
"[28] 1",\
"[29] 1",\
"[30] 1",\
"[31] 1",\
"[32] 1",\
"[33] 1",\
"",\
NULL }

#define BEAMLINE_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} BEAMLINE_COMMON;

#define BEAMLINE_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 11",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 121",\
"Period = INT : 2000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] titan01.triumf.ca",\
"Frontend name = STRING : [32] scEpics",\
"Frontend file name = STRING : [256] scepics.c",\
"",\
NULL }

#endif

#ifndef EXCL_SLOWDAC

#define SLOWDAC_SETTINGS_DEFINED

typedef struct {
  char      names[8][32];
} SLOWDAC_SETTINGS;

#define SLOWDAC_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Names = STRING[8] :",\
"[32] Sdac channel 1",\
"[32] Sdac channel 2",\
"[32] Sdac channel 3",\
"[32] Sdac channel 4",\
"[32] Sdac channel 5",\
"[32] Sdac channel 6",\
"[32] Sdac channel 7",\
"[32] Sdac channel 8",\
"",\
NULL }

#define SLOWDAC_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SLOWDAC_COMMON;

#define SLOWDAC_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 13",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 511",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 10",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fesdac",\
"Frontend file name = STRING : [256] fesdac.c",\
"",\
NULL }

#endif

#ifndef EXCL_RF

#define RF_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} RF_COMMON;

#define RF_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 12",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 33",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 511",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] feRf",\
"Frontend file name = STRING : [256] ferf.c",\
"",\
NULL }

#define RF_SETTINGS_DEFINED

typedef struct {
  char      names[5][32];
  struct {
    char      mscb_device[32];
    char      mscb_pwd[32];
    INT       base_address;
  } dd;
} RF_SETTINGS;

#define RF_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Names = STRING[5] :",\
"[32] RF_1 monitor (V)",\
"[32] RF_2 monitor (V)",\
"[32] RF_3 monitor (V)",\
"[32] RF_4 monitor (V)",\
"[32] uC Temperature (C)",\
"",\
"[DD]",\
"MSCB Device = STRING : [32] usb0",\
"MSCB Pwd = STRING : [32] ",\
"Base Address = INT : 65280",\
"",\
NULL }

#endif

#ifndef EXCL_TITAN_ACQ

#define TITAN_ACQ_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      experiment_name[32];
      char      ppgload_path[50];
      char      ppg_path[50];
      char      ppg_perl_path[50];
      float     daq_service_time__ms_;
      float     standard_pulse_width__ms_;
      float     awg_clock_width__ms_;
      float     tdcblock_width__ms_;
      float     ppg_clock__mhz_;
    } input;
    struct {
      char      compiled_file_time[32];
      DWORD     compiled_file_time__binary_;
      float     time_slice__ms_;
      float     ppg_nominal_frequency__mhz_;
      float     minimal_delay__ms_;
      float     ppg_freq_conversion_factor;
    } output;
  } ppg;
  struct {
    DWORD     number_of_steps;
    double    step_delay__ms_;
    double    vset__volts_[8];
    double    vend__volts_[8];
    INT       nsteps_before_ramp[8];
    INT       nsteps_after_ramp[8];
  } awg0;
  struct {
    DWORD     number_of_steps;
    double    step_delay__ms_;
    double    vset__volts_[8];
    double    vend__volts_[8];
    INT       nsteps_before_ramp[8];
    INT       nsteps_after_ramp[8];
  } awg1;
} TITAN_ACQ_SETTINGS;

#define TITAN_ACQ_SETTINGS_STR(_name) char *_name[] = {\
"[ppg/input]",\
"Experiment name = STRING : [32] mpet",\
"PPGLOAD path = STRING : [50] /home/mpet/online/ppg/ppgload",\
"PPG path = STRING : [50] /home/mpet/online/ppg",\
"PPG perl path = STRING : [50] /home/mpet/online/ppg/perl",\
"DAQ service time (ms) = FLOAT : 0",\
"standard pulse width (ms) = FLOAT : 0.001",\
"AWG clock width (ms) = FLOAT : 0.001",\
"TDCBLOCK width (ms) = FLOAT : 0.001",\
"PPG clock (MHz) = FLOAT : 100",\
"",\
"[ppg/output]",\
"compiled file time = STRING : [32] Mon Oct  1 18:05:34 2007",\
"compiled file time (binary) = DWORD : 1191287134",\
"Time Slice (ms) = FLOAT : 1e-05",\
"PPG nominal frequency (MHz) = FLOAT : 10",\
"Minimal Delay (ms) = FLOAT : 5e-05",\
"PPG freq conversion factor = FLOAT : 10",\
"",\
"[awg0]",\
"number of steps = DWORD : 5",\
"step delay (ms) = DOUBLE : 125",\
"Vset (volts) = DOUBLE[8] :",\
"[0] 4",\
"[1] 4",\
"[2] 0",\
"[3] -4",\
"[4] -4",\
"[5] 0",\
"[6] 0",\
"[7] -0.5",\
"Vend (volts) = DOUBLE[8] :",\
"[0] 3.274",\
"[1] 4",\
"[2] -3.12",\
"[3] 4",\
"[4] 3.274",\
"[5] 0",\
"[6] 0",\
"[7] -0.5",\
"Nsteps before ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 1",\
"[6] 2",\
"[7] 3",\
"Nsteps after ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 3",\
"[6] 2",\
"[7] 1",\
"",\
"[awg1]",\
"number of steps = DWORD : 1",\
"step delay (ms) = DOUBLE : 100",\
"Vset (volts) = DOUBLE[8] :",\
"[0] 1",\
"[1] 1",\
"[2] -0.77",\
"[3] 1",\
"[4] 1",\
"[5] 0",\
"[6] 0",\
"[7] -0.5",\
"Vend (volts) = DOUBLE[8] :",\
"[0] 2",\
"[1] 2",\
"[2] 2",\
"[3] 2",\
"[4] 2",\
"[5] 1",\
"[6] 1",\
"[7] -0.5",\
"Nsteps before ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"Nsteps after ramp = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"",\
NULL }

#define TITAN_ACQ_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TITAN_ACQ_COMMON;

#define TITAN_ACQ_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 10",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] fempet",\
"Frontend file name = STRING : [256] fempet.c",\
"",\
NULL }

#define TITAN_ACQ_CYCLE1_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
  } stdpulse_rftrig;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_trap;
  struct {
    struct {
      INT       awg_unit;
      char      set_values__volts_[128];
      double    time_offset__ms_;
    } evset_starteg;
    struct {
      double    time_offset__ms_;
      INT       awg_unit;
      char      set_values__volts_[128];
    } evset_stopeg;
  } loadtrapeg;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_openrfgate1;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_closerfgate1;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_starttdc0;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_dump;
  struct {
    struct {
      double    time_offset__ms_;
      char      time_reference[20];
      INT       loop_count;
    } begin_evloop;
    struct {
      double    time_offset__ms_;
      char      time_reference[20];
      INT       awg_unit;
      char      start_value__volts_[128];
      char      end_value__volts_[128];
      char      nsteps_before_ramp[128];
      char      nsteps_after_ramp[128];
    } evstep;
    struct {
      double    time_offset__ms_;
      char      time_reference[20];
    } end_evloop;
    struct {
      double    time_offset__ms_;
      INT       awg_unit;
      char      set_values__volts_[128];
      char      time_reference[20];
    } evset_dummy;
  } evloop;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_endtdc0;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_awg1;
  struct {
    struct {
      double    time_offset__ms_;
      char      time_reference[20];
      INT       loop_count;
    } begin_evloop_da;
    struct {
      double    time_offset__ms_;
      char      time_reference[20];
      INT       awg_unit;
      char      start_value__volts_[128];
      char      end_value__volts_[128];
      char      nsteps_before_ramp[128];
      char      nsteps_after_ramp[128];
    } evstep_da;
    struct {
      double    time_offset__ms_;
      char      time_reference[20];
    } end_evloop_da;
    struct {
      double    time_offset__ms_;
      INT       awg_unit;
      char      set_values__volts_[128];
      char      time_reference[20];
    } evset_da_try;
  } evloop_awg1;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_awg1;
  struct {
    double    time_offset__ms_;
  } end_awg1;
  struct {
    double    time_offset__ms_;
  } end_scan;
} TITAN_ACQ_CYCLE1;

#define TITAN_ACQ_CYCLE1_STR(_name) char *_name[] = {\
"[stdpulse_RFTRIG]",\
"time offset (ms) = DOUBLE : 0.002",\
"ppg signal name = STRING : [16] RFTRIG1",\
"",\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0.001",\
"loop count = INT : 100",\
"",\
"[evset_Trap]",\
"time offset (ms) = DOUBLE : 1",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,1) (1,1) (2,0) (3,1) (4,1) (7,-8)",\
"",\
"[LoadTrapEG/evset_startEG]",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (7,1)",\
"time offset (ms) = DOUBLE : 1",\
"",\
"[LoadTrapEG/evset_stopEG]",\
"time offset (ms) = DOUBLE : 500",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (7,-8)",\
"",\
"[transition_openRFGATE1]",\
"time offset (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [15] RFGATE1",\
"",\
"[transition_closeRFGATE1]",\
"ppg signal name = STRING : [15] RFGATE1",\
"time offset (ms) = DOUBLE : 2000",\
"",\
"[transition_startTDC0]",\
"time offset (ms) = DOUBLE : 2000",\
"ppg signal name = STRING : [15] TDCGATE",\
"",\
"[evset_Dump]",\
"time offset (ms) = DOUBLE : 0",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,1) (1,1) (2,0) (3,-1) (4,-1) (7,-8)",\
"",\
"[evloop/begin_evloop]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] ",\
"loop count = INT : 8",\
"",\
"[evloop/evstep]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEVLOOP",\
"AWG unit = INT : 0",\
"start value (Volts) = STRING : [128] (3,4.5),(4,-2)",\
"end value (Volts) = STRING : [128] (0,1.2) (2,7.9) (7,-4.32)",\
"Nsteps before ramp = STRING : [128] (0,0) (1,1) (3,6)",\
"Nsteps after ramp = STRING : [128] (1,3) (0,0)",\
"",\
"[evloop/end_evloop]",\
"time offset (ms) = DOUBLE : 100",\
"time reference = STRING : [20] ",\
"",\
"[evloop/evset_dummy]",\
"time offset (ms) = DOUBLE : 10",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,1) (1,1) (2,0) (3,-1) (4,-1) (7,-8)",\
"time reference = STRING : [20] _TENDEVLOOP",\
"",\
"[transition_endTDC0]",\
"ppg signal name = STRING : [15] TDCGATE",\
"time offset (ms) = DOUBLE : 2",\
"",\
"[begin_awg1]",\
"time offset (ms) = DOUBLE : 5",\
"loop count = INT : 100",\
"",\
"[evloop_awg1/begin_evloop_da]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] ",\
"loop count = INT : 15",\
"",\
"[evloop_awg1/evstep_da]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGEVLOOP_DA",\
"AWG unit = INT : 1",\
"start value (Volts) = STRING : [128] (3,2),(4,3)",\
"end value (Volts) = STRING : [128] (0,-5)",\
"Nsteps before ramp = STRING : [128] (1,2)",\
"Nsteps after ramp = STRING : [128] (1,3)",\
"",\
"[evloop_awg1/end_evloop_da]",\
"time offset (ms) = DOUBLE : 100",\
"time reference = STRING : [20] ",\
"",\
"[evloop_awg1/evset_da_try]",\
"time offset (ms) = DOUBLE : 10",\
"AWG unit = INT : 1",\
"Set Values (Volts) = STRING : [128] (0,-1) (1,-1) (2,0) (3,1) (4,1) (7,8)",\
"time reference = STRING : [20] _TENDEVLOOP_DA",\
"",\
"[evset_awg1]",\
"time offset (ms) = DOUBLE : 1",\
"AWG unit = INT : 1",\
"Set Values (Volts) = STRING : [128] (0,-1) (1,-1) (2,1) (3,-1) (4,-1),(7,8)",\
"",\
"[end_awg1]",\
"time offset (ms) = DOUBLE : 200",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 497",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_hv;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
  } stdpulse_3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[16];
  } stdpulse;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
    char      time_reference[20];
  } evset_2;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_4;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_unit1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_l2;
  struct {
    INT       awg_unit;
    char      end_value__volts_[128];
    double    time_offset__ms_;
  } evstep_2;
  struct {
    INT       awg_unit;
    char      end_value__volts_[128];
    double    time_offset__ms_;
    char      time_reference[20];
  } evstep_unit1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_l2;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[16];
  } stdpulse_2;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_3;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
    char      time_reference[20];
  } transition_4;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_5;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_6;
  struct {
    double    time_offset__ms_;
    char      ppg_signal_name[15];
  } transition_1;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_5;
  struct {
    char      ppg_signal_name[15];
    double    time_offset__ms_;
  } transition_2;
  struct {
    char      define_reference[20];
    double    time_offset__ms_;
    char      time_reference[20];
  } time_1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    char      dummy[32];
  } skip;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_l1;
  struct {
    INT       awg_unit;
    char      end_value__volts_[128];
    double    time_offset__ms_;
    char      start_value__volts_[128];
  } evstep_1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_l1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_1;
  struct {
    double    time_offset__ms_;
    INT       awg_unit;
    char      set_values__volts_[128];
  } evset_3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_lsqueeze;
  struct {
    INT       awg_unit;
    char      end_value__volts_[128];
    double    time_offset__ms_;
    char      start_value__volts_[128];
  } evstep_squeeze;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_lsqueeze;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse_tisgate;
} TITAN_ACQ_PPG_CYCLE;

#define TITAN_ACQ_PPG_CYCLE_STR(_name) char *_name[] = {\
"[transition_HV]",\
"time offset (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [15] HV",\
"",\
"[stdpulse_3]",\
"time offset (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] RFTRIG2",\
"",\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 1025",\
"",\
"[stdpulse]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"ppg signal name = STRING : [16] tdcBlock",\
"",\
"[evset_1]",\
"time offset (ms) = DOUBLE : 0",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,-1) (1,-1) (2,-0.78) (3,1) (4,1) (6,0) (7,5)",\
"",\
"[pulse]",\
"time offset (ms) = DOUBLE : 0.058",\
"time reference = STRING : [20] _TSTART_stdpulse",\
"pulse width (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] HV",\
"",\
"[evset_2]",\
"time offset (ms) = DOUBLE : 0.06",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,1) (1,1) (2,-0.78) (3,1) (4,1) (6,0) (7,5)",\
"time reference = STRING : [20] _TSTART_pulse_1",\
"",\
"[evset_4]",\
"time offset (ms) = DOUBLE : 0.1",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,8.4) (1,8.4) (2,8) (3,8.4) (4,8.4) (7,-0.5)",\
"",\
"[evset_unit1]",\
"time offset (ms) = DOUBLE : 0",\
"AWG unit = INT : 1",\
"Set Values (Volts) = STRING : [128] (0,1) (1,1) (2,-0.77) (3,-2) (4,-2) (7,-0.5)",\
"",\
"[begin_l2]",\
"time offset (ms) = DOUBLE : 0.01",\
"loop count = INT : 8",\
"",\
"[evstep_2]",\
"AWG unit = INT : 0",\
"end value (Volts) = STRING : [128] (0,1) (1,1) (2,-0.77) (3,1) (4,1) (7,-0.5)",\
"time offset (ms) = DOUBLE : 0.01",\
"",\
"[evstep_unit1]",\
"AWG unit = INT : 1",\
"end value (Volts) = STRING : [128] (0,1) (1,1) (2,-0.77) (3,1) (4,1) (7,-0.5)",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] _TBEGL2",\
"",\
"[end_l2]",\
"time offset (ms) = DOUBLE : 0.02",\
"time reference = STRING : [20] _TBEGl2",\
"",\
"[stdpulse_2]",\
"time offset (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [16] RFGATE4",\
"",\
"[transition_3]",\
"time offset (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [15] RFGATE1",\
"",\
"[transition_4]",\
"ppg signal name = STRING : [15] RFGATE1",\
"time offset (ms) = DOUBLE : 499.9999",\
"time reference = STRING : [20] _TBEGScan",\
"",\
"[transition_5]",\
"time offset (ms) = DOUBLE : 0.1",\
"ppg signal name = STRING : [15] RFGATE2",\
"",\
"[transition_6]",\
"ppg signal name = STRING : [15] RFGATE2",\
"time offset (ms) = DOUBLE : 400",\
"",\
"[transition_1]",\
"time offset (ms) = DOUBLE : 0.005",\
"ppg signal name = STRING : [15] TDCGATE",\
"",\
"[evset_5]",\
"time offset (ms) = DOUBLE : 0.5",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,4) (1,4) (2,0) (3,-4) (4,-4) (6,0) (7,-0.5)",\
"",\
"[transition_2]",\
"ppg signal name = STRING : [15] TDCGATE",\
"time offset (ms) = DOUBLE : 0.1",\
"",\
"[time_1]",\
"define reference = STRING : [20] t1",\
"time offset (ms) = DOUBLE : 100",\
"time reference = STRING : [20] _TBEGScan",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 500.005",\
"time reference = STRING : [20] _TBEGScan",\
"",\
"[skip]",\
"dummy = STRING : [32] ",\
"",\
"[begin_l1]",\
"time offset (ms) = DOUBLE : 0.001",\
"loop count = INT : 1000",\
"",\
"[evstep_1]",\
"AWG unit = INT : 0",\
"end value (Volts) = STRING : [128] (0,0.5) (1,0.5) (2,-0.39) (3,-0.5) (4,-0.5)(7,-0.5)",\
"time offset (ms) = DOUBLE : 0.005",\
"start value (Volts) = STRING : [128] (0,0.5) (1,0.5) (2,-0.39) (3,0.5) (4,0.5) (7,-0.5)",\
"",\
"[end_l1]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] _TBEGl1",\
"",\
"[pulse_1]",\
"time offset (ms) = DOUBLE : 0.055",\
"time reference = STRING : [20] _TSTART_stdpulse",\
"pulse width (ms) = DOUBLE : 0.2",\
"ppg signal name = STRING : [16] HV",\
"",\
"[evset_3]",\
"time offset (ms) = DOUBLE : 0.2",\
"AWG unit = INT : 0",\
"Set Values (Volts) = STRING : [128] (0,1) (1,1) (2,-0.78) (3,1) (4,1) (6,0) (7,-0.5)",\
"",\
"[begin_lsqueeze]",\
"time offset (ms) = DOUBLE : 0.001",\
"loop count = INT : 100",\
"",\
"[evstep_squeeze]",\
"AWG unit = INT : 0",\
"end value (Volts) = STRING : [128] (0,3.274) (1,4) (2,-3.12) (3,4) (4,3.274) (6,0) (7,-0.5)",\
"time offset (ms) = DOUBLE : 0.01",\
"start value (Volts) = STRING : [128] (0,0.02) (1,0.02) (2,-0.02) (3,0.02) (4,0.02) (6,0) (7,-0.5)",\
"",\
"[end_lsqueeze]",\
"time offset (ms) = DOUBLE : 0.02",\
"time reference = STRING : [20] _TBEGlsqueeze",\
"",\
"[pulse_TISGATE]",\
"time offset (ms) = DOUBLE : 498",\
"time reference = STRING : [20] _TBEGScan",\
"pulse width (ms) = DOUBLE : 0.5",\
"ppg signal name = STRING : [16] RFTRIG4",\
"",\
NULL }

#endif

#ifndef EXCL_RFSOURCE

#define RFSOURCE_SETTINGS_DEFINED

typedef struct {
  BOOL      modulation_toggle;
  float     rf_frequency;
  float     modulated_freqmin;
  float     modulated_freqmax;
  INT       runs_per_freq_set;
} RFSOURCE_SETTINGS;

#define RFSOURCE_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Modulation Toggle = BOOL : n",\
"RF Frequency = FLOAT : 0",\
"Modulated FreqMin = FLOAT : 0",\
"Modulated FreqMax = FLOAT : 0",\
"runs per freq set = INT : 0",\
"",\
NULL }

#define RFSOURCE_COMMON_DEFINED

typedef struct {
  INT       log_history;
  INT       type;
  char      frontend_name[256];
  WORD      event_id;
  WORD      trigger_mask;
  char      format[80];
} RFSOURCE_COMMON;

#define RFSOURCE_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Log history = INT : 0",\
"Type = INT : 1",\
"Frontend name = STRING : [256] fempet",\
"event ID = WORD : 0",\
"Trigger mask = WORD : 0",\
"Format = STRING : [80] MIDAS",\
"",\
NULL }

#endif

#ifndef EXCL_TITAN_RF

#define RF2_BANK_DEFINED

typedef struct {
  double    controlmng;
  struct {
    double    ssweepdur;
    double    sstartfreq;
    double    sendfreq;
    double    srfamplitude;
  } sweepmode;
  struct {
    double    brfamplitude;
    double    bburstfreq;
    double    bnumcycles;
  } burstmode;
  struct {
    double    lbnumsteps;
    double    lbnumcycles;
    double    lbrfamplitude;
    double    lbburstfreq;
  } laddemode;
  struct {
    double    fmsweep;
    double    fmstartfreq;
    double    fmendfreq;
    double    fmrfamplitude;
  } fmmode;
} RF2_BANK;

#define RF2_BANK_STR(_name) char *_name[] = {\
"[.]",\
"controlMng = DOUBLE : 2",\
"",\
"[SweepMode]",\
"ssweepDur = DOUBLE : 0",\
"sstartFreq = DOUBLE : 345",\
"sendFreq = DOUBLE : 34647",\
"srfAmplitude = DOUBLE : 55",\
"",\
"[BurstMode]",\
"brfAmplitude = DOUBLE : 87",\
"bburstFreq = DOUBLE : 5",\
"bnumCycles = DOUBLE : 45",\
"",\
"[LaddeMode]",\
"lbnumSteps = DOUBLE : 74",\
"lbnumCycles = DOUBLE : 7",\
"lbrfAmplitude = DOUBLE : 2",\
"lbburstFreq = DOUBLE : 1234",\
"",\
"[FMMode]",\
"fmSweep = DOUBLE : 0",\
"fmstartFreq = DOUBLE : 1",\
"fmendFreq = DOUBLE : 4",\
"fmrfAmplitude = DOUBLE : 67",\
"",\
NULL }

#define RF1_BANK_DEFINED

typedef struct {
  double    controlmng;
  struct {
    double    ssweepdur;
    double    sstartfreq;
    double    sendfreq;
    double    srfamplitude;
  } sweepmode;
  struct {
    double    brfamplitude;
    double    bburstfreq;
    double    bnumcycles;
  } burstmode;
  struct {
    double    lbnumsteps;
    double    lbnumcycles;
    double    lbrfamplitude;
    double    lbburstfreq;
  } laddemode;
  struct {
    double    fmsweep;
    double    fmstartfreq;
    double    fmendfreq;
    double    fmrfamplitude;
  } fmmode;
} RF1_BANK;

#define RF1_BANK_STR(_name) char *_name[] = {\
"[.]",\
"controlMng = DOUBLE : 2",\
"",\
"[SweepMode]",\
"ssweepDur = DOUBLE : 0",\
"sstartFreq = DOUBLE : 345",\
"sendFreq = DOUBLE : 34647",\
"srfAmplitude = DOUBLE : 55",\
"",\
"[BurstMode]",\
"brfAmplitude = DOUBLE : 87",\
"bburstFreq = DOUBLE : 5",\
"bnumCycles = DOUBLE : 45",\
"",\
"[LaddeMode]",\
"lbnumSteps = DOUBLE : 74",\
"lbnumCycles = DOUBLE : 7",\
"lbrfAmplitude = DOUBLE : 2",\
"lbburstFreq = DOUBLE : 1234",\
"",\
"[FMMode]",\
"fmSweep = DOUBLE : 0",\
"fmstartFreq = DOUBLE : 1",\
"fmendFreq = DOUBLE : 4",\
"fmrfAmplitude = DOUBLE : 67",\
"",\
NULL }

#define TITAN_RF_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TITAN_RF_COMMON;

#define TITAN_RF_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 255",\
"Period = INT : 2000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] lxmpet.triumf.ca",\
"Frontend name = STRING : [32] TITAN_RF",\
"Frontend file name = STRING : [256] frontendrf.c",\
"",\
NULL }

#define TITAN_RF_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      struct {
        char      mscb_device[32];
        char      mscb_pwd[32];
        INT       base_address;
      } dd;
    } titan_rf;
  } devices;
  char      names[32];
  struct {
    double    controlmng;
    struct {
      double    ssweepdur;
      double    sstartfreq;
      double    sendfreq;
      double    srfamplitude;
    } sweepmode;
    struct {
      double    brfamplitude;
      double    bburstfreq;
      double    bnumcycles;
    } burstmode;
    struct {
      double    lbnumsteps;
      double    lbnumcycles;
      double    lbrfamplitude;
      double    lbburstfreq;
    } laddemode;
    struct {
      double    fmsweep;
      double    fmstartfreq;
      double    fmendfreq;
      double    fmrfamplitude;
    } fmmode;
  } rf1;
  struct {
    double    controlmng;
    struct {
      double    ssweepdur;
      double    sstartfreq;
      double    sendfreq;
      double    srfamplitude;
    } sweepmode;
    struct {
      double    brfamplitude;
      double    bburstfreq;
      double    bnumcycles;
    } burstmode;
    struct {
      double    lbnumsteps;
      double    lbnumcycles;
      double    lbrfamplitude;
      double    lbburstfreq;
    } laddemode;
    struct {
      double    fmsweep;
      double    fmstartfreq;
      double    fmendfreq;
      double    fmrfamplitude;
    } fmmode;
  } rf2;
} TITAN_RF_SETTINGS;

#define TITAN_RF_SETTINGS_STR(_name) char *_name[] = {\
"[Devices/TITAN_RF/DD]",\
"MSCB Device = STRING : [32] usb0",\
"MSCB Pwd = STRING : [32] ",\
"Base Address = INT : 1",\
"",\
"[.]",\
"Names = STRING : [32] Default%CH 0",\
"",\
"[RF1]",\
"controlMng = DOUBLE : 2",\
"",\
"[RF1/SweepMode]",\
"ssweepDur = DOUBLE : 0",\
"sstartFreq = DOUBLE : 345",\
"sendFreq = DOUBLE : 34647",\
"srfAmplitude = DOUBLE : 55",\
"",\
"[RF1/BurstMode]",\
"brfAmplitude = DOUBLE : 87",\
"bburstFreq = DOUBLE : 5",\
"bnumCycles = DOUBLE : 45",\
"",\
"[RF1/LaddeMode]",\
"lbnumSteps = DOUBLE : 74",\
"lbnumCycles = DOUBLE : 7",\
"lbrfAmplitude = DOUBLE : 2",\
"lbburstFreq = DOUBLE : 1234",\
"",\
"[RF1/FMMode]",\
"fmSweep = DOUBLE : 0",\
"fmstartFreq = DOUBLE : 1",\
"fmendFreq = DOUBLE : 4",\
"fmrfAmplitude = DOUBLE : 67",\
"",\
"[RF2]",\
"controlMng = DOUBLE : 3",\
"",\
"[RF2/SweepMode]",\
"ssweepDur = DOUBLE : 0",\
"sstartFreq = DOUBLE : 345",\
"sendFreq = DOUBLE : 34647",\
"srfAmplitude = DOUBLE : 55",\
"",\
"[RF2/BurstMode]",\
"brfAmplitude = DOUBLE : 87",\
"bburstFreq = DOUBLE : 5",\
"bnumCycles = DOUBLE : 45",\
"",\
"[RF2/LaddeMode]",\
"lbnumSteps = DOUBLE : 74",\
"lbnumCycles = DOUBLE : 7",\
"lbrfAmplitude = DOUBLE : 2",\
"lbburstFreq = DOUBLE : 1234",\
"",\
"[RF2/FMMode]",\
"fmSweep = DOUBLE : 0",\
"fmstartFreq = DOUBLE : 1",\
"fmendFreq = DOUBLE : 4",\
"fmrfAmplitude = DOUBLE : 67",\
"",\
NULL }

#endif

