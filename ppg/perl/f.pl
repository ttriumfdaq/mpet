#!/usr/bin/perl -w
#
# Run this file e.g.
# /home/titan/online/titan/try.pl  nsample 1490.010000
#                             Input Parameters:
#                             
#                                             
# fplot.pl 
#         
#         number to plot
#         max time range (x axis)
use strict;
our ( $nsample, $max_time ) = @ARGV;# input parameters
our $nparam=2; # need 2 input params
my $file="/home/mpet/online/da816-ch0";
my @last;
my @volt;
my ($count,$chan,$filename);
$|=1; # flush output buffers
print   "gnuplot.pl  starting with parameters:  \n";
print    "  max_time = $max_time  number of samples=$nsample  \n";
print    "  filename = $file  \n";
$count = @ARGV;
print "num arguments = $count\n";
if($count != $nparam){ die "not enough arguments\n";}
$chan = 0;
while ($chan < 8)
{
  $filename= "$file$chan.log";
  print "filename: $filename\n";


  open FILE,$filename or die $!;
  while (<FILE>)
  {
    push @last, $_; # add to the end
    shift @last if @last > $nsample; # remove line from beginning
  }
  print "last $nsample lines:\n",@last;
  $count=0;
  foreach $_ (@last)
  {
    if(/Off:([\d]+).*Volt:([\d\-\.]+)/)
    {
	unless($count == $1) {die "incorrect sample number; got $1 expect $count";}
	print "sample=$1 Volt=$2\n";
	$volt[$chan]=$2;
	print "volt[$chan]=$volt[$chan]\n";
	$count++;
    }
   }
   $chan++;
}
