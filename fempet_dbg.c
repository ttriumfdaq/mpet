/********************************************************************\

  Name:         fempet.c
  Created by:   PAA

  Contents:     MCP, VT2

  $Id: fempet.c 83 2007-11-06 21:15:40Z midas $

\********************************************************************/
#define    VMEIO_CODE
#define      VT2_CODE
#define    DA816_CODE
#define      PPG_CODE
#define     VMCP_CODE
#define  SOFTDAC_CODE

#undef VF48_CODE
#undef VERBOSE

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h> // time
#include "midas.h"
#include "mvmestd.h"
#include "vmicvme.h"
#ifdef VMEIO_CODE
#include "vmeio.h"
#endif
#ifdef VF48_CODE
#include "vf48.h"
#endif
#ifdef VT2_CODE
#include "vt2.h"
#endif
#ifdef DA816_CODE
#include "da816.h"
ALPHIDA816 *da816=NULL;
#endif
#if defined SOFTDAC_CODE
#include "softdac.h"
ALPHISOFTDAC  *softdac = NULL;
char *SOFTDAC_RANGE[] = {"P5V", "P10V", "PM5V", "PM10V", "PM2P5V", "M2P5P7P5V", "off"};     
#endif

#ifdef VMCP_CODE
#include "lrs1190.h"
#endif
#ifdef PPG_CODE
#include "vppg.h"
#include "unistd.h" // for sleep
FILE *ppginput;
#endif

#include "experim.h"

/* Interrupt vector */
int trig_level =  0;
#define TRIG_LEVEL  (int) 1
#define INT_LEVEL   (int) 3
#define INT_VECTOR  (int) 0x16
extern INT_INFO int_info;
int myinfo = VME_INTERRUPT_SIGEVENT;
INT read_titan_event(char *pevent, INT off);

/* Globals */
extern INT run_state;
extern char host_name[HOST_NAME_LENGTH];
extern char exp_name[NAME_LENGTH];

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "fempet";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 000;

/* maximum event size produced by this frontend */
INT max_event_size = 60000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 10 * 20000;

/* Hardware */
MVME_INTERFACE *myvme;


/* VME base address */
DWORD VMEIO_BASE = 0x780000;
DWORD VT2_BASE   = 0xE00000;
DWORD VMCP_BASE  = 0x790000;
DWORD VF48_BASE  = 0xA00000;
DWORD PPG_BASE   = 0x008000;

/* Globals */
extern HNDLE hDB;
HNDLE hSet, hTASet;
TRIGGER_SETTINGS ts;
BOOL  end_of_cycle = FALSE;
BOOL  transition_PS_requested= FALSE;
INT   gbl_cycle;
BOOL   debug=0;

#ifdef PPG_CODE
  // PPG
char cmd[128];
char ppgfile[128];
BOOL ppg_running;
char PPGpath[80];
char plot_cmd[132];
char copy_cmd[132];
#endif

/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
extern void interrupt_routine(void);

INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);

#ifdef PPG_CODE
  INT ppg_load(char *ppgfile);
  INT tr_checkppg(INT run_number, char *error);
  INT tr_poststart(INT run_number, char *error);
#endif

BANK_LIST trigger_bank_list[] = {

   /* online banks */
   {"MPET", TID_DWORD,  100, NULL} ,
   {"MCPP", TID_DWORD,  100, NULL} ,
   {"FWAV", TID_DWORD, 2000, NULL} ,
   {""}
   ,
};

/*-- Equipment list ------------------------------------------------*/

#undef USE_INT

EQUIPMENT equipment[] = {

   {"Trigger",               /* equipment name */
    {1, 0,                  /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
#ifdef USE_INT
     EQ_INTERRUPT,           /* equipment type */
#else
     EQ_PERIODIC, //POLLED,     /* equipment type */
#endif
     LAM_SOURCE(0, 0x0),     /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING,             /* read only when running */
     500,                    /* poll for 500ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",},
    read_trigger_event,      /* readout routine */
    NULL, NULL,
    trigger_bank_list,
    }
   ,

   {"Scaler",                /* equipment name */
    {2, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC ,           /* equipment type */
     0,                      /* event source */
     "MIDAS",                /* format */
     FALSE,                   /* enabled */
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
     RO_ODB,                 /* and update ODB */
     10000,                  /* read every 10 sec */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* log history */
     "", "", "",},
    read_scaler_event,       /* readout routine */
    },

  {"Titan_acq",                /* equipment name */
    {10, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC ,           /* equipment type */
     0,                      /* event source */
     "MIDAS",                /* format */
     FALSE,                   /* enabled */
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
     RO_ODB,                 /* and update ODB */
     10000,                  /* read every 10 sec */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* log history */
     "", "", "",},
    read_titan_event,       /* readout routine */
    },

   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/********************************************************************/

/*-- Sequencer callback info  --------------------------------------*/
void seq_callback(INT hDB, INT hseq, void *info)
{
  printf("odb ... trigger settings touched\n");
}


/*-- Deferred transition -------------------------------------------*/
/* will be called repeatively  while transition is reuqested until transition is issued */
BOOL wait_end_cycle(int transition, BOOL first)
{
  printf("wait_end_cycle: starting with first=%d end_of_cycle=%d\n",first,end_of_cycle);
  if (first) {
    /* Will go through here the first time wait_end_cycle() is called */
    /* setup user flags */
    transition_PS_requested = TRUE;
    /* return false as long as the requested transition should be postponed */
    return FALSE;
  }

  /* Check the user flags for issuing the requested transition */
  if (end_of_cycle)  {
    transition_PS_requested = FALSE;
    end_of_cycle = FALSE;
    /* Tell system to issue transition */
    return TRUE;
  }
  else
    /* in any other case don't do anything ==> no transition */
    return FALSE;
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
  int size, status;
  char set_str[80];
  char path[80];

  /* register for deferred transition */
  status = cm_register_deferred_transition(TR_STOP, wait_end_cycle);
  if(status != SUCCESS)printf("frontend_init: failure from cm_register_deferred...(%d)\n",status);
  status = cm_register_deferred_transition(TR_PAUSE, wait_end_cycle);
  if(status != SUCCESS)printf("frontend_init: failure from cm_register_deferred...(%d)\n",status);

  // Open VME interface
  status = mvme_open(&myvme, 0);

#ifdef PPG_CODE
  status = cm_register_transition(TR_START, tr_checkppg, 350);
  if(status != CM_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init", "cannot register to transition for tr_checkppg");
      return status;
    }
  status = cm_register_transition(TR_START, tr_poststart, 600);
  if(status != CM_SUCCESS)
    {
      cm_msg(MERROR, "frontend_init", "cannot register to transition for tr_poststart");
      return status;
    }

#endif

  /* Book Setting space */
  TRIGGER_SETTINGS_STR(trigger_settings_str);

  /* Map /equipment/Trigger/settings for the sequencer */
  sprintf(set_str, "/Equipment/Trigger/Settings");
  status = db_create_record(hDB, 0, set_str, strcomb(trigger_settings_str));
  status = db_find_key (hDB, 0, set_str, &hSet);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"FE","Key %s not found", set_str);



  /* Enable hot-link on settings/ of the equipment */
  size = sizeof(TRIGGER_SETTINGS);
  if ((status = db_open_record(hDB, hSet, &ts, size, MODE_READ
                               , seq_callback, NULL)) != DB_SUCCESS)
    return status;


#ifdef PPG_CODE

  /* Book Setting space */
  TITAN_ACQ_SETTINGS_STR(titan_acq_settings_str);

  /* Map /equipment/Titan_acq/settings for the sequencer */
  sprintf(set_str, "/Equipment/TITAN_ACQ/Settings");
  status = db_create_record(hDB, 0, set_str, strcomb(titan_acq_settings_str));
  status = db_find_key (hDB, 0, set_str, &hTASet);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"FE","Key %s not found", set_str);

  /* find path for bytecode.dat */
  size = sizeof(path);
  sprintf(set_str,"ppg/input/PPGLOAD path");
  status = db_get_value(hDB, hTASet, set_str, path, &size, TID_STRING, FALSE);
  if(status != DB_SUCCESS)
  {
    cm_msg(MERROR,"frontend_init","cannot get path at %s (%d)",set_str,status);
    return FE_ERR_ODB;
  }
  printf("PPG path:%s\n",path);
  sprintf(ppgfile,"%s/bytecode.dat",path);
  printf("ppgfile:%s\n",ppgfile);


  /* find path for tri_config */
  size = sizeof(PPGpath);
  sprintf(set_str,"ppg/input/PPG path");
  status = db_get_value(hDB, hTASet, set_str, PPGpath, &size, TID_STRING, TRUE);
  if(status != DB_SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","cannot get PPG_path at %s (%d)",set_str,status);
      return FE_ERR_ODB;
    }

  printf("PPGpath:%s  Host name = %s  Exp name = %s \n",PPGpath, host_name,exp_name);
  sprintf(cmd,"%s/tri_config -h %s -s -d",PPGpath,host_name);
  printf("cmd:%s\n",cmd);

#ifdef PLOT
  // also set up command for gnuplots
  sprintf(plot_cmd,"%s/perl/plot_png.pl &",PPGpath);
#endif


   // see if we can read something from the PPG
  {
    BYTE data;
    //    DWORD pol;
    // PPG script must control these outputs
    VPPGPolzCtlPPG( myvme, PPG_BASE);
    VPPGBeamCtlPPG( myvme, PPG_BASE);
    // reverse the polarity
    //    pol = VPPGPolmskWrite( myvme, PPG_BASE, 0xFFFF);
    //pol = VPPGPolmskWrite( myvme, PPG_BASE, 0x0000);
    //printf("polarity mask =  0x%x\n",pol);

    VPPGStatusRead(myvme,PPG_BASE);
    data = VPPGExtTrigRegRead(myvme, PPG_BASE);
  }
#endif
  /* print message and return FE_ERR_HW if frontend should not be started */
  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
#ifdef PPG_CODE

  /*Disable the PPG module just in case */
  VPPGStopSequencer(myvme, PPG_BASE);

#endif
   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  int status, size;
  //  DWORD pol;

#ifdef PPG_CODE
  // make sure PPG controls these outputs
    VPPGPolzCtlPPG( myvme, PPG_BASE);
    VPPGBeamCtlPPG( myvme, PPG_BASE);
    //VPPGPolmskWrite(myvme, PPG_BASE, 0xffffff );
    // reverse the polarity
    //pol = VPPGPolmskWrite( myvme, PPG_BASE, 0xFFFF);
    //printf("polarity mask =  0x%x\n",pol);

#endif

   /* put here clear scalers etc. */

  /* read Triggger settings */
  size = sizeof(TRIGGER_SETTINGS);
  if ((status = db_get_record (hDB, hSet, &ts, &size, 0)) != DB_SUCCESS)
    return status;

#if defined DA816_CODE
#endif

#if defined SOFTDAC_CODE
  int i, range;
#endif



#if defined PPG_CODE
  if(ppg_load(ppgfile) != SUCCESS)
    return FE_ERR_HW;
#endif

#if defined VF48_CODE
  /* Initialize the VF48 --------------------------------------------*/
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);

  vf48_Reset(myvme, VF48_BASE);

  vf48_ExtTrgSet(myvme, VF48_BASE);

  // Set segment size to 16 for now
  for (i=0;i<6;i++)
    vf48_SegSizeSet(myvme, VF48_BASE, i, ts.vf48.nsamples);

  for (i=0;i<6;i++)
    printf("SegSize grp %i:%d\n", i, vf48_SegSizeRead(myvme, VF48_BASE, i));

  // Set Threshold high for external trigger, as the
  // the self-trigger is always sending info to the collector
  // and overwhelming the link. (max maybe 10 bits (0x3FF))
  for (i=0;i<6;i++)
    vf48_HitThresholdSet(myvme, VF48_BASE, i, 0xFF);

  // Set pre-trigger (default:32)
  printf("Pre-Trigger : %d\n", ts.vf48.pretrigger);
  vf48_PreTriggerSet(myvme, VF48_BASE, 0, ts.vf48.pretrigger);

  printf("vf48: CSR:0x%x\n", vf48_CsrRead(myvme, VF48_BASE));

  // Start ACQ for VF48
  vf48_AcqStart(myvme, VF48_BASE);
#endif

#if defined VT2_CODE
  //-------- VMEIO ---------------------------------------------
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);

  vt2_KeepAlive(myvme, VT2_BASE, ts.vt2.keep_alive);
  vt2_CycleReset(myvme, VT2_BASE, ts.vt2.cycle_reset);

  printf("vt2: CSR:0x%x\n", vt2_CSRRead(myvme, VT2_BASE));

#endif

#if defined VMCP_CODE
  //-------- VMCP ---------------------------------------------
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  lrs1190_Reset(myvme, VMCP_BASE);

  printf("vmcp: Count:0x%x\n", lrs1190_CountRead(myvme, VMCP_BASE));

  // Enable the module
  lrs1190_Enable(myvme, VMCP_BASE);
#endif

#if defined VMEIO_CODE
  //-------- VMEIO ---------------------------------------------
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);

  // Set 0xF in pulse mode
  vmeio_OutputSet(myvme, VMEIO_BASE, 0x1);
#endif

  //------ FINAL ACTIONS before BOR -----------
  /* reset flag */
  end_of_cycle = FALSE;

  /* reset the global cycle counter */
  gbl_cycle = 0;

#if defined VT2_CODE
  mvme_set_am(myvme, MVME_AM_A24_ND);
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  vt2_ManReset(myvme, VT2_BASE);
#endif


#if defined SOFTDAC_CODE
  if (softdac_Open(&softdac) < 0)
    {
      cm_msg(MERROR,"fempet","Cannot open PMC-SOFTDAC device!");
      return FE_ERR_HW;
    }
  softdac_SMEnable(softdac);
  printf("softdac SM enabled\n");
  softdac_Status(softdac,1);

  // Load Voltage range
  
  for (i=0;i<SOFTDAC_RANGE_MAX+1;i++) {
    if (strcmp(ts.softdac.range, SOFTDAC_RANGE[i]) == 0) {
      range = SOFTDAC_RANGE_P5V + i;
      softdac_ScaleSet(softdac, range, 0., 0.);
      cm_msg(MINFO, "mpet", "Softdac Range set to %s", ts.softdac.range);
    }    
  }
  if (i == SOFTDAC_RANGE_MAX) {
    cm_msg(MERROR, "mpet", "Softdac Range unknown (%s) uses previous value", ts.softdac.range);
  }
  if (softdac) softdac_Close(softdac);
#endif
  
#if defined DA816_CODE
  if (da816_Open(&da816) < 0)
    {
      cm_msg(MERROR,"fempet","Cannot open PMC-DA816 device!");
      return FE_ERR_HW;
    }
  da816_BankSwitch(da816, 9999);
  da816_AddReset(da816);
  da816_ClkSMEnable(da816,0);
  printf("da816  SM enabled\n");
  da816_Status(da816,1);
  if (da816) da816_Close(da816);
#endif

#ifdef PPG_CODE

  /* start the PPG  (later may use external trigger) */
  printf("Starting PPG\n");
  VPPGStartSequencer(myvme, PPG_BASE);
  ppg_running = TRUE;
#endif

  printf("End of BOR\n");
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{

#ifdef PPG_CODE
  VPPGStopSequencer(myvme,PPG_BASE); /* stops sequencer; later disable ext. trigger if used */
#endif

#ifdef VMCP_CODE
  // Disable the module
  lrs1190_Disable(myvme, VMCP_BASE);
#endif

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  /* reset flag */
  end_of_cycle = FALSE;
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  char str[80];
  BYTE value;
  INT status;

#ifdef PPG_CODE
  if (ppg_running)
    {
      if (run_state == STATE_RUNNING)
	{
	  value = VPPGRegRead(myvme, PPG_BASE, VPPG_VME_READ_STAT_REG );
	  if  (value & 2)
	    {
	      //if (debug)
		printf("pulse blaster IS running (%d)\n",value);
	    }
	  else
	    {
	      printf("pulse blaster is not running (%d); stopping run\n",value);
	      ppg_running = FALSE;
	      //        if (cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, 0) != CM_SUCCESS)
	      status = cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, 0);
	      printf("frontend_loop: after cm_transition status=%d\n",status);
	      if((status !=  CM_SUCCESS) && (status != CM_DEFERRED_TRANSITION))
		cm_msg(MERROR, "FEloop", "cannot stop run immediately: %s (%d)", str,status);
	    }
	}
    }
  if (debug)
    {
      value = VPPGRegRead(myvme, PPG_BASE, VPPG_VME_READ_STAT_REG );
      if  ((value & 2)==0)
	printf("pulse blaster IS NOT running\n");
    }
#endif
  return SUCCESS;
}

/*------------------------------------------------------------------*/
/********************************************************************\

  Readout routines for different events

\********************************************************************/
/*-- Trigger event routines ----------------------------------------*/
INT poll_event(INT source, INT count, BOOL test)
     /* Polling routine for events. Returns TRUE if event
  is available. If test equals TRUE, don't return. The test
  flag is used to time the polling */
    //    if (vmeio_CsrRead(myvme, VMEIO_BASE))
    //    if (lam > 10)
{
  int i;
  int lam = 0;

  for (i = 0; i < count; i++, lam++) {
    lam = vmeio_CsrRead(myvme, VMEIO_BASE);
    if (lam)
      if (!test)
  return lam;
  }

  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/
INT read_trigger_event(char *pevent, INT off)
{
#if defined VMEIO_CODE
INT  latched_word, evNoStrobe;
#endif

#if defined VT2_CODE
  DWORD *pdata;
  INT level;
#endif

#if defined VMCP_CODE
  INT count;
  DWORD *pmdata;
#endif

#if defined VF48_CODE
  DWORD *pwave;
  DWORD nframe, nentry;
#endif

  /* FIRST THING TO DO */
  bk_init(pevent);

  /* In case we're coming as a Periodic equipment
     Check if the cycle is finished */

#if defined VMEIO_CODE
  ///-------------------------------
  vmeio_AsyncWrite(myvme, VMEIO_BASE, 0x2);
  vmeio_SyncWrite(myvme,  VMEIO_BASE, 0x1);
  vmeio_AsyncWrite(myvme, VMEIO_BASE, 0x0);

  if ((vmeio_CsrRead(myvme, VMEIO_BASE) & 0xF)!= 0) {
    latched_word = vmeio_SyncRead(myvme, VMEIO_BASE);
#if defined VERBOSE
    printf("vmeio read 0x%x\n",latched_word);
#endif
  } else {
    evNoStrobe ++;
#if defined VERBOSE
    printf(" No vmeio strobe found \n");
#endif
  }
  vmeio_StrobeClear(myvme, VMEIO_BASE);
#endif

#if defined VT2_CODE
  ///-------------------------------
  level = vt2_FifoLevelRead(myvme, VT2_BASE);
#if defined VERBOSE
  printf("Level/Cycle VT2: %d / %d (0x%x) Level:0x%x\n", level, vt2_CycleNumberRead(myvme, VT2_BASE)
   , vt2_CSRRead(myvme, VT2_BASE)
   , mvme_read_value(myvme, VT2_BASE+VT2_FIFOSTATUS_RO));
#endif
  if ( level > 0) {
    bk_create(pevent, "MPET", TID_DWORD, &pdata);
    vt2_FifoRead(myvme, VT2_BASE, pdata, level);
    pdata += level;
    bk_close(pevent, pdata);
  }
#endif

#if defined VMCP_CODE
  ///-------------------------------
  count = lrs1190_CountRead(myvme, VMCP_BASE);
  if (count > 0) {
    bk_create(pevent, "MCPP", TID_DWORD, &pmdata);
      lrs1190_I4Read(myvme, VMCP_BASE, pmdata, count);
      pmdata += count;
      bk_close(pevent, pmdata);
  }
#endif

#if defined VF48_CODE
  ///-------------------------------
  nframe = VF48_NFrameRead(myvme, VF48_BASE);
  if (nframe > 0) {
    /* create variable length WAVE bank */
    bk_create(pevent, "WAVE", TID_DWORD, &pwave);
    status = vf48_EventRead(myvme, VF48_BASE, pwave, 0, &nentry);
    pwave += nentry;
    bk_close(pevent, pwave);
  }
#endif

  if (transition_PS_requested) {
    /* transition requested: What do we do?
       set end_of_cycle = TRUE will issue the postponed transition
       keep end_of_cycle = FALSE will deferre the transition to later */
    printf("%d Could postpone the transition now!\n", gbl_cycle++);
    if (gbl_cycle >= 1)
      end_of_cycle = TRUE;
  }

  if (bk_size(pevent) > 8)
    return bk_size(pevent);
  else
   return 0;
}
/*-- Dummy Titan Event --------------------------------------------*/
INT read_titan_event(char *pevent, INT off)
{
  return 0;
}

/*-- Scaler event --------------------------------------------------*/
INT read_scaler_event(char *pevent, INT off)
{
  DWORD *pdata;

  /* init bank structure */
  bk_init(pevent);

  /* create SCLR bank */
  bk_create(pevent, "SCLR", TID_DWORD, &pdata);

  /* read scaler bank */
  bk_close(pevent, pdata);

  //   return bk_size(pevent);
  // Nothing to send for now
  return 0;
}


#ifdef PPG_CODE

INT tr_checkppg(INT run_number, char *error)
{
  struct stat stbuf;
  int status;
  time_t timbuf;
  char timbuf_ascii[30];
  int elapsed_time;


  //  status = execl("/bin/sh", "sh", "-c", cmd, NULL);
  //  printf("tr_checkppg: %s sent through execl (status:%d\n",cmd, status);

  printf("tr_checkppg: sending system command:\"%s\" \n",cmd);
  status =  system(cmd);

  stat (ppgfile,&stbuf);
  printf("tr_checkppg: file %s  size %d\n",ppgfile, (int)stbuf.st_size);

  if(stbuf.st_size < 0)
    {
      cm_msg(MERROR,"tr_checkppg","PPG load file %s cannot be found",ppgfile);
      return DB_FILE_ERROR ;
    }

  strcpy(timbuf_ascii, (char *) (ctime(&stbuf.st_mtime)) );
  printf ("tr_checkppg: PPG loadfile last modified:  %s or (binary) %d \n",timbuf_ascii,(int)stbuf.st_size);


  // get present time
  time (&timbuf);

  strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) );
  printf ("tr_checkppg: Present time:  %s or (binary) %d \n",timbuf_ascii,(int)timbuf);

  elapsed_time= (int) ( timbuf - stbuf.st_mtime );

  printf("tr_precheck: time since ppg loadfile last updated: %d seconds \n",elapsed_time);
  if(elapsed_time > 3)
    {
      cm_msg(MERROR,"tr_checkppg","PPG load file %s is too old",ppgfile);
      return DB_FILE_ERROR ;
    }
  return SUCCESS;
}


INT ppg_load(char *ppgfile)
{
  /* download ppg file bytecode.dat
   */


  /* Stop the PPG sequencer  */
  VPPGStopSequencer(myvme, PPG_BASE);

  /* tr_precheck checked that tri_config has run recently */
  if(VPPGLoad(myvme, PPG_BASE, ppgfile) != SUCCESS)
    {
      cm_msg(MERROR,"ppg_load","failure loading ppg with file %s",ppgfile);
      return FE_ERR_HW;
    }
  printf("ppg_load: PPG file %s successfully loaded",ppgfile);
  //  cm_msg(MINFO,"ppg_load","PPG file %s successfully loaded",ppgfile);
  return SUCCESS;
}


INT tr_poststart(INT run_number, char *error)
{
  INT status;
  char copy_cmd[132];


  if(run_state == STATE_RUNNING)
    {
#ifdef PLOT
      // cm_msg(MINFO,"tr_poststart","plotting PPG data");
      //printf("tr_poststart: sending system command:\"%s\" \n",plot_cmd);
      //status =  system(plot_cmd);
      //printf("tr_poststart: status after system command=%d\n",status);
#endif
      sprintf(copy_cmd,"cp %s/ppgload/bytecode.dat /data/mpet/info/run%d_bytecode.dat &",
        PPGpath,run_number);
      status =  system(copy_cmd);
      printf("tr_poststart: status after system command \"%s\" is %d\n",copy_cmd, status);

      sprintf(copy_cmd,"cp %s/ppgload/mpet.ppg /data/mpet/info/run%d_mpet.ppg &",PPGpath,run_number);
      status =  system(copy_cmd);
      printf("tr_poststart: status after system command \"%s\" is %d\n",copy_cmd, status);


    }
  return SUCCESS;
}

#endif

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */


