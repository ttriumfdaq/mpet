#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(void)
{
  int idelay,jdelay;
  int freq=1000000000;

  double time_slice,diff;
  double mdelay,ndelay,nm;
  double df,mult,min_delay;
  
  df=(double)freq;
  mult = 10000000;

  time_slice= 1.0/df;
  min_delay = 5*time_slice;
  mdelay = .123456789;
  ndelay = .123456776;

  min_delay *=mult;
  mdelay *=mult;
  ndelay *=mult;

  diff = fabs(mdelay-ndelay);

  printf("freq=%dHz or %g; time slice=%g  min delay=%.8f diff=%.8f\n",
	 freq,df,time_slice,min_delay,diff);
  printf("mdelay=%.8f  ndelay=%.8f  \n",mdelay,ndelay);

  if(diff < min_delay) printf("within min delay\n");
  else if(mdelay > ndelay) printf("mdelay is larger\n");
  else  printf("mdelay is smaller\n");

  if(1)return;
  mult= df;
  printf("or mult=%g or %f\n",mult,mult);
  if(1)return;


   idelay = (int) ( (mdelay + time_slice) * mult );

   printf("mdelay=%f mult=%f; time_slice=%f; idelay=%d\n",
	  mdelay,mult,time_slice,idelay);
  //   idelay = (INT) ( (mdelay + 0.000005) * 100000.0 );
  // jdelay = (INT) ( (ddelay + 0.000005) * 100000.0 );

   freq=100000000;
  df=(double)freq;
  time_slice= 1.0/df;
  mult = .5/time_slice;

  printf("freq=%dHz or %g; time slice=%g mult=%g\n",freq,df,time_slice,mult);


   idelay = (int) ( (mdelay + time_slice) * mult );
   printf("mdelay=%f mult=%f; time_slice=%f; idelay=%d\n",
	  mdelay,mult,time_slice,idelay);
   return 0;
}
