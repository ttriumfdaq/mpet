// compute.h
#ifndef  COMPUTE_INCLUDE_H
#define  COMPUTE_INCLUDE_H

INT compute(char *ppg_mode);
INT compare(double my_delay, double d_delay);
void bit_assignment( FILE  *outf);
INT process_trans(TRANSITIONS *ptrans , FILE *outf);
DWORD backpattern(DWORD bitpat);
void get_delay_name(double my_delay, INT ndelay, INT *namecount, FILE *outf);
void print_delays(FILE *outf);
void assemble_line(char *string, DWORD bitpat);
#endif
