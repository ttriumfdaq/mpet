/********************************************************************\

  Name:         fevt2.c
  Created by:   Stefan Ritt

  Contents:     Experiment specific readout code (user part) of
                Midas frontend. This example simulates a "trigger
                event" and a "scaler event" which are filled with
                CAMAC or random data. The trigger event is filled
                with two banks (ADC0 and TDC0), the scaler event
                with one bank (SCLR).

  $Id: frontend.c 3197 2006-07-31 19:01:21Z ritt $

\********************************************************************/
#define   VT2_CODE

#include <stdio.h>
#include <stdlib.h>
#include "midas.h"
#include "mvmestd.h"
#include "vt2.h"
#include "experim.h"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "feVT2";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 000;

/* maximum event size produced by this frontend */
INT max_event_size = 10000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 10 * 10000;

#define N_TDC  1

// VME board
DWORD VT2_BASE  = 0xe00000;
MVME_INTERFACE *myvme;

extern HNDLE hDB;
HNDLE hSet;
TRIGGER_SETTINGS ts;

/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trigger_event(char *pevent, INT off);

/*-- Equipment list ------------------------------------------------*/
#undef USE_INT

EQUIPMENT equipment[] = {

   {"Trigger",               /* equipment name */
    {1, 0,                   /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC,
     LAM_SOURCE(0, 0xFFFFFF),        /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING |            /* read only when running */
     RO_ODB,                 /* and update ODB */
     100,                    /* poll for 100ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",},
    read_trigger_event,      /* readout routine */
    },

   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Sequencer callback info  --------------------------------------*/
void seq_callback(INT hDB, INT hseq, void *info)
{
  printf("odb ... trigger settings touched\n");
}

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  int size, status;
  char set_str[80];

  // Get access to the Equipment Settings
  /* Book Setting space */
  TRIGGER_SETTINGS_STR(trigger_settings_str);

  /* Map /equipment/Trigger/settings for the sequencer */
  sprintf(set_str, "/Equipment/Trigger/settings");
  status = db_create_record(hDB, 0, set_str, strcomb(trigger_settings_str));
  status = db_find_key (hDB, 0, set_str, &hSet);
  if (status != DB_SUCCESS)
    cm_msg(MINFO,"FE","Key %s not found", set_str);
  
  /* Enable hot-link on settings/ of the equipment */
  size = sizeof(TRIGGER_SETTINGS);
  if ((status = db_open_record(hDB, hSet, &ts, size, MODE_READ
                               , seq_callback, NULL)) != DB_SUCCESS)
    return status;
  
 /* hardware initialization */
  status = mvme_open(&myvme, 0);
  
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);
   return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  mvme_close(myvme);
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  INT status, size;

  /* read Triggger settings */
  size = sizeof(TRIGGER_SETTINGS);
  if ((status = db_get_record (hDB, hSet, &ts, &size, 0)) != DB_SUCCESS)
    return status;

  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);
  
  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  
#if defined VT2_CODE
  //-------- TDC -------------------------------------------
  if (1) {
    printf("Manual Reset\n");
    vt2_ManReset(myvme, VT2_BASE);
  }
  
    printf("Set Keep alive:%i\n", ts.keep_alive);
    vt2_KeepAlive(myvme, VT2_BASE, ts.keep_alive);

    printf("Set Cycle Reset:%i\n", ts.cycle_reset);
    vt2_CycleReset(myvme, VT2_BASE, ts.cycle_reset);
  
  status = vt2_CSRRead(myvme, VT2_BASE);
  printf("CSR: 0x%x\n", status);

#endif
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */
   return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   int i;
   DWORD lam=0;

   for (i = 0; i < count; i++) {
     //      cam_lam_read(LAM_SOURCE_CRATE(source), &lam);

      if (lam & LAM_SOURCE_STATION(source))
         if (!test)
            return lam;
   }

   return 0;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT read_trigger_event(char *pevent, INT off)
{
   DWORD *pdata;
   INT    level;

   /* init bank structure */
   bk_init(pevent);
   
   level = vt2_FifoLevelRead(myvme, VT2_BASE);
   if (level > 50) {
     
     /* create variable length TDC bank */
     bk_create(pevent, "TDC0", TID_DWORD, &pdata);
     *pdata++ = level;
     vt2_FifoRead(myvme, VT2_BASE, pdata, level);
     pdata += level;
     bk_close(pevent, pdata);
   }
   else if (level < 0) 
     printf("Fifo Full\n");
   
   return  bk_size(pevent);
}
