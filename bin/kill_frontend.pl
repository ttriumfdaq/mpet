#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
# $Log: kill_frontend.pl,v $
#
use strict;

# invoke this script with cmd 
#                         Parameter
# perl kill_frontend.pl        0 = kill task first in odb then kill program if still running
#                              1 = just kill the program if running

sub kill_frontend_task(); # prototype
#
#
our $MIDAS_EXPT_NAME = $ENV{'MIDAS_EXPT_NAME'};
our $FRONTEND_TASK_NAME = "fempet";
our $HOME = $ENV{'HOME'};
our $EXPERIM_DIR = $ENV{'EXPERIM_DIR'};
print "kill_frontend: environment variables: selected MIDAS_EXPT_NAME  $MIDAS_EXPT_NAME, HOME is $HOME  \n";

#  check-host-vmic looks for host $DAQ_HOST username  "mpet"
my $reply = `$EXPERIM_DIR/bin/check-host-vmic `;
print "$reply\n";
if ( "$?" != "0" ) {
    print "kill_frontend: failure from check-host-vmic\n";
    exit;
}

#

my ($param) = @ARGV;  # need the brackets
print "input parameter =$param\n";

unless ($param)
{  # skip this if param=1 (i.e. called from kill-all where mserver has been shut down)

    my $done=0;
    my $count=0;
    while ( ! $done && $count < 3 )
    {
        $count++;  # if pgm can't be killed avoid infinite loop
        $reply = `odb -c scl -e $MIDAS_EXPT_NAME` ;

       print "kill_frontend: active Midas clients are \n $reply \n" ;
        if ($reply =~/($FRONTEND_TASK_NAME\d?)/i)
        {  
            print "found $1 \n";
            print "shutting down $1\n";
            `odb -c 'sh $1' `;  
        }
        else
        { 
            $done=1;
        }
    }
    sleep 2;
} # end of unless $param
kill_frontend_task();
exit;


sub kill_frontend_task()
{
# make sure task is stopped
    my $task = $FRONTEND_TASK_NAME;
    print "checking for task $task\n";
    `killall $task`;
    return;
}
